<?php session_start(); ?>
<!doctype html>

<html class="no-js" lang="">
<head>
	<title>Registration To THEQUANTUMCODES</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<script src="../code.jquery.com/jquery-2.2.4.min.js"></script>
	<script src="http://soft.thequantumcodes.trade/js/form/jquery.validate.min.js"></script>
		<script type="text/javascript" src="http://soft.thequantumcodes.trade/js/form/boost-form.js"></script>
	<link rel="stylesheet" href="http://soft.thequantumcodes.trade/css/bootstrap.css">
	<link rel="stylesheet" href="http://soft.thequantumcodes.trade/css/form.css">
		<link href="http://soft.thequantumcodes.trade/domains/83/css/boost-form.css" rel="stylesheet" type="text/css" />
	<script src="http://soft.thequantumcodes.trade/js/form/p.js"></script>
</head>
<body>
	<form method="post"autocomplete="off" action="api_hit.php">
	    <input type="hidden" name="country" id="country">
	    <input type="hidden" name="ip" id="ip">
				<div id="FirstName-container" class="form-group">
            <input class="form-control input-lg names icons" type="text" id="FirstName" name="f_name" placeholder="First Name"
                   maxlength="45" data-rule-required="true" data-rule-lettersonly="true" data-msg-required="Enter Valid First Name"
                   data-rule-minlength="2" <?php if (isset($_SESSION['username'])): ?>
                   	value="<?php echo $_SESSION['username']; ?>"
									<?php endif ?> style="border-radius:5px !important;">
        </div>
		<div id="LastName-container" class="form-group">
            <input class="form-control input-lg names icons" type="text" id="LastName" name="l_name" placeholder="Last Name" maxlength="45"
                   data-rule-allletters="true" data-rule-required="true" data-msg-required="Enter Valid Last Name"
                   data-rule-minlength="2" value="" style="border-radius:5px !important;">
        </div>
		<div id="email-container" class="form-group">
            <input class="form-control input-lg icons" type="email" id="email" name="email" placeholder="E-mail" maxlength="255"
                   data-rule-email="true" data-rule-required="true" data-msg-required="Enter Valid Email" <?php if (isset($_SESSION['email'])): ?>
                   	value="<?php echo $_SESSION['email']; ?>"
									<?php endif ?> style="border-radius:5px !important;">
        </div>
		<div id="password-container" class="form-group">
		<input class="form-control input-lg icons" type="text" name="password" id="password" placeholder="Choose a Password"
			   maxlength="32" data-rule-password="true" data-rule-required="true" data-rule-minlength="6"
			   data-rule-maxlength="15" data-msg-required="Must contain letters or digits" value="" style="border-radius:5px !important;">
        </div>
		<div class="form-group col-xs-4 col-md-4 no-gutter-left">
                <input class="form-control input-lg icons" placeholder="Area Code" type="tel" id="kid" name="areaCode" maxlength="4"
                       data-rule-digits="true" data-rule-required="true" data-msg-required="Enter Valid Prefix" style="border-radius:5px !important;width:100% !important;">
        </div>
		<div class="form-group col-xs-8 col-md-8 no-gutter-right phone-right-side">
		<input class="form-control input-lg icons" type="tel" id="phone" name="phn_no" placeholder="Phone" maxlength="16"
			   data-rule-digits="true" data-rule-required="true" data-msg-required="Enter Valid Phone Number"
			   value="" style="border-radius:5px !important;width:100% !important;">
        </div>

		<div id="regBtn-container" class="form-group">
            <button class="btn btn-block buttons-style" id="regBtn" type="submit">CREATE PROFIT ACCOUNT</button>
        </div>
			</form>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
	$(document).ready(function(){
		setTimeout(function(){
			$("#password").attr("type","password");
		},7000);
		var country = '';
      var ip = '';
      $.ajax({
        url: 'http://freegeoip.net/json/',
        type: 'GET',
        dataType: 'JSON',
        success:function(result) {
          country = result.country_name;
          ip = result.ip;
          $('#country').val(country);
          $('#ip').val(ip);
          console.log(result);
          var country_code = null;
          var userip = result.country_name

          $.getJSON('https://restcountries.eu/rest/v2/name/' + userip, function(data){
              country_code = data[0]['callingCodes'][0];
              console.log(country_code);
              $('#kid').val(country_code);
          });
        }
      });
	});
</script>

</body>

</html>
