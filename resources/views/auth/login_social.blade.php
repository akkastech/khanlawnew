<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Reactor - Bootstrap Admin Template</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="{{url('admin/styles/webfont.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/climacons-font.css')}}">
  <link rel="stylesheet" href="{{url('admin/vendor/bootstrap/dist/css/bootstrap.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/font-awesome.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/card.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/sli.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/animate.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/app.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/app.skins.css')}}">
  <!-- endbuild -->
</head>

<body class="page-loading" style="overflow:hidden;">
  <!-- page loading spinner -->
  <!-- <div class="pageload">
    <div class="pageload-inner">
      <div class="sk-rotating-plane"></div>
    </div>
  </div> -->
  <!-- /page loading spinner -->
  <div class="app signup v2 usersession">
    <div class="session-wrapper">
      <div class="session-carousel slide" data-ride="carousel" data-interval="3000" style="width: calc(100% - 0px);">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
         <div class="item active" style="background-image:url({{url('admin/images/bg1.png')}});background-size:cover;background-repeat: no-repeat;background-position: 50% 50%;">
          </div>
          <div class="item" style="background-image:url({{url('admin/images/bg2.png')}});background-size:cover;background-repeat: no-repeat;background-position: 50% 50%;">
          </div>
          </div>
        </div>
      </div>
      <div class="card bg-white no-border" id="left_side" style="float: left;overflow-y:scroll;max-height:380px;">
        <div class="card-block" style="padding-top: 50px;">
          @if(session('error'))
            <p class="alert alert-danger">{{session('error')}}</p>
          @endif
          @if(session('success'))
            <p class="alert alert-success">{{session('success')}}</p>
          @endif
          <form role="form" class="form-layout" action="{{url('register')}}" method="post" id="signup">
            <input type="hidden" id="country_idFk" name="country_idFk">
          <input type="hidden" id="province_idFk" name="province_idFk">
          <input type="hidden" id="city_idFk" name="city_idFk">
          <input id="token" name="_token" type="text" value="{!! csrf_token() !!}" hidden>
            <div class="text-center m-b">
              <h4 class="text-uppercase">Register Now</h4>
              <!-- <p>Join a growing community.</p> -->
            </div>
            <div class="form-inputs">
                <input type="hidden" name="name" value="{{$user->name}}">
                <input type="hidden" name="img" value="{{ $user->avatar }}">
              <label class="text-uppercase">Type Of User</label>
              <select class="form-control input-lg" name="type" style="width:100%;" required="">
                  <option value="">Plese Select</option>
                  <option value="3" @if(old('type') == '3') selected="" @endif >Realtor</option>
                  <option value="4" @if(old('type') == '4') selected="" @endif >Lender</option>
              </select>
              <select class="form-control input-lg" name="country" id="country" style="width:100%;display: none;" required="" >
                  <option value="">Plese Select</option>
              </select>
              <select class="form-control input-lg" name="province" id="province" style="width:100%;display: none;" required="" >
                  <option value="">Plese Select</option>
              </select>
              <label class="text-uppercase">City</label>
              <select class="form-control input-lg" name="city" id="city" style="width:100%;" required="">
                  <option value="">Plese Select</option>
              </select>
              <input type="hidden" value="{{ $user->email}}" name="email">
            </div>
            <br>
            <input class="btn btn-primary btn-block btn-lg m-b" type="submit" value="Create Account">
            <p class="text-center"><small><em>By clicking Create account you agree to our <a href="#">terms and conditions</a></em></small>
            </p>
            <div class="divider">
              <span>OR</span>
            </div>
            <a class="btn btn-block no-bg btn-lg m-b" href="{{url('login')}}">Login</a>
            <p class="text-center"><small><em>By clicking Log in you agree to our <a href="#">terms and conditions</a></em></small>
            </p>
            <div class="divider">
              <span>OR</span>
            </div>
            <a class="btn btn-facebook no-bg btn-lg m-b" href="{{url('facebook/login')}}">Login</a>
          </form>
        </div>
      </div>
      <div class="push"></div>
    </div>
  </div>
  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="{{url('admin/scripts/helpers/modernizr.js')}}"></script>
  <script src="{{url('admin/vendor/jquery/dist/jquery.js')}}"></script>
  <script src="{{url('admin/vendor/bootstrap/dist/js/bootstrap.js')}}"></script>
  <script src="{{url('admin/vendor/fastclick/lib/fastclick.js')}}"></script>
  <script src="{{url('admin/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
  <script src="{{url('admin/scripts/helpers/smartresize.js')}}"></script>
  <script src="{{url('admin/scripts/constants.js')}}"></script>
  <script src="{{url('admin/scripts/main.js')}}"></script>
  <!-- endbuild -->

      <!-- <script type="text/javascript">
      $(document).ready(function(){

        var height = $(window).height();

        // var right_height = height/100*70;

        $('#left_side').css('max-height', (height/100*58));



        $.ajax({
            type:'POST',
            url:'http://dezynage.com/country/ajaxData.php',
            data:'country=yes',
            success:function(html){
                $('#country').html(html);
                $('#province').html('<option value="">Select Country first</option>');
            }
        });
          $('#country').on('change',function(){
              var countryID = $(this).val();
              if(countryID){
                  $.ajax({
                      type:'POST',
                      url:'http://dezynage.com/country/ajaxData.php',
                      data:'country_id='+countryID,
                      success:function(html){
                          $('#province').html(html);
                          $('#city').html('<option value="">Select Province first</option>');
                      }
                  });
              }else{
                  $('#province').html('<option value="">Select country first</option>');
                  $('#city').html('<option value="">Select Province first</option>');
              }
          });

          $('#province').on('change',function(){
              var stateID = $(this).val();
              if(stateID){
                  $.ajax({
                      type:'POST',
                      url:'http://dezynage.com/country/ajaxData.php',
                      data:'state_id='+stateID,
                      success:function(html){
                          $('#city').html(html);
                      }
                  });
              }else{
                  $('#city').html('<option value="">Select Province first</option>');
              }
          });
      });
    </script> -->

    <script type="text/javascript">
    var height = $(window).height();

    // var right_height = height/100*70;

    $('#left_side').css('max-height', (height/100*58));

      function ajaxCall() {
        this.send = function(data, url, method, success, type) {
            type = type || 'json';
            var successRes = function(data) {
                success(data);
            };

            var errorRes = function(e) {
                console.log(e);
                alert("Error found \nError Code: " + e.status + " \nError Message: " + e.statusText);
            };
            $.ajax({
                url: url,
                type: method,
                data: data,
                success: successRes,
                error: errorRes,
                dataType: type,
                timeout: 60000
            });

        }

    }

    function locationInfo() {
        var rootUrl = "http://akkastechdemo.website/country/v2/api.php";
        var call = new ajaxCall();
        this.getCities = function(id) {
            $("#city option:gt(0)").remove();
            var url = rootUrl + '?type=getCities&stateId=' + id;
            var method = "post";
            var data = {};
            $('#city').find("option:eq(0)").html("Please wait..");
            call.send(data, url, method, function(data) {
                $('#city').find("option:eq(0)").html("Prašome pasirinkti");
                if (data.tp == 1) {
                    $.each(data['result'], function(key, val) {
                        var option = $('<option />');
                          option.attr('value', val).attr("data-key", key).text(val);
                        $('#city').append(option);
                    });
                    $('#city_idFk').val($(this).data('key'));
                    $("#city").prop("disabled", false);
                } else {
                    alert(data.msg);
                }
            });
        };

        this.getStates = function(id) {
            $("#province option:gt(0)").remove();
            $("#city option:gt(0)").remove();
            var url = rootUrl + '?type=getStates&countryId=' + id;
            var method = "post";
            var data = {};
            $('#province').find("option:eq(0)").html("Please wait..");
            call.send(data, url, method, function(data) {
                $('#province').find("option:eq(0)").html("Prašome pasirinkti");
                if (data.tp == 1) {
                    $.each(data['result'], function(key, val) {
                        var option = $('<option />');
                        if(key == 671){
                          option.attr('selected', true).attr('value', val).attr("data-key", key).text(val);
                        }else{
                          option.attr('value', val).attr("data-key", key).text(val);
                        }
                        $('#province').append(option);
                    });
                    $("#province").prop("disabled", false);
                    var stateId = $(this).children('option:selected').data('key');
                    $('#province_idFk').val(stateId);
                } else {
                    alert(data.msg);
                }
            });
        };

        this.getCountries = function() {
            var url = rootUrl + '?type=getCountries';
            var method = "post";
            var data = {};
            $('#country').find("option:eq(0)").html("Please wait..");
            call.send(data, url, method, function(data) {
                $('#country').find("option:eq(0)").html("Select Country");
                console.log(data);
                if (data.tp == 1) {
                    $.each(data['result'], function(key, val) {
                        var option = $('<option />');
                        if(key == 38){
                          option.attr('selected', true).attr('value', val).attr("data-key", key).text(val);
                        }else{
                          option.attr('value', val).attr("data-key", key).text(val);
                        }
                        $('#country').append(option);
                    });
                    var countryId = $(this).children('option:selected').data('key');
                    $('#country_idFk').val(countryId);
                    $("#country").prop("disabled", false);
                } else {
                    alert(data.msg);
                }
            });
        };

    }

    $(function() {
        var loc = new locationInfo();
        loc.getCountries();
        loc.getStates(38);
        loc.getCities(671);
        $("#country").on("change", function(ev) {
            var countryId = $(this).children('option:selected').data('key');
            if (countryId != '') {
                loc.getStates(countryId);
                $('#country_idFk').val(countryId);
            } else {
                $("#province option:gt(0)").remove();
            }
        });
        $("#province").on("change", function(ev) {
            var stateId = $(this).children('option:selected').data('key');
            console.log(stateId);
            if (stateId != '') {
                loc.getCities(stateId);
                $('#province_idFk').val(stateId);
            } else {
                $("#city option:gt(0)").remove();
            }
        });

    });


    $('#city').on('change', function(){
      var city = $(this).children('option:selected').data('key');
      $('#city_idFk').val(city);

    });


    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    {{-- <script type="text/javascript">
    function validateEmail(email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }

    function validate() {
      $("#result").text("");
      var email = $("#email").val();
      if (validateEmail(email)) {
        $('#signup').submit();
      } else {
        $("#result").text("Email format is not valid");
        $("#result").css("color", "red");
      }
      return false;
    }

    $("#validate").bind("click", validate);
    </script> --}}

</body>

</html>
