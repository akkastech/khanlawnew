<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Portal - Login</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="{{url('admin/styles/webfont.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/climacons-font.css')}}">
  <link rel="stylesheet" href="{{url('admin/vendor/bootstrap/dist/css/bootstrap.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/font-awesome.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/card.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/sli.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/animate.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/app.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/app.skins.css')}}">
  <!-- endbuild -->
</head>

<body class="page-loading">
  <!-- page loading spinner -->
  <!-- <div class="pageload">
    <div class="pageload-inner">
      <div class="sk-rotating-plane"></div>
    </div>
  </div> -->
  <!-- /page loading spinner -->
  <div class="app signin v2 usersession">
    <div class="session-wrapper">
      <div class="session-carousel slide" data-ride="carousel" data-interval="3000" style="width: calc(100% - 0px);">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active" style="background-image:url({{url('admin/images/bg1.png')}});background-size:cover;background-repeat: no-repeat;background-position: 50% 50%;">
          </div>
          <div class="item" style="background-image:url({{url('admin/images/bg2.png')}});background-size:cover;background-repeat: no-repeat;background-position: 50% 50%;">
          </div>

        </div>
      </div>
      <div class="card bg-white no-border" style="float: left;">
        <div class="card-block" style="padding-top: 50px;">
          @if(session('error'))
            <p class="alert alert-danger">{{session('error')}}</p>
          @endif
          @if(session('success'))
            <p class="alert alert-success">{{session('success')}}</p>
          @endif
          <form role="form" class="form-layout" action="{{url('authuser')}}" method="post" id="login">
          <input id="token" name="_token" type="text" value="{!! csrf_token() !!}" hidden>
            <div class="text-center m-b">
              <h4 class="text-uppercase">My Portal</h4>
              <p>Please sign in to your account</p>
            </div>
            <div class="form-inputs p-b">
              <label class="text-uppercase">Your email address</label>
              <input type="email" class="form-control input-lg" name="email" id="email" placeholder="Email Address" required>
              <p id='result'></p>
              <span></span>
              <label class="text-uppercase">Password</label>
              <input type="password" class="form-control input-lg" name="password" placeholder="Password" required>
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="remember"> Remember Me
                </label>
              </div>
              <a ui-sref="user.forgot">Forgotten password?</a>
            </div>
            <button class="btn btn-primary btn-block btn-lg m-b" type="submit" id="validate">Login</button>
            <div class="divider">
              <span>OR</span>
            </div>

            <div class="col-md-4">
              <a class="btn btn-primary" href="{{ url('facebook/login') }}">Facebook</a>
            </div>
            <div class="col-md-4">
              <a class="btn btn-block no-bg btn-lg m-b" href="{{url('signup')}}">Signup</a>
            </div>
            <div class="col-md-4">
              <a class="btn btn-default" href="{{ url('google/login') }}">Google</a>
            </div><br>
            <p class="text-center"><small>By clicking Log in you agree to our <a href="#">terms and conditions</a></small>
            </p>
          </form>
        </div>
      </div>
      <div class="push"></div>
    </div>
  </div>
  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="{{url('adminscripts/helpers/modernizr.js')}}"></script>
  <script src="{{url('adminvendor/jquery/dist/jquery.js')}}"></script>
  <script src="{{url('adminvendor/bootstrap/dist/js/bootstrap.js')}}"></script>
  <script src="{{url('adminvendor/fastclick/lib/fastclick.js')}}"></script>
  <script src="{{url('adminvendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
  <script src="{{url('adminscripts/helpers/smartresize.js')}}"></script>
  <script src="{{url('adminscripts/constants.js')}}"></script>
  <script src="{{url('adminscripts/main.js')}}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script type="text/javascript">
  function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  function validate() {
    $("#result").text("");
    var email = $("#email").val();
    if (validateEmail(email)) {
      $('#login').submit();
    } else {
      $("#result").text("Email format is not valid");
      $("#result").css("color", "red");
    }
    return false;
  }

  $("#validate").bind("click", validate);
  </script>

  <!-- endbuild -->
</body>

</html>
