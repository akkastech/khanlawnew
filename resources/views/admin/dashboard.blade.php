@extends('admin.layout')

@section('title', 'Dashboard')


@section('content')

@section('page_name', 'Dashboard')
    <style type="text/css">
      .notification_span{
        width: 30px;
        height: 30px;
        border-radius: 50%;
        background-color: #ff6969;
        padding-top: 5px;
        position: relative;
        display: block;
        color: white;
        font-weight: bold;
        top: -68px;
        right: -15px;

      }
    </style>

    <!-- <div class="row">
      <div class="col-md-2">
        <a href="https://docs.google.com/document/d/1-IJCDiPkwQBPgapckel2kZubOSTevJxG8Q3hOUUpFnc/pub" target="_blank">View</a>
      </div>
    </div> -->


    @if(Auth::User()->user_role_idFk == 1 || Auth::User()->user_role_idFk == 6 || Auth::User()->user_role_idFk == 7 )

      <div class="row same-height-cards">

        <div class="col-md-4">
            <a href="{{url('projects/list')}}">
              <div class="card bg-default text-black" style="max-height: 175px; background-color: white;">
                <div class="p-x p-b">

                  <div class="text-center" style="margin:30px 0;">

                    <center>
                      <img src="{{url('admin/images/deals.png')}}" class="img-responsive m-a-0 text-uppercase">
                      @if(Auth::User()->new_projects() > 0)
                        <span class="notification_span">{{count(Auth::User()->new_projects())}}</span>
                      @endif
                    </center>

                     <h4 class="text-uppercase">Closings</h4>

                  </div>

                </div>
              </div>
            </a>
          </div>
           <div class="col-md-4">
           <a href="{{url('users/list')}}">
              <div class="card bg-default text-black" style="max-height: 175px; background-color: white;">
                <div class="p-x p-b">
                  <div class="text-center" style="margin:30px 0;">
                     <center><img src="{{url('admin/images/clients.png')}}" class="img-responsive m-a-0 text-uppercase">

                      @if(Auth::User()->new_users() > 0)
                        <span class="notification_span">{{Auth::User()->new_users()}}</span>
                      @endif
                     </center>
                    <h4 class="text-uppercase">Users</h4>
                  </div>

                </div>
              </div>
            </a>
          </div>
       <div class="col-md-4">
          <a href="{{url('portals/list')}}">
            <div class="card bg-default text-black" style="max-height: 175px; background-color: white;">
              <div class="p-x p-b">
                <div class="text-center" style="margin:30px 0;">
                   <center><img src="{{url('admin/images/messages.png')}}" class="img-responsive m-a-0 text-uppercase">
                    @if(count(Auth::User()->un_read) > 0)
                      <span class="notification_span">{{count(Auth::User()->un_read)}}</span>
                    @endif
                   </center>
                   <h4 class="text-uppercase">Messages</h4>
                </div>

              </div>
            </div>
          </div>
          </a>
        </div>

     <div class="row same-height-cards">
           @if(count(Auth::User()->project_for_tracker()) > 0)<div class="col-md-4">
          <a href="{{url('project/tracker/page')}}">
            <div class="card bg-default text-black" style="max-height: 175px; background-color: white;">
              <div class="p-x p-b">
                <div class="text-center" style="margin:30px 0;">
                   <center><img src="{{url('admin/images/messages.png')}}" class="img-responsive m-a-0 text-uppercase">
                    @if(Auth::User()->new_projects() > 0)
                      <span class="notification_span">{{Auth::User()->new_projects()}}</span>
                    @endif
                   </center>
                   <h4 class="text-uppercase">Trackers</h4>
                </div>

              </div>
            </div>
          </div>
          </a>
        </div>
        @endif
    </div>

    @endif

    @if(Auth::User()->user_role_idFk == 3 || Auth::User()->user_role_idFk == 4)
      <div class="row same-height-cards">

        <div class="col-md-4">
            <a href="{{url('projects/list')}}">
              <div class="card bg-default text-black" style="max-height: 175px; background-color: white;">
                <div class="p-x p-b">

                  <div class="text-center" style="margin:30px 0;">

                     <center><img src="{{url('admin/images/deals.png')}}" class="img-responsive m-a-0 text-uppercase">
                     @if(Auth::User()->new_projects() > 0)
                        <span class="notification_span">{{count(Auth::User()->new_projects())}}</span>
                      @endif
                     </center>
                     <h4 class="text-uppercase">Closings</h4>

                  </div>

                </div>
              </div>
            </a>
          </div>
          @if(Auth::User()->user_role_idFk == 3)
            <div class="col-md-4">
             <a href="{{url('users/list')}}">
                <div class="card bg-default text-black" style="max-height: 175px; background-color: white;">
                  <div class="p-x p-b">
                    <div class="text-center" style="margin:30px 0;">
                       <center><img src="{{url('admin/images/clients.png')}}" class="img-responsive m-a-0 text-uppercase">
                       @if(Auth::User()->new_projects() > 0)
                        <span class="notification_span">{{count(Auth::User()->new_users())}}</span>
                      @endif
                       </center>

                      <h4 class="text-uppercase">Clients</h4>
                    </div>

                  </div>
                </div>
              </a>
            </div>
          @endif

       <div class="col-md-4">
          <a href="{{url('projects/list')}}">
            <div class="card bg-default text-black" style="max-height: 175px; background-color: white;">
              <div class="p-x p-b">
                <div class="text-center" style="margin:30px 0;">
                   <center><img src="{{url('admin/images/messages.png')}}" class="img-responsive m-a-0 text-uppercase">
                   @if(count(Auth::User()->un_read) > 0)
                      <span class="notification_span">{{count(Auth::User()->un_read)}}</span>
                    @endif
                   </center>
                   <h4 class="text-uppercase">Messages</h4>
                </div>

              </div>
            </div>
          </div>
          </a>
        </div>

     <div class="row same-height-cards">

           <div class="col-md-4">
           <a href="#!" data-toggle="modal" data-target="#contact_model">
            <div class="card bg-default text-black" style="max-height: 175px; background-color: white;">
              <div class="p-x p-b">
                <div class="text-center" style="margin:30px 0;">
                   <center><img src="{{url('admin/images/contact.png')}}" class="img-responsive m-a-0 text-uppercase"></h2></center>
                   <h4 class="text-uppercase">Contact Khan Law</h4>
                </div>

              </div>
            </div>
            </a>
          </div>
          <div class="col-md-4">
          <a href="@if(count(Auth::User()->projects) > 0) {{url('project/tracker/page')}} @else{{'#!'}}@endif">
            <div class="card bg-default text-black" style="max-height: 175px; background-color: white;">
              <div class="p-x p-b">
                <div class="text-center" style="margin:30px 0;">
                   <center><img src="{{url('admin/images/messages.png')}}" class="img-responsive m-a-0 text-uppercase">
                   @if(Auth::User()->new_projects() > 0)
                      <span class="notification_span">{{Auth::User()->new_projects()}}</span>
                    @endif
                   </center>
                   <h4 class="text-uppercase">Tracker</h4>
                </div>

              </div>
            </div>
          </div>
          </a>
        </div>

    </div>



    @endif

    @if(Auth::User()->user_role_idFk == 2 || Auth::User()->user_role_idFk == 5)

    <div class="row same-height-cards">

        <div class="col-md-4">
            <a href="{{url('WelcomeLetter.doc')}}" download>
              <div class="card bg-default text-black" style="max-height: 175px; background-color: white;">
                <div class="p-x p-b">

                  <div class="text-center" style="margin:30px 0;">

                     <center><img src="{{url('admin/images/deals.png')}}" class="img-responsive m-a-0 text-uppercase"></h2></center>
                     <h4 class="text-uppercase">Welcome Letter</h4>

                  </div>

                </div>
              </div>
            </a>
          </div>

        @if(Auth::User()->user_role_idFk == 2)
          <div class="col-md-4">
            <a href="@if(count(Auth::User()->projects) > 0 && count(Auth::User()->projects) == 1) {{url('chat/dashboard/'.Auth::User()->projects[0]->project_idFk)}} @else {{url('portals/list')}} @endif">
            <div class="card bg-default text-black" style="max-height: 175px; background-color: white;">
              <div class="p-x p-b">
                <div class="text-center" style="margin:30px 0;">
                   <center><img src="{{url('admin/images/messages.png')}}" class="img-responsive m-a-0 text-uppercase">
                   @if(count(Auth::User()->un_read) > 0)
                      <span class="notification_span">{{count(Auth::User()->un_read)}}</span>
                    @endif
                   </center>
                   <h4 class="text-uppercase">Messages</h4>
                </div>

              </div>
            </div>
          </div>
          </a>
          </div>
        @endif

     <div class="row same-height-cards">

           <div class="col-md-4">
            <a href="#!" data-toggle="modal" data-target="#contact_model">
              <div class="card bg-default text-black" style="max-height: 175px; background-color: white;">
              <div class="p-x p-b">
                <div class="text-center" style="margin:30px 0;">
                   <center><img src="{{url('admin/images/contact.png')}}" class="img-responsive m-a-0 text-uppercase"></h2></center>
                   <h4 class="text-uppercase">Contact Khan Law</h4>
                </div>

              </div>
            </div>
            </a>
          </div>
          <div class="col-md-4">
            @if(Auth::User()->user_role_idFk == 5)
            <a href="{{url('realtors/list/'.Auth::User()->user_id)}}">
              <div class="card bg-default text-black" style="max-height: 175px; background-color: white;">
                <div class="p-x p-b">
                  <div class="text-center" style="margin:30px 0;">
                    <center><img src="{{url('admin/images/clients.png')}}" class="img-responsive m-a-0 text-uppercase">
                      <span class="notification_span">{{count(App\Realtors::where('broker_idFk', Auth::User()->user_id)->get())}}</span>
                    </center>
                    <h4 class="text-uppercase">List of Realtors</h4>
                  </div>

                </div>
              </div>
            </a>
            @endif
        </div>

        <div class="row ">

          @if(count(Auth::User()->projects) == 1)


            <div class="col-md-8">
              <div class="row">
                <div class="col-md-6">
                  <nav role="navigation" style="background-color: white;border: 1px solid #e0e0e0;">
                    <ul class="nav nav-stacked nav-pills">

                      <li>
                        <a href="javascript:;">
                          <i class="icon-tag text-warning"></i>
                          <span>File No:</span><br>
                              &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<span style="font-size: 20px;">&#10551;</span>
                              <span>{{Auth::User()->client_project->project->file_no}}</span><br>
                        </a>
                      </li>
                      @if(count(Auth::User()->client_project->project) > 0)
                        @if(count(Auth::User()->client_project->project->agent) > 0)
                            @foreach(Auth::User()->client_project->project->agent as $agent)
                              <li>
                                <a href="{{url('user/view/'.$agent->user_idFk)}}">
                                  <i class="icon-tag text-warning"></i>
                                  <span>Realtor</span><br>

                                      &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<span style="font-size: 20px;">&#10551;</span>
                                      <span>{{$agent->name}}</span><br>


                                </a>
                              </li>
                            @endforeach
                        @endif
                      @else

                      @endif
                      @if(count(Auth::User()->client_project->project) > 0)
                        @if(count(Auth::User()->client_project->project->mortgage) > 0)
                          @foreach(Auth::User()->client_project->project->mortgage as $mortgage)
                            <li>
                              <a href="{{url('user/view/'.$mortgage->user_idFk)}}">
                                <i class="icon-tag text-danger"></i>
                                <span>Lender</span><br>

                                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<span style="font-size: 20px;">&#10551;</span>
                                    <span>{{$mortgage->name}}</span><br>

                              </a>
                            </li>
                          @endforeach
                        @endif
                      @else

                      @endif
                      @if(count(Auth::User()->client_project->project) > 0)
                        @if(count(Auth::User()->client_project->project->laywer) > 0)
                          @foreach(Auth::User()->client_project->project->laywer as $laywer)
                            <li>
                              <a href="{{url('user/view/'.$laywer->user_idFk)}}">
                                <i class="icon-tag text-danger"></i>
                                <span>Laywer</span><br>

                                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<span style="font-size: 20px;">&#10551;</span>
                                    <span>{{$laywer->name}}</span><br>

                              </a>
                            </li>
                          @endforeach
                        @endif
                      @else

                      @endif

                    </ul>
                  </nav>

                </div>
                <div class="col-md-6">
                  <nav role="navigation" style="background-color: white;border: 1px solid #e0e0e0;">
                    <ul class="nav nav-stacked nav-pills">

                      <li>
                        <a href="javascript:;">
                          <i class="icon-tag text-warning"></i>
                          <span>Address:</span><br>
                              &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<span style="font-size: 20px;">&#10551;</span>
                              <span>{{Auth::User()->client_project->project->title}}</span><br>
                        </a>
                      </li>
                      <li>
                        <a href="javascript:;">
                          <i class="icon-tag text-warning"></i>
                          <span>Type</span><br>
                              &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<span style="font-size: 20px;">&#10551;</span>
                              <span>{{Auth::User()->client_project->project->project_type}}</span><br>
                        </a>
                      </li>
                      <li>
                        <a href="javascript:;">
                          <i class="icon-tag text-danger"></i>
                          <span>Closing Date</span><br>
                              &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<span style="font-size: 20px;">&#10551;</span>
                              <span>{{date('d F, Y', strtotime(Auth::User()->client_project->project->closing_date))}}</span><br>
                        </a>
                      </li>

                    </ul>
                  </nav>

                </div>

              </div>
            </div>


          @else

            @if(count(Auth::User()->projects) > 1)

              <div class="col-md-4">
                <a href="{{url('projects/list')}}">
                  <div class="card bg-default text-black" style="max-height: 175px; background-color: white;">
                    <div class="p-x p-b">

                      <div class="text-center" style="margin:30px 0;">

                         <center><img src="{{url('admin/images/deals.png')}}" class="img-responsive m-a-0 text-uppercase">
                         @if(Auth::User()->new_projects() > 0)
                        <span class="notification_span">{{count(Auth::User()->new_projects())}}</span>
                      @endif
                         </center>
                         <h4 class="text-uppercase">Closings</h4>

                      </div>

                    </div>
                  </div>
                </a>
              </div>

              @endif

          @endif

        </div>

    </div>



    @endif

@stop

@section('foot')

  @parent

@endsection
