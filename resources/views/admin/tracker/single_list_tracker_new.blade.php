@extends('admin.layout')

@section('title', 'Trackers ')


@section('content')

  <?php $name = $pro->title.' > '.$pro->project_type;?>

  @section('page_name', $name)

@if(session('success'))
  <div class="alert alert-success">
    {{session('success')}}
  </div>
@endif
@if(session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
@endif

<style type="text/css">
  .tracker_card{
    text-align: center;
  }
  .tracker_card > img{
    width: 50%;
  }
  .content > h6 > p{
    height: 25px;
    width: 25px;
    display: block;
    margin: 0px;
    float: left;
    background-color: black;
    width: 25px;
    height: 25px;
    border-radius: 50%;
    color: white;
    padding: 7px;
  }
  .project_ul{
    height: 450px;
    max-height: 450px;
    overflow: auto;
    padding: 0px;
  }
  .project_ul > li{
    list-style: none;
    background-color: #f5f5f5;
    padding: 5px 15px;
    border-bottom: 2px solid gray
  }
  .project_ul > li:hover{
    background-color: #f5f5f5 !important;
  }
  .project_ul > li > a{


  }
</style>

<div class="card bg-white" data-ng-controller="tableCtrl">
  <div class="card-header">
    Trackers
  </div>
  <div class="card-block">


    <div class="row">

    @if(Auth::User()->user_role_idFk != 2 && Auth::User()->user_role_idFk != 3 && Auth::User()->user_role_idFk != 4 && count($projects) == 1)

      {{-- <div class="col-md-2">
        <h4>Project List</h4>
        <ul class="project_ul">
          @if(count($projects) > 0)
            @foreach($projects as $proj)

              <li>
                <a href="{{url('project/tracker/'.$proj->project_id)}}">{{$proj->title}}</a>
              </li>

            @endforeach
          @endif
        </ul>

      </div> --}}

      <div @if(Auth::User()->user_role_idFk == 2 || Auth::User()->user_role_idFk == 3  || Auth::User()->user_role_idFk == 4) class="col-md-10" @endif>

          @if($pro->project_type == 'Sale')
            <div class="row">

              <div class="col-md-5">

                <div class="row" style="border: 1px solid gray;border-radius: 10px;padding: 6px 6px 21px 6px;">
                  <div class="col-md-4">
                    <div class="tracker_card">
                      @if($pro->status(1) == '1')
                        <img src="{{url('admin/images/sale/1gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/1.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(1) == '1') background-color: #2c9063; @endif">1</p> Khan Law Receives Purchased Contact ('A Firm Deal')</h6>
                      </div>
                    </div>
                  </div>


                  <div class="col-md-4">
                    <div class="tracker_card">
                      @if($pro->status(2) == '1')
                        <img src="{{url('admin/images/sale/2gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/2.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(2) == '1') background-color: #2c9063; @endif">2</p> Khan Law Contacts Client To Collect Important ID Verification Details</h6>
                      </div>
                    </div>
                  </div>


                  <div class="col-md-4">
                    <div class="tracker_card">
                      @if($pro->status(3) == '1')
                        <img src="{{url('admin/images/sale/3gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/3.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(3) == '1') background-color: #2c9063; @endif">3</p> Khan Law Requests Mortgage/LOC Pay-out Statment</h6>
                      </div>
                    </div>
                  </div>
                </div>

              </div>

              <div class="col-md-7">
                <div class="row" style="border: 1px solid gray;border-radius: 10px;padding: 6px;margin-left: 10px;">

                  <div class="col-md-3">
                    <div class="tracker_card">
                      @if($pro->status(4) == '1')
                        <img src="{{url('admin/images/sale/4gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/4.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(4) == '1') background-color: #2c9063; @endif">4</p> Khan Law Responds To Buyer's Lawyer's Title Requisitions</h6>
                      </div>
                    </div>
                  </div>


                  <div class="col-md-3">
                    <div class="tracker_card">
                      @if($pro->status(5) == '1')
                        <img src="{{url('admin/images/sale/5gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/5.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(5) == '1') background-color: #2c9063; @endif">5</p> Khan Law Prepares Sale Documents</h6>
                      </div>
                    </div>
                  </div>


                  <div class="col-md-3">
                    <div class="tracker_card">
                      @if($pro->status(6) == '1')
                        <img src="{{url('admin/images/sale/6gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/6.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(6) == '1') background-color: #2c9063; @endif">6</p> Client Meeting With Khan Law Lawyer To Sign Documents (45 Mins)</h6>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="tracker_card">
                      @if($pro->status(7) == '1')
                        <img src="{{url('admin/images/sale/7gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/7.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(7) == '1') background-color: #2c9063; @endif">7</p> Khan Law Sends Documents To Buyer's Lawyer For Signing And Registration</h6>
                      </div>
                    </div>
                  </div>

                </div>
              </div>


            </div>

            <div class="row">

              <div class="col-md-7 col-md-offset-2">

              <h6 style="text-align:center;margin-top: 20px;">Closing Day</h6>

                <div class="row" style="border: 1px solid gray;border-radius: 10px;padding: 6px;">

                  <div class="col-md-3">
                    <div class="tracker_card">
                      @if($pro->status(9) == '1')
                        <img src="{{url('admin/images/sale/9gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/9.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(9) == '1') background-color: #2c9063; @endif">9</p> Khan Law Receives Proceeds From Buyer's Lawyer</h6>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="tracker_card">
                      @if($pro->status(10) == '1')
                        <img src="{{url('admin/images/sale/10gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/10.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(10) == '1') background-color: #2c9063; @endif">10</p> Khan Law Pays Out Existing Mortgage, Realty Commission etc, And Reimburses Proceeds To Client</h6>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="tracker_card">
                      @if($pro->status(11) == '1')
                        <img src="{{url('admin/images/sale/11gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/11.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(11) == '1') background-color: #2c9063; @endif">11</p> Khan Law Discharges Seller's Financial Encumbrances And Completes All Outstanding Matters</h6>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="tracker_card">
                      @if($pro->status(12) == '1')
                        <img src="{{url('admin/images/sale/12gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/12.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(12) == '1') background-color: #2c9063; @endif">12</p> Khan Law Reports To Client</h6>
                      </div>
                    </div>
                  </div>

                </div>

              </div>

            </div>

          @elseif($pro->project_type == 'Purchase')


            <div class="row">

              <div class="col-md-4">

                  <div class="row" style="border: 1px solid gray;border-radius: 10px;padding: 6px 6px 21px 6px;">

                    <div class="col-md-6">
                      <div class="tracker_card">
                        @if($pro->status(1) == '1')
                          <img src="{{url('admin/images/purchase/1gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/1.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(1) == '1') background-color: #2c9063; @endif">1</p> Khan Law Receives Purchased Contact ('A Firm Deal')</h6>
                        </div>
                      </div>
                    </div>


                    <div class="col-md-6">
                      <div class="tracker_card">
                        @if($pro->status(2) == '1')
                          <img src="{{url('admin/images/purchase/2gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/2.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(2) == '1') background-color: #2c9063; @endif">2</p> Khan Law Contacts Client To Collect Important ID Verification Details</h6>
                        </div>
                      </div>
                    </div>

                  </div>

              </div>

              <div class="col-md-8">

                  <div class="row" style="border: 1px solid gray;border-radius: 10px;padding: 6px 6px 21px 6px;margin-left:15px;">

                    <div class="col-md-3">
                      <div class="tracker_card">
                        @if($pro->status(3) == '1')
                          <img src="{{url('admin/images/purchase/3gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/3.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(3) == '1') background-color: #2c9063; @endif">3</p> Khan Law Completes A Title Search To Confirm Title Qualifications On The Property</h6>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="tracker_card">
                        @if($pro->status(4) == '1')
                          <img src="{{url('admin/images/purchase/4gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/4.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(4) == '1') background-color: #2c9063; @endif">4</p> Khan Law Sends Requisitions To Seller's Lawyer</h6>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="tracker_card">
                        @if($pro->status(5) == '1')
                          <img src="{{url('admin/images/purchase/5gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/5.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(5) == '1') background-color: #2c9063; @endif">5</p> Lender (Bank) Sends Mortgage Instructions To Khan Law</h6>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="tracker_card">
                        @if($pro->status(6) == '1')
                          <img src="{{url('admin/images/purchase/6gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/6.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(6) == '1') background-color: #2c9063; @endif">6</p> Khan Law Prepares Closing And Mortgage Documents</h6>
                        </div>
                      </div>
                    </div>


                  </div>

              </div>


            </div>


            <div class="row" style="margin-top:15px;">

              <div class="col-md-4">

                  <div class="row" style="border: 1px solid gray;border-radius: 10px;padding: 6px 6px 21px 6px;margin-top: 33px;">

                    <div class="col-md-6">
                      <div class="tracker_card">
                        @if($pro->status(7) == '1')
                          <img src="{{url('admin/images/purchase/7gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/7.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(7) == '1') background-color: #2c9063; @endif">7</p> Client Meeting With Khan Law Lawyer To Sign Documents (45 Mins)</h6>
                        </div>
                      </div>
                    </div>


                    <div class="col-md-6">
                      <div class="tracker_card">
                        @if($pro->status(8) == '1')
                          <img src="{{url('admin/images/purchase/8gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/8.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(8) == '1') background-color: #2c9063; @endif">8</p> Khan Law Fulfills All Lender (Bank) Requirements And Submit Executed Documents</h6>
                        </div>
                      </div>
                    </div>

                  </div>

              </div>

              <div class="col-md-8">
                  <h6 style="text-align:center;">Closing Day</h6>
                  <div class="row" style="border: 1px solid gray;border-radius: 10px;padding: 6px 6px 21px 6px;margin-left:15px;">

                    <div class="col-md-3">
                      <div class="tracker_card">
                        @if($pro->status(9) == '1')
                          <img src="{{url('admin/images/purchase/9gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/9.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(9) == '1') background-color: #2c9063; @endif">9</p> Khan Law Receives Mortgage Proceeds And Releases Funds To Seller's Lawyer</h6>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="tracker_card">
                        @if($pro->status(10) == '1')
                          <img src="{{url('admin/images/purchase/10gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/10.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(10) == '1') background-color: #2c9063; @endif">10</p> Khan Law Register The Transfer Deed</h6>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="tracker_card">
                        @if($pro->status(11) == '1')
                          <img src="{{url('admin/images/purchase/11gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/11.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(11) == '1') background-color: #2c9063; @endif">11</p> Khan Law Releases The Keys To The New Buyer's</h6>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="tracker_card">
                        @if($pro->status(12) == '1')
                          <img src="{{url('admin/images/purchase/12gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/12.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(12) == '1') background-color: #2c9063; @endif">12</p> Khan Law Reports To Client And Lender (Bank)</h6>
                        </div>
                      </div>
                    </div>


                  </div>

              </div>


            </div>


          @endif


      </div>


    @else

      @if(count(Auth::User()->projects) > 1)

          <div class="col-md-2">
            <h4>Project List</h4>
            <ul class="project_ul">
              @if(count($projects) > 0)
                @foreach($projects as $proj)

                  <li>
                    <a href="{{url('project/tracker/'.$proj->project_id)}}">{{$proj->title}}</a>
                  </li>

                @endforeach
              @endif
            </ul>

          </div>

      @endif

      <div @if(Auth::User()->user_role_idFk == 2 || Auth::User()->user_role_idFk == 3  || Auth::User()->user_role_idFk == 4) @if(count(Auth::User()->projects) > 1) class="col-md-10" @else class="col-md-12" @endif @else class="col-md-10" @endif>

          @if($pro->project_type == 'Sale')
            <div class="row">

              <div class="col-md-5">

                <div class="row" style="border: 1px solid gray;border-radius: 10px;padding: 6px 6px 21px 6px;">
                  <div class="col-md-4">
                    <div class="tracker_card">
                      @if($pro->status(1) == '1')
                        <img src="{{url('admin/images/sale/1gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/1.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(1) == '1') background-color: #2c9063; @endif">1</p> Khan Law Receives Purchased Contact ('A Firm Deal')</h6>
                      </div>
                    </div>
                  </div>


                  <div class="col-md-4">
                    <div class="tracker_card">
                      @if($pro->status(2) == '1')
                        <img src="{{url('admin/images/sale/2gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/2.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(2) == '1') background-color: #2c9063; @endif">2</p> Khan Law Contacts Client To Collect Important ID Verification Details</h6>
                      </div>
                    </div>
                  </div>


                  <div class="col-md-4">
                    <div class="tracker_card">
                      @if($pro->status(3) == '1')
                        <img src="{{url('admin/images/sale/3gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/3.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(3) == '1') background-color: #2c9063; @endif">3</p> Khan Law Requests Mortgage/LOC Pay-out Statment</h6>
                      </div>
                    </div>
                  </div>
                </div>

              </div>

              <div class="col-md-7">
                <div class="row" style="border: 1px solid gray;border-radius: 10px;padding: 6px;margin-left: 10px;">

                  <div class="col-md-3">
                    <div class="tracker_card">
                      @if($pro->status(4) == '1')
                        <img src="{{url('admin/images/sale/4gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/4.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(4) == '1') background-color: #2c9063; @endif">4</p> Khan Law Responds To Buyer's Lawyer's Title Requisitions</h6>
                      </div>
                    </div>
                  </div>


                  <div class="col-md-3">
                    <div class="tracker_card">
                      @if($pro->status(5) == '1')
                        <img src="{{url('admin/images/sale/5gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/5.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(5) == '1') background-color: #2c9063; @endif">5</p> Khan Law Prepares Sale Documents</h6>
                      </div>
                    </div>
                  </div>


                  <div class="col-md-3">
                    <div class="tracker_card">
                      @if($pro->status(6) == '1')
                        <img src="{{url('admin/images/sale/6gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/6.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(6) == '1') background-color: #2c9063; @endif">6</p> Client Meeting With Khan Law Lawyer To Sign Documents (45 Mins)</h6>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="tracker_card">
                      @if($pro->status(7) == '1')
                        <img src="{{url('admin/images/sale/7gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/7.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(7) == '1') background-color: #2c9063; @endif">7</p> Khan Law Sends Documents To Buyer's Lawyer For Signing And Registration</h6>
                      </div>
                    </div>
                  </div>

                </div>
              </div>


            </div>

            <div class="row">

              <div class="col-md-7 col-md-offset-2">

              <h6 style="text-align:center;margin-top: 20px;">Closing Day</h6>

                <div class="row" style="border: 1px solid gray;border-radius: 10px;padding: 6px;">

                  <div class="col-md-3">
                    <div class="tracker_card">
                      @if($pro->status(9) == '1')
                        <img src="{{url('admin/images/sale/9gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/9.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(9) == '1') background-color: #2c9063; @endif">9</p> Khan Law Receives Proceeds From Buyer's Lawyer</h6>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="tracker_card">
                      @if($pro->status(10) == '1')
                        <img src="{{url('admin/images/sale/10gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/10.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(10) == '1') background-color: #2c9063; @endif">10</p> Khan Law Pays Out Existing Mortgage, Realty Commission etc, And Reimburses Proceeds To Client</h6>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="tracker_card">
                      @if($pro->status(11) == '1')
                        <img src="{{url('admin/images/sale/11gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/11.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(11) == '1') background-color: #2c9063; @endif">11</p> Khan Law Discharges Seller's Financial Encumbrances And Completes All Outstanding Matters</h6>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="tracker_card">
                      @if($pro->status(12) == '1')
                        <img src="{{url('admin/images/sale/12gr.png')}}">
                      @else
                        <img src="{{url('admin/images/sale/12.png')}}">
                      @endif
                      <div class="content">
                        <h6><p style="@if($pro->status(12) == '1') background-color: #2c9063; @endif">12</p> Khan Law Reports To Client</h6>
                      </div>
                    </div>
                  </div>

                </div>

              </div>

            </div>

          @elseif($pro->project_type == 'Purchase')


            <div class="row">

              <div class="col-md-4">

                  <div class="row" style="border: 1px solid gray;border-radius: 10px;padding: 6px 6px 21px 6px;">

                    <div class="col-md-6">
                      <div class="tracker_card">
                        @if($pro->status(1) == '1')
                          <img src="{{url('admin/images/purchase/1gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/1.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(1) == '1') background-color: #2c9063; @endif">1</p> Khan Law Receives Purchased Contact ('A Firm Deal')</h6>
                        </div>
                      </div>
                    </div>


                    <div class="col-md-6">
                      <div class="tracker_card">
                        @if($pro->status(2) == '1')
                          <img src="{{url('admin/images/purchase/2gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/2.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(2) == '1') background-color: #2c9063; @endif">2</p> Khan Law Contacts Client To Collect Important ID Verification Details</h6>
                        </div>
                      </div>
                    </div>

                  </div>

              </div>

              <div class="col-md-8">

                  <div class="row" style="border: 1px solid gray;border-radius: 10px;padding: 6px 6px 21px 6px;margin-left:15px;">

                    <div class="col-md-3">
                      <div class="tracker_card">
                        @if($pro->status(3) == '1')
                          <img src="{{url('admin/images/purchase/3gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/3.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(3) == '1') background-color: #2c9063; @endif">3</p> Khan Law Completes A Title Search To Confirm Title Qualifications On The Property</h6>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="tracker_card">
                        @if($pro->status(4) == '1')
                          <img src="{{url('admin/images/purchase/4gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/4.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(4) == '1') background-color: #2c9063; @endif">4</p> Khan Law Sends Requisitions To Seller's Lawyer</h6>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="tracker_card">
                        @if($pro->status(5) == '1')
                          <img src="{{url('admin/images/purchase/5gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/5.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(5) == '1') background-color: #2c9063; @endif">5</p> Lender (Bank) Sends Mortgage Instructions To Khan Law</h6>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="tracker_card">
                        @if($pro->status(6) == '1')
                          <img src="{{url('admin/images/purchase/6gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/6.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(6) == '1') background-color: #2c9063; @endif">6</p> Khan Law Prepares Closing And Mortgage Documents</h6>
                        </div>
                      </div>
                    </div>


                  </div>

              </div>


            </div>


            <div class="row" style="margin-top:15px;">

              <div class="col-md-4">

                  <div class="row" style="border: 1px solid gray;border-radius: 10px;padding: 6px 6px 21px 6px;margin-top: 33px;">

                    <div class="col-md-6">
                      <div class="tracker_card">
                        @if($pro->status(7) == '1')
                          <img src="{{url('admin/images/purchase/7gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/7.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(7) == '1') background-color: #2c9063; @endif">7</p> Client Meeting With Khan Law Lawyer To Sign Documents (45 Mins)</h6>
                        </div>
                      </div>
                    </div>


                    <div class="col-md-6">
                      <div class="tracker_card">
                        @if($pro->status(8) == '1')
                          <img src="{{url('admin/images/purchase/8gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/8.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(8) == '1') background-color: #2c9063; @endif">8</p> Khan Law Fulfills All Lender (Bank) Requirements And Submit Executed Documents</h6>
                        </div>
                      </div>
                    </div>

                  </div>

              </div>

              <div class="col-md-8">
                  <h6 style="text-align:center;">Closing Day</h6>
                  <div class="row" style="border: 1px solid gray;border-radius: 10px;padding: 6px 6px 21px 6px;margin-left:15px;">

                    <div class="col-md-3">
                      <div class="tracker_card">
                        @if($pro->status(9) == '1')
                          <img src="{{url('admin/images/purchase/9gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/9.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(9) == '1') background-color: #2c9063; @endif">9</p> Khan Law Receives Mortgage Proceeds And Releases Funds To Seller's Lawyer</h6>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="tracker_card">
                        @if($pro->status(10) == '1')
                          <img src="{{url('admin/images/purchase/10gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/10.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(10) == '1') background-color: #2c9063; @endif">10</p> Khan Law Register The Transfer Deed</h6>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="tracker_card">
                        @if($pro->status(11) == '1')
                          <img src="{{url('admin/images/purchase/11gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/11.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(11) == '1') background-color: #2c9063; @endif">11</p> Khan Law Releases The Keys To The New Buyer's</h6>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="tracker_card">
                        @if($pro->status(12) == '1')
                          <img src="{{url('admin/images/purchase/12gr.png')}}">
                        @else
                          <img src="{{url('admin/images/purchase/12.png')}}">
                        @endif
                        <div class="content">
                          <h6><p style="@if($pro->status(12) == '1') background-color: #2c9063; @endif">12</p> Khan Law Reports To Client And Lender (Bank)</h6>
                        </div>
                      </div>
                    </div>


                  </div>

              </div>


            </div>


          @endif


      </div>

    @endif

    </div>

  </div>
</div>



@stop

@section('foot')

  @parent
   <script src="{{url('admin/vendor/datatables/media/js/jquery.dataTables.js')}}"></script>
  <script src="{{url('admin/vendor/datatables/media/js/datatables.bootstrap.js')}}"></script>


@endsection
