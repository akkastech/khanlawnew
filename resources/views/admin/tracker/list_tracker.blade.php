@extends('admin.layout')

@section('title', 'Trackers ')


@section('content')



@if(session('success'))
  <div class="alert alert-success">
    {{session('success')}}
  </div>
@endif
@if(session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
@endif

<style type="text/css">
  .tracker_card{
    text-align: center;
  }
  .tracker_card > img{
    width: 50%;
  }
  .content > h6 > p{
    height: 25px;
    width: 25px;
    display: block;
    margin: 0px;
    float: left;
    background-color: black;
    width: 25px;
    height: 25px;
    border-radius: 50%;
    color: white;
    padding: 7px;
  }
  .project_ul{
    height: 450px;
    max-height: 450px;
    overflow: auto;
    padding: 0px;
  }
  .project_ul > li{
    list-style: none;
    background-color: #f5f5f5;
    padding: 5px 15px;
    border-bottom: 2px solid gray
  }
  .project_ul > li:hover{
    background-color: #f5f5f5 !important;
  }
  .project_ul > li > a{


  }
</style>

<div class="card bg-white" data-ng-controller="tableCtrl">
  <div class="card-header">
    Trackers
  </div>
  <div class="card-block">


    <div class="row">

    @if(Auth::User()->user_role_idFk == 1 || Auth::User()->user_role_idFk == 6 || Auth::User()->user_role_idFk == 7)

      <div class="col-md-2">
        <h4>Project List</h4>
        <ul class="project_ul">
          @if(count($projects) > 0)
            @foreach($projects as $proj)

              <li>
                <a href="{{url('project/tracker/'.$proj->project_id)}}">{{$proj->title}}</a>
              </li>

            @endforeach
          @endif
        </ul>

      </div>


    @else

      @if(count(Auth::User()->projects) > 0)

          <div class="col-md-2">
            <h4>Project List</h4>
            <ul class="project_ul">
              @if(count($projects) > 0)
                @foreach($projects as $proj)

                  <li>
                    <a href="{{url('project/tracker/'.$proj->project_id)}}">{{$proj->title}}</a>
                  </li>

                @endforeach
              @endif
            </ul>

          </div>

      @endif

    @endif



    </div>

  </div>
</div>



@stop

@section('foot')

  @parent
   <script src="{{url('admin/vendor/datatables/media/js/jquery.dataTables.js')}}"></script>
  <script src="{{url('admin/vendor/datatables/media/js/datatables.bootstrap.js')}}"></script>


@endsection
