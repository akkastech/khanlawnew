
@extends('admin.layout')

@section('title', 'Brokers List')


@section('content')

  @section('page_name', 'Brokers List')

@if(session('success'))
  <div class="alert alert-success">
    {{session('success')}}
  </div>
@endif
@if(session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
@endif

<div class="card bg-white" data-ng-controller="tableCtrl">

  <div class="card-block">
    <table class="table table-bordered table-condensed datatable m-b-0" ui-jq="dataTable" ui-options="dataTableOpt">
      <thead>
        <tr>
          <th>Name</th>
          <th>Status</th>
          <th>Email</th>
          <th>Created At</th>
          <th>Realtors</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @if(count($brokers) > 0)

          @foreach($brokers as $user)
            <tr>
              <td><a href="{{url('user/view/'.$user->user_id)}}">{{$user->name}}</a></td>
              <td>
                @if($user->user_status == '1')
                  {{'Active'}}
                @else
                  {{'Deactive'}}
                @endif
              </td>
              <td>{{$user->email}}</td>
              <td>{{date('d F, Y', strtotime($user->created_at))}}</td>
              <td>{{ count(App\Realtors::where('broker_idFk', $user->user_id)->get()) }}</td>
              <td><a href="{{url('realtors/list/'.$user->user_id)}}"><button type="button" name="button">View Realtors</button></a></td>
            </tr>
          @endforeach
        @else
          <tr>
              <td colspan="4">Record Not Found</td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>



@stop

@section('foot')

  @parent
   <script src="{{url('admin/vendor/datatables/media/js/jquery.dataTables.js')}}"></script>
  <script src="{{url('admin/vendor/datatables/media/js/datatables.bootstrap.js')}}"></script>

  <script type="text/javascript">
    $('.datatable').dataTable({
        "scrollX": true
    } );
  </script>

@endsection
