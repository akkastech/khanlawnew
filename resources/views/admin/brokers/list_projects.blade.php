
@extends('admin.layout')

@section('title', 'Project List ')


@section('content')

  @section('page_name', 'Project List')

@if(session('success'))
  <div class="alert alert-success">
    {{session('success')}}
  </div>
@endif
@if(session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
@endif

<div class="card bg-white" data-ng-controller="tableCtrl">
  <div class="card-header">
    Project List @if(Auth::User()->user_role_idFk == 1 || Auth::User()->user_role_idFk == 6 || Auth::User()->user_role_idFk == 7)
            <a href="{{url('project/add')}}" class="pull-right btn btn-info">Add Project</a>
          @endif
  </div>
  <div class="card-block">
    <table class="table table-bordered table-condensed datatable m-b-0" ui-jq="dataTable" ui-options="dataTableOpt">
      <thead>
        <tr>
          <th>File No</th>
          <th>Title</th>
          <th>Type</th>
          <!-- <th>Opening Date</th> -->
          <th>Closing Date</th>
          <th>Client</th>
          @if(Auth::User()->user_role_idFk != 3)
          <th>Realtor</th>
          @endif
          @if(Auth::User()->user_role_idFk != 4)
          <th>Lender</th>
          @endif
          <th>Status</th>
          @if(Auth::User()->user_role_idFk == 1 || Auth::User()->user_role_idFk == 6 || Auth::User()->user_role_idFk == 7)
            <th>Action</th>
          @elseif(Auth::User()->user_role_idFk == 3 || Auth::User()->user_role_idFk == 4 || Auth::User()->user_role_idFk == 2)
            <th>Messages</th>
          @endif
        </tr>
      </thead>
      <tbody>
        @if(count($projects) > 0)

          @foreach($projects as $project)
            <tr>
              <td>{{$project->project->file_no}}</td>
              <td>{{$project->project->title}}</td>
              <td>{{$project->project->project_type}}</td>
              <!-- <td>{{date('d F, Y', strtotime($project->opening_date))}}</td> -->
              <td>{{date('d F, Y', strtotime($project->project->closing_date))}}</td>
              <td>
              @if($project->project->client)
                @foreach($project->project->client as $key => $client)

                  @if(Auth::User()->user_id != $client->user_idFk)

                    @if($key == 0)
                      {{$client->name}}
                    @else
                    , {{$client->name}}</a>
                    @endif

                  @endif

                @endforeach
              @endif
              </td>

              @if($project->project->agent)
              @foreach($project->project->agent as $key => $agent)

                @if(Auth::User()->user_id != $agent->user_idFk)
                    <td>



                        @if($key == 0)
                          {{$agent->name}}
                        @else
                        , {{$agent->name}}
                        @endif


                      </td>
                @endif

              @endforeach

              @endif
              <td>
                @if($project->project->mortgage)
                  @foreach($project->project->mortgage as $key => $mortgage)

                    @if(Auth::User()->user_id != $mortgage->user_idFk)

                        @if($key == 0)
                          {{$mortgage->name}}
                        @else
                        , {{$mortgage->name}}
                        @endif

                    @endif

                  @endforeach
                @endif

              </td>
              <!-- <td>
                @if($project->opposition)
                  @foreach($project->opposition as $key => $opposition)
                    @if($key == 0)
                      {{$opposition->name}}
                    @else
                    , {{$opposition->name}}
                    @endif
                  @endforeach
                @endif
              </td> -->

              <td style="text-align:center;">

              @if(Auth::User()->user_role_idFk == 1 || Auth::User()->user_role_idFk == 6 || Auth::User()->user_role_idFk == 7)
                <a href="{{url('project/status/'.$project->project->project_id)}}">
                  <i style="font-size:26px;color:#09cc09;" class="fa fa-eye" aria-hidden="true"></i>
                </a>
              @elseif(Auth::User()->user_role_idFk == 3 || Auth::User()->user_role_idFk == 4 || Auth::User()->user_role_idFk == 2  || Auth::User()->user_role_idFk == 5)
                <a href="{{url('project/tracker/'.$project->project->project_id)}}">
                  <i style="font-size:26px;color:#09cc09;" class="fa fa-eye" aria-hidden="true"></i>
                </a>
              @endif
              </td>
                @if(Auth::User()->user_role_idFk == 1 || Auth::User()->user_role_idFk == 6 || Auth::User()->user_role_idFk == 7)
                  <td>
                    <div class="row">
                      {{ $pro = new App\Projects}}
                      <div class="col-md-4">
                        <a @if($pro->check_complete($project->project->project_id) == 'false') title="Project Not Completed Yet" href="#!" @else href="{{url('project/complete/'.$project->project->project_id)}}" title="Click To Compelete" @endif   disabled="disabled">
                            <i class="fa fa-check" aria-hidden="true"  style="font-size:20px;color:#09cc09;"></i>
                        </a>
                      </div>
                      <div class="col-md-4">
                        <a href="{{url('project/edit/'.$project->project->project_id)}}" >
                            <i class="fa fa-pencil" aria-hidden="true" style="font-size:20px;color:#1ebcfb;"></i>
                        </a>
                      </div>
                      <div class="col-md-4">
                        <a href="{{url('project/delete/'.$project->project->project_id)}}" >
                            <i class="fa fa-trash-o" aria-hidden="true" style="font-size:20px;color:#fe6767;"></i>
                        </a>
                      </div>
                    </div>
                  </td>

                @elseif(Auth::User()->user_role_idFk == 3 || Auth::User()->user_role_idFk == 4 || Auth::User()->user_role_idFk == 2)
                  <td style="text-align:center;">
                    <a href="{{url('chat/dashboard/'.$project->project->project_id)}}">
                      <i style="font-size:26px;color:#09b0cc;" class="fa fa-envelope" aria-hidden="true"></i>
                    @if($project->un_read)
                      <span style="height:10px;width:10px;background-color:red;padding:5px;border-radius: 50%;display: block;position: relative;top: -28px;right: -54px;"></span>
                    @endif
                    </a>
                  </td>
                @endif
            </tr>
          @endforeach
        @else
          <tr>
              <td colspan="4">Record Not Found</td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>



@stop

@section('foot')

  @parent
   <script src="{{url('admin/vendor/datatables/media/js/jquery.dataTables.js')}}"></script>
  <script src="{{url('admin/vendor/datatables/media/js/datatables.bootstrap.js')}}"></script>

  <script type="text/javascript">
    $('.datatable').dataTable({
        "scrollX": true
      });
  </script>

@endsection
