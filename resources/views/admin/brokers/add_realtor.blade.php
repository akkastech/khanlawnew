
@extends('admin.layout')

@section('title', 'Add Realtor')


@section('content')

  @section('page_name', 'Add Realtor')

  @if(session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
@endif

<div class="card bg-white">

    <div class="card-header">
      Add Realtors
    </div>
    <div class="card-block">

        <form class="form-horizontal" role="form" action="{{url('realtors/submit')}}" method="post">
          <input id="token" name="_token" type="text" value="{!! csrf_token() !!}" hidden>
          <input type="hidden" name="user_id" value="{{$id}}">
          <div class="row m-a-0">
            @if(Auth::User()->user_role_idFk == 3)
              <input type="hidden" name="role" value="2">
            @else
              <div class="col-lg-4">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Realtors</label>
                    <div class="col-sm-8">
                      <select id="role" class="form-control" style="width:100%;" name="realtors[]" required="" multiple>
                        <option value="">Please Select</option>
                        @if(count($users) > 0)
                          @foreach($users as $user)
                              @if(!$user->check_realtor($id, $user->user_id))
                                <option value="{{$user->user_id}}">{{$user->name}}</option>
                              @endif
                          @endforeach
                        @endif
                      </select>
                    </div>
                  </div>
              </div>
            @endif
          </div>
          <div class="card-footer">
            <div class="row">
              <div class="col-md-2">
                <a href="#!" onclick="window.history.back()" class="btn btn-default btn-block ">Cancel</a>
              </div>
              <div class="col-md-2 pull-right">
                <button class="btn btn-info btn-block ">Submit</button>
              </div>
            </div>
          </div>
          </form>
        </div>
@stop

@section('foot')

  @parent
   <script src="{{url('admin/vendor/datatables/media/js/jquery.dataTables.js')}}"></script>
  <script src="{{url('admin/vendor/datatables/media/js/datatables.bootstrap.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


@stop


    