
@extends('admin.layout')

@section('title', 'Brokers List')


@section('content')

  @section('page_name', 'Brokers List')

@if(session('success'))
  <div class="alert alert-success">
    {{session('success')}}
  </div>
@endif
@if(session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
@endif

<div class="card bg-white" data-ng-controller="tableCtrl">
  @if(Auth::User()->user_role_idFk == 1 || Auth::User()->user_role_idFk == 6 || Auth::User()->user_role_idFk == 7)
    <div class="card-header">
      Datatables <a href="{{url('realtors/add/'.$id)}}" class="pull-right btn btn-info">Add Realtors</a>
    </div>
  @endif
  <div class="card-block">
    <table class="table table-bordered table-condensed datatable m-b-0" ui-jq="dataTable" ui-options="dataTableOpt">
      <thead>
        <tr>
          <th>Name</th>
          <th>Email</th>
          <th>Created At</th>
          <th>Status</th>
          @if(Auth::User()->user_role_idFk == 1 || Auth::User()->user_role_idFk == 6 || Auth::User()->user_role_idFk == 7)
          <th>Action</th>
          @endif
        </tr>
      </thead>
      <tbody>
        @if(count($realtors) > 0)

          @foreach($realtors as $user)

            <tr>
              <td><a href="{{url('projects/view/'.$user->user->user_id)}}">{{$user->user->name}}</a></td>
              <td>{{$user->user->email}}</td>
              <td>{{date('d F, Y', strtotime($user->user->created_at))}}</td>
              <td>@if($user->user->user_status == '1')
                {{'Active'}}
              @else
                {{'Deactive'}}
              @endif</td>
              @if(Auth::User()->user_role_idFk == 1 || Auth::User()->user_role_idFk == 6 || Auth::User()->user_role_idFk == 7)
              <td>
                <div class="row">
                  <div class="col-md-4">
                    <a class="delete" href="{{url('realtors/delete/'.$user->user->user_id)}}" >
                        <i class="fa fa-trash-o" aria-hidden="true" style="font-size:20px;color:#fe6767;"></i>
                    </a>
                  </div>
                </div>
              </td>
              @endif
            </tr>
          @endforeach
        @else
          <tr>
              <td colspan="4">Record Not Found</td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>



@stop

@section('foot')

  @parent
   <script src="{{url('admin/vendor/datatables/media/js/jquery.dataTables.js')}}"></script>
  <script src="{{url('admin/vendor/datatables/media/js/datatables.bootstrap.js')}}"></script>

  <script type="text/javascript">
    $('.datatable').dataTable({
        "scrollX": true
    } );
    $('.delete').on('click',function(event) {
        var r = window.confirm('Are you sure to remove this Realtor?');
        if (r != true) {
          event.preventDefault();
        }
    });
  </script>

@endsection
