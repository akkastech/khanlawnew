  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
  <!-- page stylesheets -->
  <!-- end page stylesheets -->
  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="{{url('admin/styles/webfont.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/climacons-font.css')}}">
  <link rel="stylesheet" href="{{url('admin/vendor/bootstrap/dist/css/bootstrap.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/font-awesome.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/card.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/sli.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/animate.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/app.css')}}">
  <link rel="stylesheet" href="{{url('admin/styles/app.skins.css')}}">
  <link rel="stylesheet" href="{{url('admin/vendor/datatables/media/css/dataTables.bootstrap.css')}}">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css">