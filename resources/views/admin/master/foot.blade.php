<!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="{{url('admin/scripts/helpers/modernizr.js')}}"></script>
  <script src="{{url('admin/vendor/jquery/dist/jquery.js')}}"></script>
  <script src="{{url('admin/vendor/bootstrap/dist/js/bootstrap.js')}}"></script>
  <script src="{{url('admin/vendor/fastclick/lib/fastclick.js')}}"></script>
  <script src="{{url('admin/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
  <script src="{{url('admin/scripts/helpers/smartresize.js')}}"></script>
  <script src="{{url('admin/scripts/constants.js')}}"></script>
  <script src="{{url('admin/scripts/main.js')}}"></script>
  <!-- endbuild -->
  <!-- page scripts -->
  <script src="{{url('admin/vendor/flot/jquery.flot.js')}}"></script>
  <script src="{{url('admin/vendor/flot/jquery.flot.resize.js')}}"></script>
  <script src="{{url('admin/vendor/flot/jquery.flot.categories.js')}}"></script>
  <script src="{{url('admin/vendor/flot/jquery.flot.stack.js')}}"></script>
  <script src="{{url('admin/vendor/flot/jquery.flot.time.js')}}"></script>
  <script src="{{url('admin/vendor/flot/jquery.flot.pie.js')}}"></script>
  <script src="{{url('admin/vendor/flot-spline/js/jquery.flot.spline.js')}}"></script>
  <script src="{{url('admin/vendor/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
  <!-- end page scripts -->
  <!-- initialize page scripts -->
  <script src="{{url('admin/scripts/helpers/sameheight.js')}}"></script>
  <!-- <script src="{{url('admin/scripts/ui/dashboard.js')}}"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>