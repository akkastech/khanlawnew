<!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">
      <div class="brand">
        <!-- toggle small sidebar menu -->
        <!-- /toggle small sidebar menu -->
        <!-- toggle offscreen menu -->
        <div class="toggle-offscreen">
          <a href="javascript:;" class="visible-xs hamburger-icon" data-toggle="offscreen" data-move="ltr">
            <span></span>
            <span></span>
            <span></span>
          </a>
        </div>
        <!-- /toggle offscreen menu -->
        <!-- logo -->
        <a class="brand-logo">
          <span>Khan LAW</span>
        </a>
        <a href="#" class="small-menu-visible brand-logo">K</a>
        <!-- /logo -->
      </div>
      <!-- <ul class="quick-launch-apps hide">
        <li>
          <a href="apps-gallery.html">
            <span class="app-icon bg-danger text-white">
            G
            </span>
            <span class="app-title">Gallery</span>
          </a>
        </li>
        <li>
          <a href="apps-messages.html">
            <span class="app-icon bg-success text-white">
            M
            </span>
            <span class="app-title">Messages</span>
          </a>
        </li>
        <li>
          <a href="apps-social.html">
            <span class="app-icon bg-primary text-white">
            S
            </span>
            <span class="app-title">Social</span>
          </a>
        </li>
        <li>
          <a href="apps-travel.html">
            <span class="app-icon bg-info text-white">
            T
            </span>
            <span class="app-title">Travel</span>
          </a>
        </li>
      </ul> -->
      <!-- main navigation -->
      <nav role="navigation">
        <ul class="nav">
          <!-- dashboard -->
          <li>
            <a href="{{url('dashboard')}}">
              <i class="icon-compass"></i>
              <span>Dashboard</span>
            </a>
          </li>
          <!-- /dashboard -->
          <!-- customizer -->
          @if(Auth::User()->user_role_idFk != 2 &&  Auth::User()->user_role_idFk != 4 &&  Auth::User()->user_role_idFk != 3 &&  Auth::User()->user_role_idFk != 5)
            <li>
              <a href="{{url('users/list')}}">
                <i class="fa fa-users" style="padding-right: 0.9375rem;width: 1.875rem;text-align: center; "></i>
                <span>Users</span>
              </a>

            </li>
          @endif
          @if(Auth::User()->user_role_idFk == 3)
            <li>
              <a href="{{url('users/list')}}">
                <i class="fa fa-users" style="padding-right: 0.9375rem;width: 1.875rem;text-align: center; "></i>
                <span>Clients</span>
              </a>

            </li>
          @endif
          @if(count(Auth::User()->projects) > 1 && Auth::User()->user_role_idFk == 2)
            <li>
            <a href="{{url('projects/list')}}">
              <i class="icon-book-open"></i>
              <span>Closings</span>
            </a>

          </li>
          @endif
          @if(Auth::User()->user_role_idFk == 1 || Auth::User()->user_role_idFk == 6 || Auth::User()->user_role_idFk == 7 || Auth::User()->user_role_idFk == 3 || Auth::User()->user_role_idFk == 4)

          <li>
            <a href="{{url('projects/list')}}">
              <i class="icon-book-open"></i>
              <span>Closings</span>
            </a>

          </li>
          <li>
            <a href="{{url('projects/closed/list')}}">
              <i class="icon-book-open"></i>
              <span>Closed</span>
            </a>

          </li>
          @endif
          @if(Auth::User()->user_role_idFk == 1 || Auth::User()->user_role_idFk == 3 )
          <li>
            <a href="{{url('requested/projects/list')}}">
              <i class="icon-book-open"></i>
              <span>Requested Closings</span>
            </a>

          </li>
          @endif
          @if(Auth::User()->user_role_idFk != 5)
            <li>
              <a href="{{url('portals/list')}}">
                <i class="icon-cursor"></i>
                <span>Portals</span>
              </a>

            </li>
          @endif
          @if(Auth::User()->user_role_idFk == 1 || Auth::User()->user_role_idFk == 6 || Auth::User()->user_role_idFk == 7)
              <li>
                <a href="{{url('brokers/list')}}">
                  <i class="icon-cursor"></i>
                  <span>Brokers</span>
                </a>
              </li>

          @endif

          @if(Auth::User()->user_role_idFk == 5)
            <li>
              <a href="{{url('realtors/list/'.Auth::User()->user_id)}}">
                <i class="icon-cursor"></i>
                <span>List Of Realtors</span>
              </a>
            </li>
          @endif
          @if(Auth::User()->user_role_idFk != 1 && Auth::User()->user_role_idFk != 7 && Auth::User()->user_role_idFk != 6)

            @if(Auth::User()->user_role_idFk == 2)
              @if(count(Auth::User()->projects) > 1)
                <li>
                  <a href="{{url('projects/closed/list')}}">
                    <i class="icon-book-open"></i>
                    <span>Closed</span>
                  </a>

                </li>
              @endif
            @endif

            <li>
              <a href="#!" data-toggle="modal" data-target="#contact_model">
                <i class="glyphicon glyphicon-earphone"></i>
                &nbsp&nbsp&nbsp&nbsp<span>Contact Khan Law</span>
              </a>
            </li>
          @endif

          @if(Auth::User()->user_role_idFk == 2)
            <li>
              <a href="#!" data-toggle="modal" data-target="#mail_model">
                <i class="glyphicon glyphicon-earphone"></i>
                &nbsp&nbsp&nbsp&nbsp<span>Invite Realtor/Lender</span>
              </a>
            </li>
          @endif

            </ul>

          <!-- /extras -->
          <!-- menu levels -->

      </nav>
      <!-- /main navigation -->
    </div>
    <!-- /sidebar panel -->
