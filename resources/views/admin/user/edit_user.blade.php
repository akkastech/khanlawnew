@extends('admin.layout')

@section('title', 'Edit User')


@section('content')
  
  @section('page_name', 'Edit User')
  
  @if(session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
@endif

<div class="card bg-white">

    <div class="card-header">
      Edit User
    </div>
    <div class="card-block">

        <form class="form-horizontal" role="form" action="{{url('user/update')}}" method="post">
          <input id="token" name="_token" type="text" value="{!! csrf_token() !!}" hidden>
          <input type="hidden" name="user_id" value="{{$user->user_id}}">
          <input type="hidden" id="country_idFk" name="country_idFk" value="{{$user->country_idFk}}">
          <input type="hidden" id="province_idFk" name="province_idFk" value="{{$user->province_idFk}}">
          <input type="hidden" id="city_idFk" name="city_idFk" value="{{$user->city_idFk}}">
          <div class="row m-a-0">
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Name</label>
                  <div class="col-sm-8">
                    <input type="text" name="name" value="{{$user->name}}" class="form-control" required="">
                  </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">User Position</label>
                  <div class="col-sm-8">
                    <select id="role" class="form-control" style="width:100%;" name="role" required="">
                      <option value="">Please Select</option>
                      @if(count($roles) > 0)

                        @foreach($roles as $role)
                          <option value="{{$role->role_id}}" @if($user->user_role_idFk == $role->role_id) {{'selected'}} @endif >{{$role->role_title}}</option>
                        @endforeach

                      @endif
                    </select>
                  </div>
                </div>
            </div>
          </div>

          <div class="row m-a-0">
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Mobile Number</label>
                  <div class="col-sm-8">
                    <input type="text" name="number" value="{{$user->number}}" class="form-control" required="">
                  </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Email</label>
                  <div class="col-sm-8">
                    <input type="text" name="email" value="{{$user->email}}" class="form-control" required="">
                  </div>
                </div>
            </div>
            
          </div>

         <div class="row m-a-0">
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Password</label>
                  <div class="col-sm-8">
                    <input type="password" name="password" class="form-control" required="">
                  </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">County</label>
                  <div class="col-sm-8">
                    <select id="country" class="form-control" style="width:100%;" name="country" required="">
                    </select>
                  </div>
                </div>
            </div>
            
            
          </div>
          <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Province</label>
                  <div class="col-sm-8">
                    <select id="province" class="form-control" style="width:100%;" name="province" required="">
                      <option value="">Please Select</option>
                    </select>
                  </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">City</label>
                  <div class="col-sm-8">
                    <select id="city" class="form-control" style="width:100%;" name="city" required="">
                      <option value="">Please Select</option>
                    </select>
                  </div>
                </div>
            </div>
          </div>

      

  </div>
  <div class="card-footer">
    <div class="row">
      <div class="col-md-2">
        <a href="#!" onclick="window.history.back()" class="btn btn-default btn-block ">Cancel</a>
      </div>
      <div class="col-md-2 pull-right">
        <button class="btn btn-info btn-block ">Submit</button>
      </div>
    </div>
  </div>
  </form>
</div>


@stop

@section('foot')

  @parent
   <script src="{{url('admin/vendor/datatables/media/js/jquery.dataTables.js')}}"></script>
  <script src="{{url('admin/vendor/datatables/media/js/datatables.bootstrap.js')}}"></script>

    <script type="text/javascript">
      function ajaxCall() {
        this.send = function(data, url, method, success, type) {
            type = type || 'json';
            var successRes = function(data) {
                success(data);
            };

            var errorRes = function(e) {
                console.log(e);
                alert("Error found \nError Code: " + e.status + " \nError Message: " + e.statusText);
            };
            $.ajax({
                url: url,
                type: method,
                data: data,
                success: successRes,
                error: errorRes,
                dataType: type,
                timeout: 60000
            });

        }

    }

    function locationInfo() {
        var rootUrl = "http://akkastechdemo.website/country/v2/api.php";
        var call = new ajaxCall();
        this.getCities = function(id) {
            $("#city option:gt(0)").remove();
            var url = rootUrl + '?type=getCities&stateId=' + id;
            var method = "post";
            var data = {};
            $('#city').find("option:eq(0)").html("Please wait..");
            call.send(data, url, method, function(data) {
                $('#city').find("option:eq(0)").html("Prašome pasirinkti");
                if (data.tp == 1) {
                    $.each(data['result'], function(key, val) {
                        var option = $('<option />');
                        if(key == '{{$user->city_idFk}}'){
                          option.attr('selected', true).attr('value', val).attr("data-key", key).text(val);
                        }else{
                          option.attr('value', val).attr("data-key", key).text(val);
                        }
                        $('#city').append(option);
                    });
                    $("#city").prop("disabled", false);
                } else {
                    alert(data.msg);
                }
            });
        };

        this.getStates = function(id) {
            $("#province option:gt(0)").remove();
            $("#city option:gt(0)").remove();
            var url = rootUrl + '?type=getStates&countryId=' + id;
            var method = "post";
            var data = {};
            $('#province').find("option:eq(0)").html("Please wait..");
            call.send(data, url, method, function(data) {
                $('#province').find("option:eq(0)").html("Prašome pasirinkti");
                if (data.tp == 1) {
                    $.each(data['result'], function(key, val) {
                        var option = $('<option />');
                        if(key == '{{$user->province_idFk}}'){
                          option.attr('selected', true).attr('value', val).attr("data-key", key).text(val);
                        }else{
                          option.attr('value', val).attr("data-key", key).text(val);
                        }
                        $('#province').append(option);
                    });
                    $("#province").prop("disabled", false);
                } else {
                    alert(data.msg);
                }
            });
        };

        this.getCountries = function() {
            var url = rootUrl + '?type=getCountries';
            var method = "post";
            var data = {};
            $('#country').find("option:eq(0)").html("Please wait..");
            call.send(data, url, method, function(data) {
                $('#country').find("option:eq(0)").html("Select Country");
                console.log(data);
                if (data.tp == 1) {
                    $.each(data['result'], function(key, val) {
                        var option = $('<option />');
                        if(key == '{{$user->country_idFk}}'){
                          option.attr('selected', true).attr('value', val).attr("data-key", key).text(val);
                        }else{
                          option.attr('value', val).attr("data-key", key).text(val);
                        }
                        $('#country').append(option);
                    });
                    $("#country").prop("disabled", false);
                } else {
                    alert(data.msg);
                }
            });
        };

    }

    $(function() {
        var loc = new locationInfo();
        loc.getCountries();
        loc.getStates('{{$user->country_idFk}}');
        loc.getCities('{{$user->province_idFk}}');
        $("#country").on("change", function(ev) {
            var countryId = $(this).children('option:selected').data('key');
            if (countryId != '') {
                loc.getStates(countryId);
                $('#country_idFk').val(countryId);
            } else {
                $("#province option:gt(0)").remove();
            }
        });
        $("#province").on("change", function(ev) {
            var stateId = $(this).children('option:selected').data('key');
            console.log(stateId);
            if (stateId != '') {
                loc.getCities(stateId);
                $('#province_idFk').val(stateId);
            } else {
                $("#city option:gt(0)").remove();
            }
        });

    });

    $('#city').on('change', function(){

      $('#city_idFk').val($(this).data('key'));

    });

    </script>


    <!-- <script type="text/javascript">
      $(document).ready(function(){


          $.ajax({
              type:'POST',
              url:'http://dezynage.com/country/ajaxData.php',
              data:'country={{$user->country}}',
              success:function(html){
                  $('#country').html(html);
                  $('#province').html('<option value="">Select Country first</option>');
              }
          });



            $.ajax({
                type:'POST',
                url:'http://dezynage.com/country/ajaxData.php',
                data:{
                  'country_id':'{{$user->country}}',
                  'state': '{{$user->province}}'
                },
                success:function(html){
                    $('#province').html(html);
                }
            });

             $.ajax({
                  type:'POST',
                  url:'http://dezynage.com/country/ajaxData.php',
                  data:{
                  'state_id':'{{$user->province}}',
                  'city': '{{$user->city}}'
                },
                  success:function(html){
                      $('#city').html(html);
                  }
              });

        
          $('#country').on('change',function(){
              var countryID = $(this).val();
              if(countryID){
                  $.ajax({
                      type:'POST',
                      url:'http://dezynage.com/country/ajaxData.php',
                      data:'country_id='+countryID,
                      success:function(html){
                          $('#province').html(html);
                          $('#city').html('<option value="">Select Province first</option>');
                      }
                  });
              }else{
                  $('#province').html('<option value="">Select country first</option>');
                  $('#city').html('<option value="">Select Province first</option>');
              }
          });

          $('#province').on('change',function(){
              var stateID = $(this).val();
              if(stateID){
                  $.ajax({
                      type:'POST',
                      url:'http://dezynage.com/country/ajaxData.php',
                      data:'state_id='+stateID,
                      success:function(html){
                          $('#city').html(html);
                      }
                  });
              }else{
                  $('#city').html('<option value="">Select Province first</option>');
              }
          });
      });
    </script> -->


@endsection