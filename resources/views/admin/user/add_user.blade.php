
@extends('admin.layout')

@section('title', 'Add User')


@section('content')

  @section('page_name', 'Add User')

  @if(session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
@endif

<div class="card bg-white">

    <div class="card-header">
      Add User
    </div>
    <div class="card-block">

        <form class="form-horizontal" role="form" action="{{url('user/submit')}}" method="post">
          <input id="token" name="_token" type="text" value="{!! csrf_token() !!}" hidden>
          <input type="hidden" id="country_idFk" name="country_idFk" value="38">
          <input type="hidden" id="province_idFk" name="province_idFk" value="671">
          <input type="hidden" id="city_idFk" name="city_idFk" value="10417">
          <div class="row m-a-0">
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Name</label>
                  <div class="col-sm-8">
                    <input type="text" name="name" class="form-control" required="">
                  </div>
                </div>
            </div>
            @if(Auth::User()->user_role_idFk == 3)
              <input type="hidden" name="role" value="2">
            @else
              <div class="col-lg-4">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">User Position</label>
                    <div class="col-sm-8">
                      <select id="role" class="form-control" style="width:100%;" name="role" required="">
                        <option value="">Please Select</option>
                        @if(count($roles) > 0)

                          @foreach($roles as $role)

                            @if(!((Auth::User()->user_role_idFk == 6 && ($role->role_id == 6 || $role->role_id == 7)) || (Auth::User()->user_role_idFk == 7 && ($role->role_id == 6 || $role->role_id == 7))))
                              <option value="{{$role->role_id}}">{{$role->role_title}}</option>

                            @endif
                          @endforeach

                        @endif
                      </select>
                    </div>
                  </div>
              </div>
            @endif
          </div>

          <div class="row m-a-0">
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Mobile Number</label>
                  <div class="col-sm-8">
                    <input type="text" name="number" class="form-control" required="">
                  </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Email</label>
                  <div class="col-sm-8">
                    <input type="text" name="email" class="form-control" required="">
                  </div>
                </div>
            </div>

          </div>

          <div class="row m-a-0">
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Password</label>
                  <div class="col-sm-8">
                    <input type="password" name="password" class="form-control" required="">
                  </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">County</label>
                  <div class="col-sm-8">
                    <select id="country" class="form-control" style="width:100%;" name="country" required="">
                    </select>
                  </div>
                </div>
            </div>


          </div>
          <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Province</label>
                  <div class="col-sm-8">
                    <select id="province" class="form-control" style="width:100%;" name="province" required="">
                      <option value="">Please Select</option>
                    </select>
                  </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">City</label>
                  <div class="col-sm-8">
                    <select id="city" class="form-control" style="width:100%;" name="city" required="">
                      <option value="">Please Select</option>
                    </select>
                  </div>
                </div>
            </div>
          </div>



  </div>
  <div class="card-footer">
    <div class="row">
      <div class="col-md-2">
        <a href="#!" onclick="window.history.back()" class="btn btn-default btn-block ">Cancel</a>
      </div>
      <div class="col-md-2 pull-right">
        <button class="btn btn-info btn-block ">Submit</button>
      </div>
    </div>
  </div>
  </form>
</div>


@stop

@section('foot')

  @parent
   <script src="{{url('admin/vendor/datatables/media/js/jquery.dataTables.js')}}"></script>
  <script src="{{url('admin/vendor/datatables/media/js/datatables.bootstrap.js')}}"></script>


    <script type="text/javascript">
      $(document).ready(function(){


          // $.ajax({
          //   type:'GET',
          //   url:'https://api.printful.com/countries ',
          //   data:'country=yes',
          //   success:function(html){
          //     console.log(html);
          //       $('#country').html('<option value="">Select Country first</option>');
          //       $('#province').html('<option value="">Select Country first</option>');

          //   }
          // });

          // $('#country').on('change',function(){
          //     var countryID = $(this).val();
          //     if(countryID){
          //         $.ajax({
          //             type:'POST',
          //             url:'http://dezynage.com/country/ajaxData.php',
          //             data:'country_id='+countryID,
          //             success:function(html){
          //                 $('#province').html(html);
          //                 $('#city').html('<option value="">Select Province first</option>');
          //             }
          //         });
          //     }else{
          //         $('#province').html('<option value="">Select country first</option>');
          //         $('#city').html('<option value="">Select Province first</option>');
          //     }
          // });

          // $('#province').on('change',function(){
          //     var stateID = $(this).val();
          //     if(stateID){
          //         $.ajax({
          //             type:'POST',
          //             url:'http://dezynage.com/country/ajaxData.php',
          //             data:'state_id='+stateID,
          //             success:function(html){
          //                 $('#city').html(html);
          //             }
          //         });
          //     }else{
          //         $('#city').html('<option value="">Select Province first</option>');
          //     }
          // });
      });
    </script>
    <script type="text/javascript">
      function ajaxCall() {
        this.send = function(data, url, method, success, type) {
            type = type || 'json';
            var successRes = function(data) {
                success(data);
            };

            var errorRes = function(e) {
                console.log(e);
                alert("Error found \nError Code: " + e.status + " \nError Message: " + e.statusText);
            };
            $.ajax({
                url: url,
                type: method,
                data: data,
                success: successRes,
                error: errorRes,
                dataType: type,
                timeout: 60000
            });

        }

    }

    function locationInfo() {
        var rootUrl = "http://akkastechdemo.website/country/v2/api.php";
        var call = new ajaxCall();
        this.getCities = function(id) {
            $("#city option:gt(0)").remove();
            var url = rootUrl + '?type=getCities&stateId=' + id;
            var method = "post";
            var data = {};
            $('#city').find("option:eq(0)").html("Please wait..");
            call.send(data, url, method, function(data) {
                $('#city').find("option:eq(0)").html("Prašome pasirinkti");
                if (data.tp == 1) {
                    $.each(data['result'], function(key, val) {
                        var option = $('<option />');
                          option.attr('value', val).attr("data-key", key).text(val);
                        $('#city').append(option);
                    });
                    $('#city_idFk').val($(this).data('key'));
                    $("#city").prop("disabled", false);
                } else {
                    alert(data.msg);
                }
            });
        };

        this.getStates = function(id) {
            $("#province option:gt(0)").remove();
            $("#city option:gt(0)").remove();
            var url = rootUrl + '?type=getStates&countryId=' + id;
            var method = "post";
            var data = {};
            $('#province').find("option:eq(0)").html("Please wait..");
            call.send(data, url, method, function(data) {
                $('#province').find("option:eq(0)").html("Prašome pasirinkti");
                if (data.tp == 1) {
                    $.each(data['result'], function(key, val) {
                        var option = $('<option />');
                        if(key == 671){
                          option.attr('selected', true).attr('value', val).attr("data-key", key).text(val);
                        }else{
                          option.attr('value', val).attr("data-key", key).text(val);
                        }
                        $('#province').append(option);
                    });
                    $("#province").prop("disabled", false);
                    var stateId = $(this).children('option:selected').data('key');
                    $('#province_idFk').val(stateId);
                } else {
                    alert(data.msg);
                }
            });
        };

        this.getCountries = function() {
            var url = rootUrl + '?type=getCountries';
            var method = "post";
            var data = {};
            $('#country').find("option:eq(0)").html("Please wait..");
            call.send(data, url, method, function(data) {
                $('#country').find("option:eq(0)").html("Select Country");
                console.log(data);
                if (data.tp == 1) {
                    $.each(data['result'], function(key, val) {
                        var option = $('<option />');
                        if(key == 38){
                          option.attr('selected', true).attr('value', val).attr("data-key", key).text(val);
                        }else{
                          option.attr('value', val).attr("data-key", key).text(val);
                        }
                        $('#country').append(option);
                    });
                    var countryId = $(this).children('option:selected').data('key');
                    $('#country_idFk').val(countryId);
                    $("#country").prop("disabled", false);
                } else {
                    alert(data.msg);
                }
            });
        };

    }

    $(function() {
        var loc = new locationInfo();
        loc.getCountries();
        loc.getStates(38);
        loc.getCities(671);
        $("#country").on("change", function(ev) {
            var countryId = $(this).children('option:selected').data('key');
            if (countryId != '') {
                loc.getStates(countryId);
                $('#country_idFk').val(countryId);
            } else {
                $("#province option:gt(0)").remove();
            }
        });
        $("#province").on("change", function(ev) {
            var stateId = $(this).children('option:selected').data('key');
            console.log(stateId);
            if (stateId != '') {
                loc.getCities(stateId);
                $('#province_idFk').val(stateId);
            } else {
                $("#city option:gt(0)").remove();
            }
        });

    });


    $('#city').on('change', function(){
      var city = $(this).children('option:selected').data('key');
      $('#city_idFk').val(city);

    });
    </script>


@endsection
