@extends('admin.layout')

@section('title', 'Dashboard')


@section('content')

  @section('page_name', 'Users List')

@if(session('success'))
  <div class="alert alert-success">
    {{session('success')}}
  </div>
@endif
@if(session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
@endif

<div class="card bg-white" data-ng-controller="tableCtrl">
  <div class="card-header">
    Datatables <a href="{{url('user/add')}}" class="pull-right btn btn-info">Add User</a>
  </div>
  <div class="card-block">
    <table class="table table-bordered table-condensed datatable m-b-0" ui-jq="dataTable" ui-options="dataTableOpt">
      <thead>
        <tr>
          <th>Name</th>
          <th>Position</th>
          <th>Status</th>
          <th>Email</th>
          <th>Created At</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @if(count($users) > 0)

          @foreach($users as $user)
            <tr>
              <td><a href="{{url('user/view/'.$user->user_id)}}">{{$user->name}}</a></td>
              <td>{{$user->role->role_title}}</td>
              <td>
                @if($user->user_status == '1')
                  {{'Active'}}
                @else
                  {{'Deactive'}}
                @endif
              </td>
              <td>{{$user->email}}</td>
              <td>{{date('d F, Y', strtotime($user->created_at))}}</td>
              <td>
                <div class="row">
                  <div class="col-md-4">
                    @if($user->user_status == '1')
                      <a href="{{url('user/deactive/'.$user->user_id)}}" >
                          <i class="fa fa-ban" aria-hidden="true" style="font-size:20px;color:#fe6767;"></i>
                      </a>
                    @else
                      <a href="{{url('user/active/'.$user->user_id)}}" >
                          <i class="fa fa-check" aria-hidden="true" style="font-size:20px;color:#09cc09;"></i>
                      </a>
                    @endif
                  </div>
                  <div class="col-md-4">
                    <a href="{{url('user/edit/'.$user->user_id)}}" >
                        <i class="fa fa-pencil" aria-hidden="true" style="font-size:20px;color:#1ebcfb;"></i>
                    </a>
                  </div>
                  <div class="col-md-4">
                    <a class="delete" href="{{url('user/delete/'.$user->user_id)}}" >
                        <i class="fa fa-trash-o" aria-hidden="true" style="font-size:20px;color:#fe6767;"></i>
                    </a>
                  </div>
                </div>
              </td>
            </tr>
          @endforeach
        @else
          <tr>
              <td colspan="4">Record Not Found</td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>



@stop

@section('foot')

  @parent
   <script src="{{url('admin/vendor/datatables/media/js/jquery.dataTables.js')}}"></script>
  <script src="{{url('admin/vendor/datatables/media/js/datatables.bootstrap.js')}}"></script>

  <script type="text/javascript">
    $('.datatable').dataTable({
        "scrollX": true
    } );
    $('.delete').on('click',function(event) {
        var r = window.confirm('Are you sure to delete this User');
        if (r != true) {
          event.preventDefault();
        }
    });
  </script>

@endsection
