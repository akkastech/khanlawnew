@extends('admin.layout')

@section('title', 'Dashboard')


@section('content')
  
  @section('page_name', 'User')

  @if(session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
@endif
@if(session('success'))
  <div class="alert alert-success">
    {{session('success')}}
  </div>
@endif

  <div class="card bg-white">

    <div class="card-header">
      {{$user->name}}
    </div>
    <div class="card-block">
      
      <div class="row">
        <div class="col-md-3">
          <h4>Full Name</h4>
        </div>
        <div class="col-md-3">
          <h4>{{$user->name}}</h4>
        </div>
        <div class="col-md-3">
          <h4>Contact No</h4>
        </div>
        <div class="col-md-3">
          <h4>{{$user->number}}</h4>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-3">
          <h4>Email Address</h4>
        </div>
        <div class="col-md-3">
          <h4>{{$user->email}}</h4>
        </div>
      </div>

    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-2">
          <a href="#!" onclick="window.history.back()" class="btn btn-default btn-block ">Back</a>
        </div>
      </div>
    </div>

</div>

@stop

@section('foot')

  @parent


@endsection