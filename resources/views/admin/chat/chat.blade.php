@extends('admin.layout')

@section('title', 'Portals ')


@section('content')
  
  @section('page_name', 'Portals')
  <style type="text/css">
    
    .other-text-box{
      display: inline-block;
      padding: 10px 15px;
      background-color: #d7d5d5;
      border-radius: 4px;
    }

  </style>
  <div class="fill-container" >
    <div class="relative full-height">
            <div class="display-columns">
              <div class="column contacts-sidebar hidden-xs bg-white b-r" style="height: auto;">
                <div class="scroll">
                  <div class="p-a">
                    <nav role="navigation">
                      <ul class="nav nav-stacked nav-pills">
                       
                        
                        <li>
                          <a href="javascript:;">
                            <i class="icon-tag text-success"></i>
                            <span>Clients</span><br>
                            @if(count($pro) > 0 && $pro->client)
                              @foreach($pro->client as $client)
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<span style="font-size: 20px;">&#10551;</span>
                                <span>{{$client->name}}</span><br>
                              @endforeach
                            @else

                            @endif

                          </a>
                        </li>
                        <li>
                          <a href="javascript:;">
                            <i class="icon-tag text-warning"></i>
                            <span>Realtor</span><br>
                            @if(count($pro) > 0 && $pro->agent)
                              @foreach($pro->agent as $agent)
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<span style="font-size: 20px;">&#10551;</span>
                                <span>{{$agent->name}}</span><br>
                              @endforeach
                            @else

                            @endif
                          </a>
                        </li>
                        <li>
                          <a href="javascript:;">
                            <i class="icon-tag text-danger"></i>
                            <span>Lender</span><br>
                            @if(count($pro) > 0 && $pro->mortgage)
                              @foreach($pro->mortgage as $mortgage)
                                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<span style="font-size: 20px;">&#10551;</span>
                                <span>{{$mortgage->name}}</span><br>
                              @endforeach
                            @else

                            @endif
                          </a>
                        </li>
                        @if(Auth::User()->user_role_idFk == 2)
                          <li>
                            <a href="javascript:;">
                              <i class="icon-tag text-danger"></i>
                              <span>Lawyer/Clerk</span><br>
                                  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<span style="font-size: 20px;">&#10551;</span>
                                  <span>{{$pro->user->name}}</span><br>
                            </a>
                          </li>
                        @else

                          @if(count($pro) > 0 && $pro->laywer)
                            <li>
                              <a href="javascript:;">
                                <i class="icon-tag text-danger"></i>
                                <span>Laywer</span><br>
                                
                                  @foreach($pro->laywer as $laywer)
                                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<span style="font-size: 20px;">&#10551;</span>
                                    <span>{{$laywer->name}}</span><br>
                                  @endforeach
                                @else

                              </a>
                            </li>
                          @endif
                          
                        @endif
                       
                      </ul>
                    </nav>
                  </div>
                </div>
              </div>
              <div class="column contact-view">
                <div class="scroll full-height">
                   <div class="scroll full-height">
                  <div class="full-height">
                    <div class="message-header m-b" style="box-shadow: 0px 1px 4px 0px #b4b4b4;margin: 0px !important;z-index:999;">
                      <div class="p-a">
                        <!-- <div class="pull-left p-r">
                          <img src="images/face2.jpg" class="avatar avatar-md img-circle" alt="">
                        </div> -->
                        <div class="overflow-hidden">
                          <div class="date"><b>Closing on {{date('d F, Y', strtotime($pro->closing_date))}}</b></div>
                          <div class="lead h3 m-t-0" style="margin-left: 5%; font-size: 20pt;">{{$pro->title}}  </div>
                          <!-- <div class="message-sender">
                            <p><span class="text-info">Denise Peterson</span> to me, Jeff &amp; Suzzane</p>
                          </div> -->
                        </div>
                      </div>
                    </div>
                    <div class="p-a" id="chat-box" style="overflow-y:scroll;">

                      @if(count($chats) > 0)
                        @if(count($chats) == 1)
                          <input type="hidden" name="last_entry" value="{{$chats[0]->chat_id}}">
                        @else
                          <input type="hidden" name="last_entry" value="{{$chats->last()->chat_id}}">
                        @endif
                        @foreach($chats as $key => $chat)

                          
                          @if($chat->user_idFk == Auth::User()->user_id)

                              @if($chat->type == 'message')

                                <div class="card-block">
                                  <div class="profile-timeline-header">
                                    <a href="#" class="profile-timeline-user" style="float:right;">
                                      <img src="{{url('uploads/'.Auth::User()->img)}}" alt="" class="img-rounded">
                                    </a>
                                    <div class="profile-timeline-user-details" style="float:right;">
                                      <a href="#" class="bold">{{$chat->user->name}}</a>
                                    </div>
                                  </div>
                                  <div class="profile-timeline-content" style="margin-left:0px;margin-right:63px;text-align:right;">
                                    <p style="margin-bottom:0px;">{{Carbon\Carbon::createFromTimeStamp(strtotime($chat->created_at))->diffForHumans()}}</p>
                                    <p class="other-text-box" style="background-color: #bbead8;">{{$chat->message}}</p>
                                  </div>
                                </div>

                              @elseif($chat->type == 'file')

                                  <div class="card-block">
                                    <div class="profile-timeline-header">
                                      <a href="#" class="profile-timeline-user" style="float:right;">
                                        <img src="{{url('uploads/'.$chat->user->img)}}" alt="" class="img-rounded">
                                      </a>
                                      <div class="profile-timeline-user-details" style="float:right;">
                                        <a href="#" class="bold">{{$chat->user->name}}</a>
                                      </div>
                                    </div>
                                    <div class="profile-timeline-content" style="margin-left:0px;margin-right:63px;text-align:right;">
                                      <p style="margin-bottom:0px;">{{Carbon\Carbon::createFromTimeStamp(strtotime($chat->created_at))->diffForHumans()}}</p>
                                      <p class="other-text-box" style="background-color: #bbead8;">
                                        <a href="{{url('uploads/'.$chat->file->value)}}">
                                          @if($chat->file->media_type == 'pdf')
                                            <i style="font-size: 22px;padding: 0px 10px;" class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                          @else
                                            <i style="font-size: 22px;padding: 0px 10px;" class="fa fa-file-excel-o" aria-hidden="true"></i>
                                          @endif
                                        </a>
                                      </p>
                                    </div>
                                  </div>

                              @elseif($chat->type == 'img')

                                <div class="card-block">
                                  <div class="profile-timeline-header">
                                    <a href="#" class="profile-timeline-user" style="float:right;">
                                      <img src="{{url('admin/images/avatar.jpg')}}" alt="" class="img-rounded">
                                    </a>
                                    <div class="profile-timeline-user-details" style="float:right;">
                                      <a href="#" class="bold">{{$chat->user->name}}</a>
                                    </div>
                                  </div>
                                  <div class="profile-timeline-content" style="margin-left:0px;margin-right:63px;text-align:right;">
                                    <p style="margin-bottom:0px;">{{Carbon\Carbon::createFromTimeStamp(strtotime($chat->created_at))->diffForHumans()}}</p>
                                    <p class="other-text-box" style="background-color: #bbead8;">
                                      @foreach($chat->imgs as $img)
                                        <a href="{{url('uploads/'.$img->value)}}"><img src="{{url('uploads/'.$img->value)}}" style="height:80px;width:80px;margin:0px 5px;"></a>
                                      @endforeach
                                    </p>
                                  </div>
                                </div>

                              @endif

                          @else

                            @if($chat->type == 'message')
                              <div class="card-block">
                                <div class="profile-timeline-header">
                                  <a href="#" class="profile-timeline-user">
                                    <img src="{{url('admin/images/avatar.jpg')}}" alt="" class="img-rounded">
                                  </a>
                                  <div class="profile-timeline-user-details">
                                    <a href="#" class="bold">{{$chat->user->name}}</a>
                                  </div>
                                </div>
                                <div class="profile-timeline-content ">
                                  <p style="margin-bottom:0px;">{{Carbon\Carbon::createFromTimeStamp(strtotime($chat->created_at))->diffForHumans()}}</p>
                                  <p class="other-text-box">{{$chat->message}}</p>
                                </div>
                              </div>

                            @elseif($chat->type == 'file')

                              <div class="card-block">
                                <div class="profile-timeline-header">
                                  <a href="#" class="profile-timeline-user">
                                    <img src="{{url('admin/images/avatar.jpg')}}" alt="" class="img-rounded">
                                  </a>
                                  <div class="profile-timeline-user-details">
                                    <a href="#" class="bold">{{$chat->user->name}}</a>
                                  </div>
                                </div>
                                <div class="profile-timeline-content ">
                                  <p style="margin-bottom:0px;">{{Carbon\Carbon::createFromTimeStamp(strtotime($chat->created_at))->diffForHumans()}}</p>
                                  <p class="other-text-box">
                                    <a href="{{url('uploads/'.$chat->file->value)}}">
                                      @if($chat->file->media_type == 'pdf')
                                        <i style="font-size: 22px;padding: 0px 10px;" class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                      @else
                                        <i style="font-size: 22px;padding: 0px 10px;" class="fa fa-file-excel-o" aria-hidden="true"></i>
                                      @endif
                                    </a>
                                  </p>
                                </div>
                              </div>

                            @elseif($chat->type == 'img')

                                <div class="card-block">
                                  <div class="profile-timeline-header">
                                    <a href="#" class="profile-timeline-user" >
                                      <img src="{{url('admin/images/avatar.jpg')}}" alt="" class="img-rounded">
                                    </a>
                                    <div class="profile-timeline-user-details">
                                      <a href="#" class="bold">{{$chat->user->name}}</a>
                                    </div>
                                  </div>
                                  <div class="profile-timeline-content" >
                                    <p style="margin-bottom:0px;">{{Carbon\Carbon::createFromTimeStamp(strtotime($chat->created_at))->diffForHumans()}}</p>
                                    <p class="other-text-box">
                                      @foreach($chat->imgs as $img)
                                        <a href="{{url('uploads/'.$img->value)}}"><img src="{{url('uploads/'.$img->value)}}" style="height:80px;width:80px;margin:0px 5px;"></a>
                                      @endforeach
                                    </p>
                                  </div>
                                </div>

                              @endif

                          @endif

                        @endforeach

                      @else
                        <h3 id="empty">New Conversation</h3>

                      @endif

                    </div>

                    @if(Auth::User()->user_role_idFk != 5)

                      @if($pro->project_going_status == 'Not-completed')
                    <!-- new text here -->
                      <div class="p-a">
                          <div class="row">
                            <div class="col-md-8" style="height:100px;">
                              <textarea class="form-control" id="message" rows="4"></textarea>
                            </div>
                            <div class="col-md-2">
                              <div class="row">
                                <div class="col-md-4">
                                  <a href="#!" id="docs-click"><i class="fa fa-paperclip" style="font-size: 24px;" aria-hidden="true"></i></a>
                                  <form enctype="multipart/form-data" id="upload_form" role="form" method="POST" action="">
                                    <input type="file" style="display:none;" name="docs" id="docs-input" accept=".pdf, .docx, .xlsx">
                                  </form>
                                </div>
                                <div class="col-md-4">
                                  <a href="#!" id="img-click"><i class="fa fa-file-image-o" style="font-size: 24px;" aria-hidden="true"></i></a>
                                  <form enctype="multipart/form-data" id="upload_imgs" role="form" method="POST" action="">
                                    <input type="file" style="display:none;" name="imgs[]" multiple="true" id="img-input" accept="image/*">
                                  </form>
                                </div>
                              </div>
                              <div class="row" style="margin-top:40px;">
                                <div class="col-md-8">
                                  <button class="btn btn-info col-md-12" id="send">Send</button>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>

                      @endif

                    @endif
                    
                </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>


@stop

@section('foot')

  @parent
   <script src="{{url('admin/vendor/datatables/media/js/jquery.dataTables.js')}}"></script>
  <script src="{{url('admin/vendor/datatables/media/js/datatables.bootstrap.js')}}"></script>

  <script type="text/javascript">
    $('.datatable').dataTable();
  </script>
  <script type="text/javascript">
    $(document).ready(function(){

      var height = $(window).height();

      @if(Auth::User()->user_role_idFk != 5)

        @if($pro->project_going_status == 'Not-completed')

          $('#chat-box').css('height', (height/100*60));
          $('#chat-box').css('max-height', (height/100*60));

        @else

          $('#chat-box').css('height', (height/100*78));
          $('#chat-box').css('max-height', (height/100*78));
          
        @endif

      @else

          $('#chat-box').css('height', (height/100*78));
          $('#chat-box').css('max-height', (height/100*78));

      @endif
      // scroll down
      $('#chat-box').scrollTop($('#chat-box')[0].scrollHeight);

      // docs
      $('#docs-click').on('click', function(){
          $('#docs-input').click();
      });
      $('#img-click').on('click', function(){
          $('#img-input').click();
      });

      $("#docs-input").on('change', function(){
        var project_id = '{{$pro->project_id}}';

        $.ajax({
          url:'{{url("message/docs/")}}/'+project_id,
          data:new FormData($("#upload_form")[0]),
          dataType:'',
          async:false,
          type:'post',
          processData: false,
          contentType: false,
          success:function(res){

            $('#chat-box').find('#empty').remove();
            $('input[name=last_entry]').val(res.last);
            $('#message').val('');
            $('#chat-box').append(res.html);

            $('#chat-box').scrollTop($('#chat-box')[0].scrollHeight);
          },
        });

      });

      $("#img-input").on('change', function(){
        var project_id = '{{$pro->project_id}}';

        $.ajax({
          url:'{{url("message/img/")}}/'+project_id,
          data:new FormData($("#upload_imgs")[0]),
          dataType:'',
          async:false,
          type:'post',
          processData: false,
          contentType: false,
          success:function(res){

            $('#chat-box').find('#empty').remove();
            $('input[name=last_entry]').val(res.last);
            $('#message').val('');
            $('#chat-box').append(res.html);

            $('#chat-box').scrollTop($('#chat-box')[0].scrollHeight);
          },
        });

      });


      // chat start here
      $('#send').on('click', function(){

        var message = $('#message').val();
        var project_id = '{{$pro->project_id}}';
        console.log(message);
        if(message != ""){

            $.ajax({
              type: "get",
              url: '{{ url("message/send")}}',
              data:{
                message: message,
                project_id: project_id
              },
              dataType: ''
            }).success(function(res) {
              $('#chat-box').find('#empty').remove();
              $('input[name=last_entry]').val(res.last);
              $('#message').val('');
              $('#chat-box').append(res.html);

              $('#chat-box').scrollTop($('#chat-box')[0].scrollHeight);

            });

        }

      });


      var last_chat_id = $('input[name=last_entry]').val();

      if(last_chat_id !== undefined){

        window.setInterval(function(){

          var last_chat_id = $('input[name=last_entry]').val();
          var url = '{{ url("message/check/new")}}/'+last_chat_id;
          console.log(url);
          // ajax calls
          $.ajax({
            type: "get",
            url: url,
            data:'',
            dataType: ''
          }).success(function(res) {
            
            if(res == 'true'){
              record_get(last_chat_id);
            }

          });
        }, 5000);

      }
      


    }); //document end

    function record_get(id)
    {
      $.ajax({
          type: "get",
          url: '{{ url("message/get/new")}}/'+id,
          data:'',
          dataType: ''
        }).success(function(res) {
          $('#chat-box').find('#empty').remove();
          $('input[name=last_entry]').val(res.last);
          $('#message').val('');
          $('#chat-box').append(res.html);

          $('#chat-box').scrollTop($('#chat-box')[0].scrollHeight);

        });
    }
  </script>

@endsection