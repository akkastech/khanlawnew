@extends('admin.layout')

@section('title', 'Portals ')


@section('content')
  
  @section('page_name', 'Portals')

@if(session('success'))
  <div class="alert alert-success">
    {{session('success')}}
  </div>
@endif
@if(session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
@endif

<div class="page-title">
    <div class="title">Portals</div>
</div>
<div class="row mb25">

  @if(count($projects) > 0)

    @foreach($projects as $key => $pro)


        <div class="col-md-6">
          <div class="card bg-white">
            <a href="{{url('project/tracker/'.$pro->project_id)}}">
              <div class="card-header bg-primary text-white">
                <div class="pull-left" style="width:100%;border-bottom: 1px solid gray;margin-bottom: 10px;padding-bottom:10px;">{{$pro->title}}</div>
                <div class="pull-left">{{$pro->file_no}} <span style="margin-left:15px;">{{date('d F, Y', strtotime($pro->created_at))}}</span></div>
                  <div class="pull-right">{{$pro->project_type}}</div>
              </div>
            </a>
            <div class="card-block">
              <p>{{$pro->description}} <a href="{{url('chat/dashboard/'.$pro->project_id)}}" class="btn btn-info pull-right">Chat</a></p>

            </div>
          </div>
        </div>


    @endforeach

  @else
    <h4>No Record Found</h4>
  @endif

</div>


@stop

@section('foot')

  @parent
   <script src="{{url('admin/vendor/datatables/media/js/jquery.dataTables.js')}}"></script>
  <script src="{{url('admin/vendor/datatables/media/js/datatables.bootstrap.js')}}"></script>

  <script type="text/javascript">
    $('.datatable').dataTable();
  </script>

@endsection