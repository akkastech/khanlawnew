@extends('admin.layout')

@section('title', 'Project List ')


@section('content')
  
  @section('page_name', 'Project List')

@if(session('success'))
  <div class="alert alert-success">
    {{session('success')}}
  </div>
@endif
@if(session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
@endif

<div class="card bg-white" data-ng-controller="tableCtrl">
  <div class="card-header">
    Closed Project List 
  </div>
  <div class="card-block">
    <table class="table table-bordered table-condensed datatable m-b-0" ui-jq="dataTable" ui-options="dataTableOpt">
      <thead>
        <tr>
          <th>File No</th>
          <th>Title</th>
          <th>Type</th>
          <th>Opening Date</th>
          <th>Closing Date</th>
          <th>Client</th>
          <th>Realtor</th>
          <th>Lender</th>
          <th>Status</th>
          <th>Message</th>
        </tr>
      </thead>
      <tbody>
        @if(count($projects) > 0)

          @foreach($projects as $project)
            <tr>
              <td>{{$project->file_no}}</td>
              <td>{{$project->title}}</td>
              <td>{{$project->project_type}}</td>
              <td>{{date('d F, Y', strtotime($project->opening_date))}}</td>
              <td>{{date('d F, Y', strtotime($project->closing_date))}}</td>
              <td>
              @if($project->client)
                @foreach($project->client as $key => $client)
                  @if(Auth::User()->user_id != $client->user_idFk)

                    @if($key == 0)
                      <a href="{{url('user/view/'.$client->user_idFk)}}">{{$client->name}}</a>
                    @else
                    , <a href="{{url('user/view/'.$client->user_idFk)}}">{{$client->name}}</a>
                    @endif

                  @endif
                @endforeach
              @endif
              </td>
              <td>
                @if($project->agent)
                  @foreach($project->agent as $key => $agent)

                    @if(Auth::User()->user_id != $agent->user_idFk)

                      @if($key == 0)
                        <a href="{{url('user/view/'.$agent->user_idFk)}}">{{$agent->name}}</a>
                      @else
                      , <a href="{{url('user/view/'.$agent->user_idFk)}}">{{$agent->name}}</a>
                      @endif

                    @endif

                  @endforeach
                @endif
              </td>
              <td>
                @if($project->mortgage)
                  @foreach($project->mortgage as $key => $mortgage)

                     @if(Auth::User()->user_id != $mortgage->user_idFk)

                      @if($key == 0)
                        <a href="{{url('user/view/'.$mortgage->user_idFk)}}">{{$mortgage->name}}</a>
                      @else
                      , <a href="{{url('user/view/'.$mortgage->user_idFk)}}">{{$mortgage->name}}</a>
                      @endif

                    @endif
                  
                  @endforeach
                @endif
              </td>
              
              <td style="text-align:center;">
                @if(Auth::User()->user_role_idFk == 1 || Auth::User()->user_role_idFk == 6 || Auth::User()->user_role_idFk == 7)
                  <a href="{{url('project/status/'.$project->project_id)}}">
                    <i style="font-size:26px;color:#09cc09;" class="fa fa-eye" aria-hidden="true"></i>
                  </a>
                @elseif(Auth::User()->user_role_idFk == 3 || Auth::User()->user_role_idFk == 4 || Auth::User()->user_role_idFk == 2)
                  <a href="{{url('project/tracker/'.$project->project_id)}}">
                    <i style="font-size:26px;color:#09cc09;" class="fa fa-eye" aria-hidden="true"></i>
                  </a>
                @endif
              </td>
                <td style="text-align:center;">
                  <a href="{{url('chat/dashboard/'.$project->project_id)}}">
                    <i style="font-size:26px;color:#09b0cc;" class="fa fa-envelope" aria-hidden="true"></i>
                  </a>
                </td>

          @endforeach
        @else
          <tr>
              <td colspan="4">Record Not Found</td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>



@stop

@section('foot')

  @parent
   <script src="{{url('admin/vendor/datatables/media/js/jquery.dataTables.js')}}"></script>
  <script src="{{url('admin/vendor/datatables/media/js/datatables.bootstrap.js')}}"></script>

  <script type="text/javascript">
    $('.datatable').dataTable({
        "scrollX": true
      });
  </script>

@endsection