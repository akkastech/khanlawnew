@extends('admin.layout')

@section('title', 'Status List ')


@section('content')
  
  @section('page_name', 'Status List')

@if(session('success'))
  <div class="alert alert-success">
    {{session('success')}}
  </div>
@endif
@if(session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
@endif

<div class="card bg-white" data-ng-controller="tableCtrl">
  <div class="card-header">
    Status List 
  </div>
  <div class="card-block">
    <h4 style="float: left;margin-left:30px;margin-top: 20px">{{ $project->title }} ({{ $project->file_no }})</h4>
    <table class="table table-bordered table-condensed datatable m-b-0" ui-jq="dataTable" ui-options="dataTableOpt">
      <thead>
        <tr>
          <th style="text-align:center;">#No</th>
          <th style="text-align:center;">Title</th>
          <th style="text-align:center;">Checklist</th>
          <th style="text-align:center;">Status</th>
          <th style="text-align:center;">Completed Date</th>
          <th style="text-align:center;">Action</th>
        </tr>
      </thead>
      <tbody>
        @if(count($status) > 0)

          @foreach($status as $key => $sta)


            <tr>
              <td style="text-align:center;">{{$key+1}}</td>
              <td style="text-align:center;">
                @if($project->project_type == 'Purchase')

                  @if($key == 0)
                    Khan Law Receives Purchased Contact ('A Firm Deal')
                  @elseif($key == 1)
                    Khan Law Contacts Client To Collect Important ID Verification Details
                  @elseif($key == 2)
                    Khan Law Completes A Title Search To Confirm Title Qualifications On The Property
                  @elseif($key == 3)
                    Khan Law Sends Requisitions To Seller's Lawyer
                  @elseif($key == 4)
                    Lender (Bank) Sends Mortgage Instructions To Khan Law
                  @elseif($key == 5)
                    Khan Law Prepares Closing And Mortgage Documents
                  @elseif($key == 6)
                    Client Meeting With Khan Law Lawyer To Sign Documents (45 Mins)
                  @elseif($key == 7)
                    Khan Law Fulfills All Lender (Bank) Requirements And Submit Executed Documents
                  @elseif($key == 8)
                    Khan Law Receives Mortgage Proceeds And Releases Funds To Seller's Lawyer
                  @elseif($key == 9)
                    Khan Law Register The Transfer Deed
                  @elseif($key == 10)
                    Khan Law Releases The Keys To The New Buyer's
                  @elseif($key == 11)
                    Khan Law Reports To Client And Lender (Bank)
                  @endif


                @elseif($project->project_type == 'Sale')

                  @if($key == 0)
                    Khan Law Receives Purchased Contact ('A Firm Deal')
                  @elseif($key == 1)
                    Khan Law Contacts Client To Collect Important ID Verification Details
                  @elseif($key == 2)
                    Khan Law Requests Mortgage/LOC Pay-out Statment
                  @elseif($key == 3)
                    Khan Law Responds To Buyer's Lawyer's Title Requisitions
                  @elseif($key == 4)
                    Khan Law Prepares Sale Documents
                  @elseif($key == 5)
                    Client Meeting With Khan Law Lawyer To Sign Documents (45 Mins)
                  @elseif($key == 6)
                    Khan Law Sends Documents To Buyer's Lawyer For Signing And Registration
                  @elseif($key == 7)
                    
                  @elseif($key == 8)
                    Khan Law Receives Proceeds From Buyer's Lawyer
                  @elseif($key == 9)
                    Khan Law Pays Out Existing Mortgage, Realty Commission etc, And Reimburses Proceeds To Client
                  @elseif($key == 10)
                    Khan Law Discharges Seller's Financial Encumbrances And Completes All Outstanding Matters
                  @elseif($key == 11)
                    Khan Law Reports To Client
                  @endif

                @elseif($project->project_type == 'Refinance')

                  @if($key == 0)
                    Khan Law Receives Purchased Contact ('A Firm Deal')
                  @elseif($key == 1)
                    Client service portal is activated and you are notified.
                  @elseif($key == 2)
                    Khan Law completes a property title search
                  @elseif($key == 3)
                    Khan Law requests mortgage/LOC pay-out statement
                  @elseif($key == 4)
                    Khan Law receives new mortgage instructions and prepares necessary documents
                  @elseif($key == 5)
                    "Khan Law prepares closing and mortgage documents"
                  @elseif($key == 6)
                    You meet with your lawyer to review and sign required documents  (45 mins)
                  @elseif($key == 7)
                    Khan Law fulfills all lender (Bank) requirements and submits executed documents
                  @elseif($key == 8)
                    Khan Law receives mortgage proceeds and pays out existing mortgage and net proceeds are reimbursed to the clien
                  @elseif($key == 9)
                    Khan Law discharges prior financial encumbrances and completes all outstanding matters
                  @elseif($key == 10)
                    Khan Law reports to client and the lender (bank)
                  @elseif($key == 11)
                    
                  @endif

                @endif
              </td>
              <td style="text-align:center;">
                @if($project->project_type == 'Purchase')

                  @if($key == 0)
                    Contract received on
                  @elseif($key == 1)
                    Portal activated and client notified?
                  @elseif($key == 2)
                    Title search performed
                  @elseif($key == 3)
                    Requisitions sent to lawyer 
                  @elseif($key == 4)
                    Lenders instructions 
                  @elseif($key == 5)
                    Title insurance, Draft charge/transfer, Requsistion from fund docs, Client trust ledger, Vendors docs, Our docs. Advise client re insurance value and first lose payee
                  @elseif($key == 6)
                    Client meeting
                  @elseif($key == 7)
                    Send Requsition for funds
                  @elseif($key == 8)
                    Confiramtion of funding 
                    </br>
                    </br>
                    </br>
                    Closing package received
                  @elseif($key == 9)
                    
                  @elseif($key == 10)
                    
                  @elseif($key == 11)
                    
                  @endif

                @elseif($project->project_type == 'Sale')

                  @if($key == 0)
                    Contract received on
                  @elseif($key == 1)
                    Portal activated and client notified?
                  @elseif($key == 2)
                    Ask client for mortgage/LOC statements, property taxes.
                    </br></br></br>
                    Obtain payout statments 
                  @elseif($key == 3)
                    Repy to recquistions /w SOA and transfer deed
                  @elseif($key == 4)
                    prepair Vendor's doc, A&D, Dir re funds, IVF, conflict doc, 
                  @elseif($key == 5)
                    
                  @elseif($key == 6)
                    Send closing package
                    </br></br></br>
                    Prepair Client Ledger and payout letters
                  @elseif($key == 7)
                    Funds rec'd w/ copy cert cheq, PUR U/T re adj, Dir re title (w/ sign)
                  @elseif($key == 8)
                    Create checks certify, mail/discharge
                  @elseif($key == 9)
                    Sign and release transfer   
                  @elseif($key == 10)
                    create reproting  client:Trust ledg, SOA, Pur U/T re adjust, Mtg P/O, doc's sign by vendor
                  @elseif($key == 11)
                    create reproitng khan law : Trust ledger & Statment of account Scanned 
                  @endif

                @elseif($project->project_type == 'Refinance')

                  @if($key == 0)
                    Contract received on
                  @elseif($key == 1)
                    Portal activated and client notified?
                  @elseif($key == 2)
                    Title search performed 
                  @elseif($key == 3)
                    Ask client for mortgage/LOC statements, property taxes. 
                  @elseif($key == 4)
                    Lenders instructions
                  @elseif($key == 5)
                    Title insurance, Draft charge/transfer, Requsistion from fund docs, Client trust ledger,Our docs. Advise client re insurance value and first lose payee
                  @elseif($key == 6)
                    Client meeting
                  @elseif($key == 7)
                    Send Requsition for funds, prepard payout statment and checks
                    </br></br></br>
                    Confiramtion of funding 
                  @elseif($key == 8)
                    Prepare checks certify mail/payout in branch
                  @elseif($key == 9)
                    Register charge 
                  @elseif($key == 10)
                    Prepair reproting client: Reporting letter, SOA, trust ledger, Stat of Acc, U/T & vend's doc by seller, mtg doc, title ins, stand charg term, copy of transf & charge
                  @elseif($key == 11)
                    Prepair reporting lender: check instructions, docs provided by bank, A&D, title insurance, signed Mtg docs, Home ins policy
                    </br></br></br>
                    Prepair reproting khan law: TRUST LEDGER, SOA, TRANSFER & CHARGE (SCAN AND PUT IN TRUST LEDGER)
                  @endif

                @endif

              </td>
              <td style="text-align:center;">
                @if($sta->status == '1')
                  {{'Completed'}}
                @else
                  {{'Pending'}}
                @endif
              </td>
              <td style="text-align:center;">@if($sta->updated_at > $sta->created_at) {{date('d F, Y', strtotime($sta->updated_at))}} @else {{'N/A'}} @endif</td>
              <td style="text-align:center;">
                <div class="row">
                  <div class="col-md-12">
                    @if($sta->status == '1')
                      <a href="{{url('project/status/deactive/'.$sta->status_id)}}" >
                          <i class="fa fa-check" aria-hidden="true" style="font-size:20px;color:#09cc09;"></i>
                      </a>
                    @else
                      <a href="{{url('project/status/active/'.$sta->status_id)}}" >
                          <i class="fa fa-ban" aria-hidden="true" style="font-size:20px;color:#fe6767;"></i>
                      </a>
                    @endif
                  </div>
                </div>
              </td>
            </tr>

            
          @endforeach
        @else
          <tr>
              <td colspan="4">Record Not Found</td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>



@stop

@section('foot')

  @parent
   <script src="{{url('admin/vendor/datatables/media/js/jquery.dataTables.js')}}"></script>
  <script src="{{url('admin/vendor/datatables/media/js/datatables.bootstrap.js')}}"></script>

  <script type="text/javascript">
    $('.datatable').dataTable({
    paging: false
});
  </script>

@endsection