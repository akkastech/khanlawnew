@extends('admin.layout')

@section('title', 'Edit Project')


@section('content')
  <style type="text/css">
    .select2-results__options li:hover{
      background-color: #5897fb !important;
      color: white !important;
    }
  </style>
  @section('page_name', 'Edit Project')
  
  @if(session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
@endif

<div class="card bg-white">

    <div class="card-header">
      Edit Project
      @if($project->agrement) <a href="{{url('uploads/'.$project->agrement)}}" download class="btn btn-info pull-right">Download Agreement</a> @endif
    </div>
    <div class="card-block">
      
        <form class="form-horizontal" role="form" action="{{url('requested/project/update')}}" method="post"  enctype="multipart/form-data">
          <input id="token" name="_token" type="text" value="{!! csrf_token() !!}" hidden>
          <input type="hidden" value="{{$project->project_id}}" name="project_id">
          <div class="row m-a-0">
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Title</label>
                  <div class="col-sm-8">
                    <input type="text" name="title" value="{{$project->title}}" class="form-control" required="">
                  </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Project Type</label>
                  <div class="col-sm-8">
                    <select id="type" class="form-control" style="width:100%;" name="type" required="">
                      <option value="">Please Select</option>
                      <option value="Sale" @if($project->project_type == 'Sale') {{'selected'}} @endif >Sale</option>
                      <option value="Purchase" @if($project->project_type == 'Purchase') {{'selected'}} @endif >Purchase</option>
                      
                    </select>
                  </div>
                </div>
            </div>
          </div>

          <div class="row m-a-0">
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Client</label>
                  <div class="col-sm-8">
                    <select id="client_idFk" class="form-control" style="width:100%;" name="client_idFk[]" required="" multiple="true">
                      <option value="">Please Select</option>
                      @if(count($users) > 0)

                        @foreach($users as $user)

                          @if($user->user_role_idFk == 2)
                            <option value="{{$user->user_id}}" 
                            @if($project->client)
                              @foreach($project->client as $key => $client)
                                @if($client->user_idFk == $user->user_id) {{'selected'}} @endif
                              @endforeach
                            @endif
                             >{{$user->name}}</option>
                          @endif

                        @endforeach

                      @endif
                    </select>
                  </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Upload Agreement</label>
                  <div class="col-sm-8">
                    <input type="file" name="agrement" accept="application/pdf,.docx" >
                  </div>
                </div>
            </div>
          </div>

          <div class="row m-a-0">
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Opening Date</label>
                  <div class="col-sm-8">
                    <input type="text" name="opening_date" value="{{$project->opening_date}}" class="form-control date" required="">
                  </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Closing Date</label>
                  <div class="col-sm-8">
                    <input type="text" name="closing_date" value="{{$project->closing_date}}" class="form-control date" required="">
                  </div>
                </div>
            </div>
            
            
          </div>

          <div class="row">
          <div class="col-lg-8">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="10" name="desc">{{$project->description}}</textarea>
                  </div>
                </div>
            </div>
          </div>

      

  </div>
  <div class="card-footer">
    <div class="row">
      <div class="col-md-2">
        <a href="#!" onclick="window.history.back()" class="btn btn-default btn-block ">Cancel</a>
      </div>
      <div class="col-md-2 pull-right">
        <button class="btn btn-info btn-block ">Submit</button>
      </div>
    </div>
  </div>
  </form>
</div>


@stop

@section('foot')

  @parent
   <script src="{{url('admin/vendor/datatables/media/js/jquery.dataTables.js')}}"></script>
  <script src="{{url('admin/vendor/datatables/media/js/datatables.bootstrap.js')}}"></script>
    <script type="text/javascript">
    $('#agent_idFk').select2({
      placeholder: 'Select an option'
    });
    $('#client_idFk').select2({
      placeholder: 'Select an option'
    });
    $('#mortgage_idFk').select2({
      placeholder: 'Select an option'
    });
    $('#type').select2({
      placeholder: 'Select an option'
    });
    $('#opposision_idFk').select2({
      placeholder: 'Please Agent Select First'
    });
  </script>

    <script type="text/javascript">
      $(document).ready(function(){

        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
        $('.date').datepicker({
          autoclose: true,
          startDate: today,
          format: 'yyyy-mm-dd'
        });
        

        $('#agent_idFk').on('change', function(){

          var agent = $(this).val();
          var new_users = [];
          if(agent.length > 0){
            var $opposision = $('#opposision_idFk').select2({placeholder: 'Select an option'});
            
            var options = $opposision.data('select2').options.options;

            var users = [];
            @if(count($users) > 0)
              @foreach($users as $user)
                @if($user->user_role_idFk == 3)
                  users.push('{{$user->user_id}}');
                @endif
              @endforeach
            @endif

            for (var i = 0; i < agent.length ; i++) {
             
              users = jQuery.grep(users, function(value) {
                return value != agent[i];
              });

            }
            $opposision.html('');
            var names = [];
            @if(count($users) > 0)
              @foreach($users as $key => $user)
                @if($user->user_role_idFk == 3)

                  jQuery.grep(users, function(value) {
                    if(value == '{{$user->user_id}}'){
                      names.push('{{$user->name}}');
                    }
                  });

                @endif
              @endforeach
            @endif

            var items = [];
            for (var i = 0; i < users.length; i++) {

                items.push({
                    "id": users[i],
                    "text": names[i]
                });

                $opposision.append("<option value=\"" + users[i] + "\">" + names[i] + "</option>");
            }

            // add new items
            options.data = items;
            $opposision.select2(options);

            $opposision.attr('disabled', false);

          }else{
            $('#opposision_idFk option:first').html("").html('Please Agent Select First');
            $('#opposision_idFk').attr('disabled', true);

          }


        });

      });
    </script>


@endsection