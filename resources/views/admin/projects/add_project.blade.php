@extends('admin.layout')

@section('title', 'Add Project')


@section('content')
  <style type="text/css">
    .select2-results__options li:hover{
      background-color: #5897fb !important;
      color: white !important;
    }
  </style>
  @section('page_name', 'Add Project')

  @if(session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
@endif

<div class="card bg-white">

    <div class="card-header">
      Add Project
      <a href="{{url('user/add')}}" class="pull-right btn btn-info">Add User</a>
    </div>
    <div class="card-block">

        <form class="form-horizontal" role="form" action="{{url('project/submit')}}" method="post">
          <input id="token" name="_token" type="text" value="{!! csrf_token() !!}" hidden>
          <div class="row m-a-0">
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">File No.</label>
                  <div class="col-sm-8">
                    <input type="text" name="file_no" class="form-control" required="">
                  </div>
                </div>
            </div>  
            <div class="col-lg-4"></div>
                          
          </div>

          <div class="row m-a-0">
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Title</label>
                  <div class="col-sm-8">
                    <input type="text" name="title" class="form-control" required="">
                  </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Project Type</label>
                  <div class="col-sm-8">
                    <select id="type" class="form-control" style="width:100%;" name="type" required="">
                      <option value="">Please Select</option>
                      <option value="Sale">Sale</option>
                      <option value="Purchase">Purchase</option>
                      <option value="Refinance">Refinance</option>
                    </select>
                  </div>
                </div>
            </div>

          </div>

          <div class="row m-a-0">
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Realtor</label>
                  <div class="col-sm-8">
                    <select id="agent_idFk" class="form-control" style="width:100%;" name="agent_idFk[]" >
                      <option value="">Please Select</option>
                      @if(count($users) > 0)

                        @foreach($users as $user)

                          @if($user->user_role_idFk == 3)
                            <option value="{{$user->user_id}}">{{$user->name}}</option>
                          @endif

                        @endforeach

                      @endif
                    </select>
                  </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Lender</label>
                  <div class="col-sm-8">
                    <select id="mortgage_idFk" class="form-control" style="width:100%;" name="mortgage_idFk[]"  >
                      <option value="">Please Select</option>
                      @if(count($users) > 0)

                        @foreach($users as $user)

                          @if($user->user_role_idFk == 4)
                            <option value="{{$user->user_id}}">{{$user->name}}</option>
                          @endif

                        @endforeach

                      @endif
                    </select>
                  </div>
                </div>
            </div>

          </div>

          <div class="row m-a-0">
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Opposition Realtor</label>
                  <div class="col-sm-8">
                    <select id="opposision_idFk" class="form-control" style="width:100%;" disabled="" name="opposision_idFk[]" >
                      <option value="">Please Realtor Select First</option>
                      @if(count($users) > 0)

                        @foreach($users as $user)

                          @if($user->user_role_idFk == 3)
                            <option value="{{$user->user_id}}">{{$user->name}}</option>
                          @endif

                        @endforeach

                      @endif
                    </select>
                  </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Client</label>
                  <div class="col-sm-8">
                    <select id="client_idFk" class="form-control" style="width:100%;" name="client_idFk[]" multiple="true">
                      <option value="">Please Select</option>
                      @if(count($users) > 0)

                        @foreach($users as $user)

                          @if($user->user_role_idFk == 2)
                            <option value="{{$user->user_id}}">{{$user->name}}</option>
                          @endif

                        @endforeach

                      @endif
                    </select>
                  </div>
                </div>
            </div>



          </div>

          <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Lawyer</label>
                  <div class="col-sm-8">
                    <select id="laywer_idFk" class="form-control" style="width:100%;" name="laywer_idFk[]">
                      <option value="">Please Select</option>
                      @if(count($users) > 0)

                        @foreach($users as $user)

                          @if($user->user_role_idFk == 6)
                            <option value="{{$user->user_id}}">{{$user->name}}</option>
                          @endif

                        @endforeach

                      @endif
                    </select>
                  </div>
                </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Opening Date</label>
                  <div class="col-sm-8">
                    <input type="text" name="opening_date" class="form-control date" required="">
                  </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Closing Date</label>
                  <div class="col-sm-8">
                    <input type="text" name="closing_date" class="form-control date" required="">
                  </div>
                </div>
            </div>
          </div>

          <div class="row">
          <div class="col-lg-8">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="10" name="desc"></textarea>
                  </div>
                </div>
            </div>
          </div>



  </div>
  <div class="card-footer">
    <div class="row">
      <div class="col-md-2">
        <a href="#!" onclick="window.history.back()" class="btn btn-default btn-block ">Cancel</a>
      </div>
      <div class="col-md-2 pull-right">
        <button class="btn btn-info btn-block ">Submit</button>
      </div>
    </div>
  </div>
  </form>
</div>


@stop

@section('foot')

  @parent
   <script src="{{url('admin/vendor/datatables/media/js/jquery.dataTables.js')}}"></script>
  <script src="{{url('admin/vendor/datatables/media/js/datatables.bootstrap.js')}}"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>


    <script type="text/javascript">

    $('#agent_idFk').select2();
    $('#client_idFk').select2();
    $('#mortgage_idFk').select2();
    $('#type').select2();
    $('#opposision_idFk').select2();
    $('#laywer_idFk').select2();
  </script>

    <script type="text/javascript">
      $(document).ready(function(){

          $('#type').on('change', function(){

            var value = $(this).val();
            if(value == 'Refinance'){
              $('#opposision_idFk').val(' ');
              $('#agent_idFk').val(' ');
              $('#agent_idFk').attr('required', false);
              $('#opposision_idFk').closest('.form-group').hide();
              $('#agent_idFk').closest('.form-group').hide();
            }else{
              $('#opposision_idFk').closest('.form-group').show();
              $('#agent_idFk').closest('.form-group').show();
            }

            if(value == 'Sale'){
              $('#mortgage_idFk').val(' ');
              $('#mortgage_idFk').attr('required', false);
              $('#mortgage_idFk').closest('.form-group').hide();
              $('#opposision_idFk').closest('.form-group').show();
              $('#agent_idFk').closest('.form-group').show();
            }
            if(value == 'Purchase'){
              $('#mortgage_idFk').val(' ');
              $('#mortgage_idFk').attr('required', false);
              $('#mortgage_idFk').closest('.form-group').show();
              $('#opposision_idFk').closest('.form-group').show();
              $('#agent_idFk').closest('.form-group').show();
            }



          });

          var nowDate = new Date();
          var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
          $('.date').datepicker({
            autoclose: true,
            startDate: today,
            format: 'yyyy-mm-dd'
          });

        $('#agent_idFk').on('change', function(){

          var agent = $(this).val();
          var new_users = [];
          if(agent.length > 0){
            var $opposision = $('#opposision_idFk').select2({placeholder: 'Select an option'});

            var options = $opposision.data('select2').options.options;

            var users = [];
            @if(count($users) > 0)
              @foreach($users as $user)
                @if($user->user_role_idFk == 3)
                  users.push('{{$user->user_id}}');
                @endif
              @endforeach
            @endif

            Array.prototype.remove = function() {
                var what, a = arguments, L = a.length, ax;
                while (L && this.length) {
                    what = a[--L];
                    while ((ax = this.indexOf(what)) !== -1) {
                        this.splice(ax, 1);
                    }
                }
                return this;
            };

            users.remove(agent);


            $opposision.html('');
            var names = [];
            @if(count($users) > 0)
              @foreach($users as $key => $user)
                @if($user->user_role_idFk == 3)

                  jQuery.grep(users, function(value) {
                    if(value == '{{$user->user_id}}'){
                      names.push('{{$user->name}}');
                    }
                  });

                @endif
              @endforeach
            @endif

            var items = [];
            for (var i = 0; i < users.length; i++) {

                items.push({
                    "id": users[i],
                    "text": names[i]
                });

                if(i == 0){
                  $opposision.append("<option value=' '>Please Select</option><option value=\"" + users[i] + "\">" + names[i] + "</option>");
                }else{
                  $opposision.append("<option value=\"" + users[i] + "\">" + names[i] + "</option>");
                }
            }

            // add new items
            options.data = items;
            $opposision.select2(options);

            $opposision.attr('disabled', false);

          }else{
            $('#opposision_idFk option:first').html("").html('Please Agent Select First');
            $('#opposision_idFk').attr('disabled', true);

          }


        });

      });



    </script>


@endsection
