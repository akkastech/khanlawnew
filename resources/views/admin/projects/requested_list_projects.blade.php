@extends('admin.layout')

@section('title', 'Requested Project List ')


@section('content')
  
  @section('page_name', 'Requested Project List')

@if(session('success'))
  <div class="alert alert-success">
    {{session('success')}}
  </div>
@endif
@if(session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
@endif

<div class="card bg-white" data-ng-controller="tableCtrl">
  <div class="card-header">
    Project List @if(Auth::User()->user_role_idFk == 3) <a href="{{url('request/project/add')}}" class="pull-right btn btn-info">Add Request</a> @endif
  </div>
  <div class="card-block">
    <table class="table table-bordered table-condensed datatable m-b-0" ui-jq="dataTable" ui-options="dataTableOpt">
      <thead>
        <tr>
          <th>Title</th>
          <th>Opening Date</th>
          <th>Closing Date</th>
          <th>Type</th>
          <th>Client</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @if(count($projects) > 0)

          @foreach($projects as $project)
            <tr>
              <td>{{$project->title}}</td>
              <td>{{date('d F, Y', strtotime($project->opening_date))}}</td>
              <td>{{date('d F, Y', strtotime($project->closing_date))}}</td>
              <td>{{$project->project_type}}</td>
              <td>
              @if($project->client)
                @foreach($project->client as $key => $client)
                  @if($key == 0)
                    {{$client->name}}
                  @else
                  , {{$client->name}}
                  @endif
                @endforeach
              @endif
              </td>
              <td>
                @if($project->project_status == '0')
                  {{'Pending'}}
                @elseif($project->project_status == '2')
                  {{'Rejected'}}
                @elseif($project->project_status == '1')
                  {{'Accepted'}}
                @endif
              </td>
              <td>
                <div class="row">
                  @if(Auth::User()->user_role_idFk == 1)

                    <div class="col-md-4">
                      <a href="{{url('requested/project/reject/'.$project->project_id)}}" >
                          <i class="fa fa-ban" aria-hidden="true" style="font-size:20px;color:#fe6767;"></i>
                      </a>
                    </div>
                    <div class="col-md-4">
                      <a href="{{url('requested/project/accept/'.$project->project_id)}}" >
                          <i class="fa fa-check" aria-hidden="true" style="font-size:20px;color:#09cc09;"></i>
                      </a>
                    </div>

                  @else

                    @if($project->project_status != '1')
                      <div class="col-md-4">
                        <a href="{{url('requested/project/edit/'.$project->project_id)}}" >
                            <i class="fa fa-pencil" aria-hidden="true" style="font-size:20px;color:#1ebcfb;"></i>
                        </a>
                      </div>
                      <div class="col-md-4">
                        <a class="delete" href="{{url('project/delete/'.$project->project_id)}}" >
                            <i class="fa fa-trash-o" aria-hidden="true" style="font-size:20px;color:#fe6767;"></i>
                        </a>
                      </div>
                    @else

                      @if($project->agrement) 
                        <a href="{{url('uploads/'.$project->agrement)}}" download class="btn btn-info pull-right">Download Agreement</a>
                      @endif
                      
                    @endif

                  @endif
                </div>
              </td>
            </tr>
          @endforeach
        @else
          <tr>
              <td colspan="4">Record Not Found</td>
          </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>



@stop

@section('foot')

  @parent
   <script src="{{url('admin/vendor/datatables/media/js/jquery.dataTables.js')}}"></script>
  <script src="{{url('admin/vendor/datatables/media/js/datatables.bootstrap.js')}}"></script>

  <script type="text/javascript">
    $('.datatable').dataTable({
        "scrollX": true
    });
    $('.delete').on('click',function(event) {
        var r = window.confirm('Are you sure to delete this requested Project');
        if (r != true) {
          event.preventDefault();
        }
    });
  </script>

@endsection