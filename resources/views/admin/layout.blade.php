<?php

$projects = '';
  if(Auth::User()->user_role_idFk == 1){
      $projects = App\Projects::where('project_status', '1')->get();
  }else{

      $projects = Auth::User()->projects;
      $project_id_array = [];
      if(count($projects) > 0){

          foreach($projects as $key => $pro){
              $project_id_array[$key] = $pro->project_idFk;
          }

      }

      $projects = App\Projects::where('project_status', '1')->whereIn('project_id', $project_id_array)->get();
  }
  // dd($projects);
 ?>
<!DOCTYPE html>
<html>
  <head>

    <title>Portal - @yield('title')</title>
    @include('admin.master.head')

  </head>
  <body class="page-loading" style="overflow: auto;">
    <!-- page loading spinner -->
    {{-- <div class="pageload">
      <div class="pageload-inner">
        <div class="sk-rotating-plane"></div>
      </div>
    </div> --}}

    <!-- /page loading spinner -->
    <div class="app layout-fixed-header">
        @include('admin.master.sidebar')

        <div class="main-panel">
          <!-- top header -->
      <div class="header navbar">
        <div class="brand visible-xs">
          <!-- toggle offscreen menu -->
          <div class="toggle-offscreen">
            <a href="javascript:;" class="hamburger-icon visible-xs" data-toggle="offscreen" data-move="ltr">
              <span></span>
              <span></span>
              <span></span>
            </a>
          </div>
          <!-- /toggle offscreen menu -->
          <!-- logo -->
          <a class="brand-logo">
            <span>Khan LAW</span>
          </a>
          <!-- /logo -->
        </div>
        <ul class="nav navbar-nav hidden-xs">
          <li>
            <a href="javascript:;" class="small-sidebar-toggle ripple" data-toggle="layout-small-menu">
              <i class="icon-toggle-sidebar"></i>
            </a>
          </li>
          <li style="color: white; margin-top: 6%; font-size: 15pt;">@yield('page_name')</li>
        </ul>
        <ul class="nav navbar-nav navbar-right hidden-xs" style="margin-right:200px;">
          <li class="">
            <a href="javascript:;" class="ripple" style="background-color: black;" data-toggle="dropdown" aria-expanded="true">
              <img src="{{url('uploads/'.Auth::User()->img)}}" style="width: 30px; height: 30px;" class="header-avatar img-circle" alt="user" title="user">
              <span style="font-size: 12pt;">{{AUth::User()->name}}</span>
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li>
                <a href="{{url('settings')}}">Settings</a>
              </li>
              <!-- <li>
                <a href="javascript:;">
                  <span class="label bg-danger pull-right">34</span>
                  <span>Notifications</span>
                </a>
              </li> -->
              <li role="separator" class="divider"></li>
              <!-- <li>
                <a href="javascript:;">Help</a>
              </li> -->
              <li>
                <a href="{{url('logout')}}">Logout</a>
              </li>
            </ul>
          </li>
          <!-- <li>
            <a href="javascript:;" class="ripple" data-toggle="dropdown">
              <i class="icon-bell"></i>
            </a>
            <ul class="dropdown-menu notifications">
              <li class="notifications-header">
                <p class="text-muted small">You have 3 new messages</p>
              </li>
              <li>
                <ul class="notifications-list">
                  <li>
                    <a href="javascript:;">
                      <div class="notification-icon">
                        <div class="circle-icon bg-success text-white">
                          <i class="icon-bulb"></i>
                        </div>
                      </div>
                      <span class="notification-message"><b>Sean</b> launched a new application</span>
                      <span class="time">2s</span>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;">
                      <div class="notification-icon">
                        <div class="circle-icon bg-danger text-white">
                          <i class="icon-cursor"></i>
                        </div>
                      </div>
                      <span class="notification-message"><b>Removed calendar</b> from app list</span>
                      <span class="time">4h</span>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;">
                      <div class="notification-icon">
                        <div class="circle-icon bg-primary text-white">
                          <i class="icon-basket"></i>
                        </div>
                      </div>
                      <span class="notification-message"><b>Denise</b> bought <b>Urban Admin Kit</b></span>
                      <span class="time">2d</span>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;">
                      <div class="notification-icon">
                        <div class="circle-icon bg-info text-white">
                          <i class="icon-bubble"></i>
                        </div>
                      </div>
                      <span class="notification-message"><b>Vincent commented</b> on an item</span>
                      <span class="time">2s</span>
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;">
                      <span class="notification-icon">
                      <img src="{{url('admin/images/face3.jpg')}}" class="avatar img-circle" alt="">
                      </span>
                      <span class="notification-message"><b>Jack Hunt</b> has <b>joined</b> mailing list</span>
                      <span class="time">9d</span>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li> -->
          <li>
            <ul class="dropdown-menu">
              <li>
                <a href="{{url('settings')}}">Settings</a>
              </li>
              <!-- <li>
                <a href="javascript:;">
                  <span class="label bg-danger pull-right">34</span>
                  <span>Notifications</span>
                </a>
              </li> -->
              <li role="separator" class="divider"></li>
              <!-- <li>
                <a href="javascript:;">Help</a>
              </li> -->
              <li>
                <a href="{{url('logout')}}">Logout</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="javascript:;" class="ripple" data-toggle="layout-chat-open">
              <i class="icon-user"></i>
            </a>
          </li>
        </ul>
      </div>
      <!-- /top header -->



           <div class="main-content">@yield('content')</div>

        </div>


    </div>

    <div class="modal fade" style="z-index: 999999;" id="contact_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="confirmation">Contact KhanLaw</h4>
            </div>
            <div class="modal-body">

                <div class="row">

                <form action="{{url('contact/email/send')}}"  method="post" enctype="multipart/form-data">
                  <div class="col-md-6 col-sm-12 ">
                    <select id="agent_idFk" class="form-control" style="width:100%;" name="agent_idFk[]" required=""><span class="glyphicon glyphicon-remove"></span>
                      <option value="">Please Select</option>
                      @if(count($projects) > 0)

                        @foreach($projects as $project)
                          <option value="{{$project->title}}">{{$project->title}}</option>
                        @endforeach

                      @endif
                    </select>
                  </div>
                  <div class="col-md-6 col-sm-12 ">
                    <div class="form-group">
                      <label>Attach a File</label>
                      <input name="files[]" id="file" type="file" multiple>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-12 ">
                    <div class="form-group">
                      <label>Body</label>
                      <textarea class="form-control" name="body" id="body" rows="10"></textarea>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <button type="submit" class="btn_1 btn btn-info green pull-right">Submit</button>
                  </div>
                </form>

              </div><!-- End row -->

            </div>
        </div>
    </div>
</div>


 <div class="modal fade" style="z-index: 999999;" id="mail_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="border-radius: 5px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal2" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="confirmation">Invite Realtor/Lender</h4>
            </div>
            <div class="modal-body">

                <div class="row">

                <form action="{{url('invite/email/send')}}"  method="post" enctype="multipart/form-data">
                  <div class="col-md-6 col-sm-12 ">
                    <div class="form-group">
                      <label>Email Address</label>
                      <input type="text" name="name" required="" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-12 ">
                    <div class="form-group">
                      <label>Body</label>
                      <textarea class="form-control" name="body" id="body" rows="10"></textarea>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <button type="submit" class="btn_1 btn btn-info green pull-right">Submit</button>
                  </div>
                </form>

              </div><!-- End row -->

            </div>
        </div>
    </div>
</div>

    <!-- ./wrapper -->
    @section('foot')
      @include('admin.master.foot')

    @show
  </body>
</html>
