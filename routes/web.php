<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middlewareGroups' => 'web'], function () {

    // email url redirect to chat
    Route::get('chat/hiturl/{project_id}/{user_id}', 'ChatController@hiturl_for_chat');


    // For AuthController
    Route::group(['middleware' => 'unAuthenticated'], function() {

        Route::get('/', function () {
            return view('errors.soon');
        });

        Route::get('login', 'Auth\LoginController@login_page');
        Route::get('signup', 'Auth\LoginController@signup_page');
        Route::post('authuser', 'Auth\LoginController@authenticateUser');
        Route::post('register', 'Auth\RegisterController@registration');
        Route::get('facebook/login' , 'Auth\LoginController@facebook_login_redirect');
        Route::get('facebook/callback', 'Auth\LoginController@facebook_login_callback');
        Route::get('google/login' , 'Auth\LoginController@google_login_redirect');
        Route::get('google/callback', 'Auth\LoginController@google_login_callback');
    });

    Route::get('logout', 'Auth\LoginController@logout');

    // Auth User
    Route::group(['middleware' => 'Authenticated'], function() {

        Route::get('dashboard', 'DashboardController@dashboard');
        Route::post('contact/email/send', 'DashboardController@contact_email');
        Route::post('invite/email/send', 'DashboardController@invite_email');
        Route::get('settings', 'ProfileController@setting_page');
        Route::post('user/profile/update', 'ProfileController@profile_update');
        Route::get('check_password/{password}', 'ProfileController@check_password');
        Route::post('user/passwrod/update', 'ProfileController@profile_password');

        // users list
        Route::get('user/view/{id}', 'UserController@user_view');
        Route::get('users/list', 'UserController@users_list');
        Route::get('user/add', 'UserController@users_add_page');
        Route::post('user/submit', 'UserController@user_submit');
        Route::get('user/edit/{user_id}', 'UserController@user_edit_page');
        Route::post('user/update', 'UserController@user_update');
        Route::get('user/active/{user_id}', 'UserController@user_active');
        Route::get('user/deactive/{user_id}', 'UserController@user_deactive');
        Route::get('user/delete/{user_id}', 'UserController@user_delete');

        // portals list
        Route::get('portals/list', 'PortalController@portals_list');

        Route::get('project/tracker/page', 'PortalController@tracker_list_page');
        Route::get('project/tracker/{project_id}', 'PortalController@tracker_list');

        // chat
        Route::get('chat/dashboard/{project_id}', 'ChatController@chat_page');
        Route::get('message/send', 'ChatController@message_send');
        Route::post('message/docs/{project_id}', 'ChatController@message_docs');
        Route::post('message/img/{project_id}', 'ChatController@message_img');
        Route::get('message/check/new/{chat_id}', 'ChatController@message_check_new');
        Route::get('message/get/new/{chat_id}', 'ChatController@message_get_new');

        // brokers
        Route::get('brokers/list', 'RealtorsController@list_brokers');
        Route::get('realtors/list/{id}', 'RealtorsController@list_realtors');
        Route::get('realtors/add/{id}', 'RealtorsController@add_realtors');
        Route::post('realtors/submit', 'RealtorsController@submit_realtors');
        Route::get('realtors/delete/{id}', 'RealtorsController@delete_realtors');
        Route::get('projects/view/{id}', 'RealtorsController@view_projects');

        // requested projects
        Route::get('requested/projects/list', 'ProjectController@request_project_list');
        Route::get('request/project/add', 'ProjectController@request_project_add');
        Route::post('requested/project/submit', 'ProjectController@request_project_submit');
        Route::get('requested/project/edit/{project_id}', 'ProjectController@request_project_edit');
        Route::post('requested/project/update', 'ProjectController@request_project_update');

        Route::get('requested/project/reject/{project_id}', 'ProjectController@request_project_reject');
        Route::get('requested/project/accept/{project_id}', 'ProjectController@request_project_accept');

        Route::get('project/delete/{project_id}', 'ProjectController@project_delete');

        // admin area
        Route::get('projects/closed/list', 'ProjectController@project_closed_list');

        Route::get('projects/list', 'ProjectController@project_list');

        Route::group(['middleware' => 'adminAuth'], function() {

            // projects

            Route::get('project/add', 'ProjectController@project_add_page');
            Route::post('project/submit', 'ProjectController@project_submit');
            Route::get('project/edit/{project_id}', 'ProjectController@project_edit_page');
            Route::post('project/update', 'ProjectController@project_update');
            Route::get('project/complete/{project_id}', 'ProjectController@project_complete');

            // Route::get('project/deactive/{project_id}', 'ProjectController@project_deactive');




            // project status
            Route::get('project/status/{project_id}', 'ProjectController@project_status_page');
            Route::get('project/status/deactive/{status_id}', 'ProjectController@project_status_deactive');
            Route::get('project/status/active/{status_id}', 'ProjectController@project_status_active');

        });

        // user area
        Route::group(['middleware' => 'user'], function() {

        });


    });


});
