<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'chat';
    protected $primaryKey = 'chat_id';

    public function user(){
        return $this->belongsTo('App\User', 'user_idFk', 'user_id');
    }

    public function file(){
        return $this->hasOne('App\ChatMedia', 'chat_idFk', 'chat_id')->where('media_type', '!=', 'img');
    }
    public function imgs(){
        return $this->hasMany('App\ChatMedia', 'chat_idFk', 'chat_id')->where('media_type', 'img');
    }
}
