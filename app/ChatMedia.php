<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatMedia extends Model
{
    protected $table = 'chat_media';
    protected $primaryKey = 'media_id';
}
