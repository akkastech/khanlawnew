<?php

namespace App\Http\Controllers;

// use Request;
use Illuminate\Http\Request;
use App\Chat;
use App\Projects;
use App\Mailers\Mailer;
use App\User;

class DashboardController extends Controller
{
	protected $mailer;
    function __construct(Mailer $mailer){

        $this->mailer = $mailer;

    }

    public function dashboard()
    {

    	$chat = Chat::orderBy('chat_id', 'desc')->get();

    	return view('admin.dashboard', compact('chat'));
    }

    public function contact_email(Request $request)
    {
        // email
        $view = 'admin.mail2';
        $subject = $request->title;
        $data['message_mail'] = $request->body;
                $filename_array = [];
                if($request->hasFile('files')){

                    foreach ($request->file('files') as $key => $value) {

                        $filename_array[$key] = $value->getClientOriginalName();
                        $path = $value->move('uploads/attachments/', $value->getClientOriginalName());
                    }
                }

                $data['attachments'] = $filename_array;
        $this->mailer->sendTo(User::findOrFail(1)->email, $subject, $view, $data);

        return redirect()->back()->with('success', 'Message send successfully');
    }

    public function invite_email(Request $request)
    {
    	// email
        $view = 'admin.mail2';
        $subject = $request->name;
        $data['message_mail'] = $request->body;
        $this->mailer->sendTo(User::findOrFail(1)->email, $subject, $view, $data);

        return redirect()->back()->with('success', 'Message send successfully');
    }
}
