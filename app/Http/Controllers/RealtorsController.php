<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Realtors;
use App\User;
use App\ProjectUsers;

class RealtorsController extends Controller
{
    public function list_brokers()
    {
      $brokers = User::where('user_role_idFk', 5)->get();
      $realtors = Realtors::all();
      
      return view('admin.brokers.list_brokers', compact('brokers','realtors'));
    }

    public function list_realtors($id)
    {
      $realtors = Realtors::where('broker_idFk', $id)->get();


      return view('admin.brokers.list_realtors', compact('realtors','id'));
    }

    public function add_realtors($id)
    {
      $users = User::where('user_role_idFk', 3)->get();
      $realtors = Realtors::all();

      return view('admin.brokers.add_realtor', compact('realtors','id','users'));
    }

    public function submit_realtors(Request $request)
    {
      // dd($request);
        if(count($request->realtors) > 0){
            foreach ($request->realtors as $value) {
              $real = new Realtors();
              $real->broker_idFk  =   $request->user_id;
              $real->realtor_idFk = $value;
              $real->save();

          }
      }

      return redirect('realtors/list/'.$request->user_id);
    }

    public function delete_realtors($id)
    {
      $del = Realtors::where('realtor_idFk',$id)->delete();
      return redirect()->back();
    }

    public function view_projects($id)
    {
      $projects = ProjectUsers::where('user_idFk',$id)->get();
      // dd($projects);
      return view('admin.brokers.list_projects',compact('projects'));
    }




}
