<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;

class ProfileController extends Controller
{
    public function setting_page()
    {
    	$user = Auth::User();

    	return view('admin.user.profile', compact('user'));
    }

    public function profile_update(Request $request)
    {
    	$user = User::findOrFail(Auth::User()->user_id);

        $user->name = $request->name;
        $user->number = $request->number;
        $user->country = $request->country;
        $user->province = $request->province;
        $user->city = $request->city;
        $user->country_idFk = $request->country_idFk;
        $user->province_idFk = $request->province_idFk;
        $user->city_idFk = $request->city_idFk;


        if($request->hasFile('img')){

            $filename = 'profile_2017'.str_random(40);
            
            $request->file('img')->move('uploads/', $filename);
            $user->img = $filename;
        }

        $user->save();

        return back()->with('success', 'Profile Updated Successfuly');
    }

    public function check_password($password)
    {
        $email = Auth::User()->email;
        
        if (Auth::attempt(['email' => $email, 'password' => $password])){
                     
            return 'true';

        }else{

            return 'false';

        }
    }

    public function profile_password(Request $request)
    {
        $user = User::findOrFail(Auth::User()->user_id);

        $user->password = Hash::make($request->password);

        $user->save();

        return redirect()->back()->with('success', 'Password Update Successfully');
    }
}
