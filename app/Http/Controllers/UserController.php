<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserRoles;
use Hash;
use Auth;
use App\Mailers\Mailer;

class UserController extends Controller
{
    protected $mailer;
    function __construct(Mailer $mailer){

        $this->mailer = $mailer;

    }

    public function user_view($id)
    {
        $user = User::findOrFail($id);

        return view('admin.user.user_view', compact('user'));
    }


    public function users_list()
    {
        $user = User::where('notification', '=', '0')->get();
        // dd($user);
        if (count($user) > 0) {
            foreach ($user as $key => $value) {

                $new = User::findOrFail($value->user_id);
                $new->notification = 1;

                $new->save();

            }
        }

    	$role_ids = array('2', '3', '4', '5', '6', '7');

    	if(Auth::User()->user_role_idFk == 3 || Auth::User()->user_role_idFk == 4 || Auth::User()->user_role_idFk == 6){

    		$role_ids = array('2');

    	}

        if(Auth::User()->user_role_idFk == 1 || Auth::User()->user_role_idFk == 6 || Auth::User()->user_role_idFk == 7){

            $users = User::whereIn('user_role_idFk', $role_ids)->orderBy('user_id', 'acs')->get();

        }else{

            $users = User::whereIn('user_role_idFk', $role_ids)->where('created_by', Auth::User()->user_id)->orderBy('user_id', 'acs')->get();

        }


    	return view('admin.user.list_users', compact('users'));
    }

    public function users_add_page()
    {
    	$role_ids = array('2', '3', '4', '5', '6', '7');

        if(Auth::User()->user_role_idFk == 3 || Auth::User()->user_role_idFk == 4 ){

            $role_ids = array('2');

        }

    	$roles = UserRoles::whereIn('role_id', $role_ids)->get();

    	return view('admin.user.add_user', compact('roles'));
    }

    public function user_submit(Request $request)
    {

        // dd($request);
    	$check = User::where('email', $request->email)->exists();

        if($check == true)
            return back()->with('error', 'Email Already Register..');

        if($request->role == '2' ||  $request->role == '3' || $request->role == '4' || $request->role == '5' || $request->role == '6' || $request->role == '7'){

            $user = new User();

            $user->name = $request->name;
            $user->email = $request->email;
            $user->number = $request->number;
            $user->country = $request->country;
            $user->province = $request->province;
            $user->city = $request->city;
            $user->country_idFk = $request->country_idFk;
            $user->province_idFk = $request->province_idFk;
            $user->city_idFk = $request->city_idFk;
            $user->password = Hash::make($request->password);

            $user->user_role_idFk = $request->role;
            $user->created_by = Auth::User()->user_id;

            $user->save();

            // email
            $view = 'admin.mail';
            $subject = 'Welcome';
            $data['noti'] = 'This is a notication!';

            $this->mailer->sendTo($user->email, $subject, $view, $data);

            return redirect('users/list')->with('success', 'Registration Successfully Completed.');

        }else{

            return back()->with('error', 'Cannot proccess try again later');

        }
    }

    public function user_edit_page($user_id)
    {
    	$role_ids = array('2', '3', '4', '5', '6', '7');

        if(Auth::User()->user_role_idFk == 3 || Auth::User()->user_role_idFk == 4 || Auth::User()->user_role_idFk == 6){

            $role_ids = array('2');

        }

    	$roles = UserRoles::whereIn('role_id', $role_ids)->get();
    	$user = User::findOrFail($user_id);

    	return view('admin.user.edit_user', compact('user', 'roles'));
    }

    public function user_update(Request $request)
    {
    	$check = User::where('email', $request->email)->where('user_id', '!=', $request->user_id)->exists();

        if($check == true)
            return back()->with('error', 'Email Already Register..');

        if($request->role == '2' ||  $request->role == '3' || $request->role == '4' || $request->role == '5' || $request->role == '6' || $request->role == '7'){

            $user = User::findOrFail($request->user_id);

            $user->name = $request->name;
            $user->email = $request->email;
            $user->number = $request->number;
            $user->country = $request->country;
            $user->province = $request->province;
            $user->country_idFk = $request->country_idFk;
            $user->province_idFk = $request->province_idFk;
            $user->city_idFk = $request->city_idFk;
            $user->city = $request->city;
            $user->password = Hash::make($request->password);

            $user->user_role_idFk = $request->role;

            $user->save();

            return redirect('users/list')->with('success', 'User Updated Successfully');

        }else{

            return back()->with('error', 'Cannot proccess try again later');

        }
    }

    public function user_active($user_id)
    {
    	User::where('user_id', $user_id)->update(['user_status' => '1']);

    	return back()->with('success', 'User Activated Successfully');
    }

    public function user_deactive($user_id)
    {
        User::where('user_id', $user_id)->update(['user_status' => '0']);

        return back()->with('success', 'User Deactivated Successfully');
    }

    public function user_delete($user_id)
    {
    	User::where('user_id', $user_id)->delete();

    	return back()->with('success', 'User Deleted Successfully');
    }


}
