<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chat;
use App\ChatMedia;
use App\Projects;
use App\ProjectUsers;
use App\UnReadChat;
use App\User;
use Auth;
use Carbon\Carbon;
use URL;
use App\Mailers\Mailer;

class ChatController extends Controller
{

    protected $mailer;
    function __construct(Mailer $mailer){

        $this->mailer = $mailer;

    }

    public function hiturl_for_chat($project_id, $user_id)
    {
      $user = User::findOrFail($user_id);
      Auth::login($user);

      return redirect('chat/dashboard/'.$project_id);

    }

    private function send_mail($email, $userID,  $message, $project_id)
    {
        // email
        $view = 'admin.mail3';
        $subject = 'Welcome';
        $data['noti'] = 'This is a notication!';
        $data['message_mail'] = $message;
        $data['link'] = 'chat/hiturl/'.$project_id.'/'.$userID;

        $this->mailer->sendTo($email, $subject, $view, $data);
    }

    public function chat_page($project_id)
    {
    	$chats = Chat::where('project_idFk', $project_id)->whereNotNull('message')->get();
    	$pro = Projects::findOrFail($project_id);
      $projects = Projects::where('project_status', '1')->get();

      $un_read = UnReadChat::where('project_idFk', $project_id)->where('user_idFk', Auth::User()->user_id)->exists();

      if($un_read == true){

        UnReadChat::where('project_idFk', $project_id)->where('user_idFk', Auth::User()->user_id)->update(['status' => '1']);

      }

    	return view('admin.chat.chat', compact('chats', 'pro', 'projects'));
    }

    public function message_send()
    {
    	$message = $_GET['message'];
    	$project_id = $_GET['project_id'];

    	if(isset($message)){

    		$chat = new chat();

    		$chat->project_idFk = $project_id;
    		$chat->message = $message;
    		$chat->type = 'message';
    		$chat->user_idFk = Auth::User()->user_id;

    		$chat->save();

        $users = ProjectUsers::where('project_idFk', $project_id)->get();

        if(count($users) > 0){
          foreach($users as $user){

            if(Auth::User()->user_id != $user->user_idFk){
              $this->send_mail(User::findOrFail($user->user_idFk)->email, $user->user_idFk,  $message, $_GET['project_id']);

              $check = UnReadChat::where('project_idFk', $user->project_idFk)->where('user_idFk', $user->user_idFk)->exists();

              if($check == true){
                UnReadChat::where('project_idFk', $user->project_idFk)->where('user_idFk', $user->user_idFk)->update(['status' => '0']);
              }else{

                $new = new UnReadChat();

                $new->project_idFk = $user->project_idFk;
                $new->user_idFk = $user->user_idFk;

                $new->save();

              }

            }

          }
        }


    		$message_div = '<div class="card-block">
                              <div class="profile-timeline-header">
                                <a href="#" class="profile-timeline-user" style="float:right;">
                                  <img src="'.URL::to('/').'/uploads/'.Auth::User()->img.'" alt="" class="img-rounded">
                                </a>
                                <div class="profile-timeline-user-details" style="float:right;">
                                  <a href="#" class="bold">'.$chat->user->name.'</a>
                                </div>
                              </div>
                              <div class="profile-timeline-content" style="margin-left:0px;margin-right:63px;text-align:right;">
                                <p style="margin-bottom:0px;">'.Carbon::createFromTimeStamp(strtotime($chat->created_at))->diffForHumans().'</p>
                                <p class="other-text-box" style="background-color: #bbead8;">'.$chat->message.'</p>
                              </div>
                            </div>';

            $data['html'] = $message_div;
    		$data['last'] = $chat->chat_id;
    		return $data;

    	}
    }

    public function message_docs(Request $request, $project_id)
    {
    	if($request->hasFile('docs')){

    		$chat = new Chat();
    		$chat->type = 'file';
    		$chat->project_idFk = $project_id;
    		$chat->message = 'file';
    		$chat->user_idFk = Auth::User()->user_id;

    		$chat->save();

    		$filename = 'docs'.str_random(40).'.'.$request->file('docs')->getClientOriginalExtension();
    		$request->file('docs')->move('uploads/', $filename);

    		$media = new ChatMedia();

    		$media->chat_idFk = $chat->chat_id;
    		$media->media_type = $request->file('docs')->getClientOriginalExtension();
    		$media->value = $filename;

    		$media->save();

        $users = ProjectUsers::where('project_idFk', $project_id)->get();

        if(count($users) > 0){
          foreach($users as $user){

            if(Auth::User()->user_id != $user->user_idFk){
              $anchor = '<a href="'.url('uploads/'.$filename).'" target="_blank" >Document</a>';
              $this->send_mail(User::findOrFail($user->user_idFk)->email, $anchor);

              $check = UnReadChat::where('project_idFk', $user->project_idFk)->where('user_idFk', $user->user_idFk)->exists();

              if($check == true){
                UnReadChat::where('project_idFk', $user->project_idFk)->where('user_idFk', $user->user_idFk)->update(['status' => '0']);
              }else{

                $new = new UnReadChat();

                $new->project_idFk = $user->project_idFk;
                $new->user_idFk = $user->user_idFk;

                $new->save();

              }

            }

          }
        }

           	$file_type = '<i style="font-size: 22px;padding: 0px 10px;" class="fa fa-file-excel-o" aria-hidden="true"></i>';
           	if($request->file('docs')->getClientOriginalExtension() == 'pdf'){
           		$file_type = '<i style="font-size: 22px;padding: 0px 10px;" class="fa fa-file-pdf-o" aria-hidden="true"></i>';
           	}

           	$message_div = '<div class="card-block">
                              <div class="profile-timeline-header">
                                <a href="#" class="profile-timeline-user" style="float:right;">
                                  <img src="'.URL::to('/').'/uploads/'.Auth::User()->img.'" alt="" class="img-rounded">
                                </a>
                                <div class="profile-timeline-user-details" style="float:right;">
                                  <a href="#" class="bold">'.$chat->user->name.'</a>
                                </div>
                              </div>
                              <div class="profile-timeline-content" style="margin-left:0px;margin-right:63px;text-align:right;">
                                <p style="margin-bottom:0px;">'.Carbon::createFromTimeStamp(strtotime($chat->created_at))->diffForHumans().'</p>
                                <p class="other-text-box" style="background-color: #bbead8;"><a href="'.URL::to('/').'/uploads/'.$media->value.'">'.$file_type.'</a></p>
                              </div>
                            </div>';

            $data['html'] = $message_div;
    		$data['last'] = $chat->chat_id;
    		return $data;

        }

    }

    public function message_img(Request $request, $project_id)
    {
    	if($request->hasFile('imgs')){

    		$chat = new Chat();
    		$chat->type = 'img';
    		$chat->project_idFk = $project_id;
    		$chat->message = 'img';
    		$chat->user_idFk = Auth::User()->user_id;

    		$chat->save();

    		$files = $request->file('imgs');

    		foreach ($files as $key => $file) {

    			$filename = 'imgs'.str_random(40).'.'.$file->getClientOriginalExtension();
	    		$file->move('uploads/', $filename);

	    		$media = new ChatMedia();

	    		$media->chat_idFk = $chat->chat_id;
	    		$media->media_type = 'img';
	    		$media->value = $filename;

	    		$media->save();
    		}

        $users = ProjectUsers::where('project_idFk', $project_id)->get();

        if(count($users) > 0){
          foreach($users as $user){

            if(Auth::User()->user_id != $user->user_idFk){
              $anchor = '<a href="'.url('uploads/'.$filename).'" target="_blank" >Image</a>';
              $this->send_mail(User::findOrFail($user->user_idFk)->email, $anchor);

              $check = UnReadChat::where('project_idFk', $user->project_idFk)->where('user_idFk', $user->user_idFk)->exists();

              if($check == true){
                UnReadChat::where('project_idFk', $user->project_idFk)->where('user_idFk', $user->user_idFk)->update(['status' => '0']);
              }else{

                $new = new UnReadChat();

                $new->project_idFk = $user->project_idFk;
                $new->user_idFk = $user->user_idFk;

                $new->save();

              }

            }

          }
        }

           	$imgs = '';
           	foreach ($chat->imgs as $key => $value) {
           		$imgs .= '<a href="'.URL::to('/').'/uploads/'.$value->value.'"><img src="'.URL::to('/').'/uploads/'.$value->value.'" style="height: 80px;width: 80px;margin: 0px 5px;"></a>';
           	}

           	$message_div = '<div class="card-block">
                              <div class="profile-timeline-header">
                                <a href="#" class="profile-timeline-user" style="float:right;">
                                  <img src="'.URL::to('/').'/uploads/'.Auth::User()->img.'" alt="" class="img-rounded">
                                </a>
                                <div class="profile-timeline-user-details" style="float:right;">
                                  <a href="#" class="bold">'.$chat->user->name.'</a>
                                </div>
                              </div>
                              <div class="profile-timeline-content" style="margin-left:0px;margin-right:63px;text-align:right;">
                                <p style="margin-bottom:0px;">'.Carbon::createFromTimeStamp(strtotime($chat->created_at))->diffForHumans().'</p>
                                <p class="other-text-box" style="background-color: #bbead8;">'.$imgs.'</p>
                              </div>
                            </div>';

            $data['html'] = $message_div;
    		$data['last'] = $chat->chat_id;
    		return $data;

        }
    }


    public function message_check_new($chat_id)
    {
    	$chat = Chat::where('chat_id', '>', $chat_id)->exists();

    	if($chat == false){
    		return 'false';
    	}else{
    		return 'true';
    	}

    }

    public function message_get_new($chat_id)
    {

    	$chats = Chat::where('chat_id', '>', $chat_id)->get();

    	$html = "";
    	$last = "";
    	if(count($chats) > 0){

    		foreach ($chats as $key => $chat) {

    			$last = $chats->last()->chat_id;

    			if($chat->user_idFk != Auth::User()->user_id){

    				if($chat->type == 'message'){

    					$html .= '<div class="card-block">
                              <div class="profile-timeline-header">
                                <a href="#" class="profile-timeline-user" >
                                  <img src="'.URL::to('/').'/uploads/'.$chat->User->img.'" alt="" class="img-rounded">
                                </a>
                                <div class="profile-timeline-user-details" >
                                  <a href="#" class="bold">'.$chat->user->name.'</a>
                                </div>
                              </div>
                              <div class="profile-timeline-content">
                                <p style="margin-bottom:0px;">'.Carbon::createFromTimeStamp(strtotime($chat->created_at))->diffForHumans().'</p>
                                <p class="other-text-box">'.$chat->message.'</p>
                              </div>
                            </div>';

    				}elseif($chat->type == 'file'){

    					$file_type = '<i style="font-size: 22px;padding: 0px 10px;" class="fa fa-file-excel-o" aria-hidden="true"></i>';
			           	if($chat->file->media_type == 'pdf'){
			           		$file_type = '<i style="font-size: 22px;padding: 0px 10px;" class="fa fa-file-pdf-o" aria-hidden="true"></i>';
			           	}

			           	$html .= '<div class="card-block">
	                              <div class="profile-timeline-header">
	                                <a href="#" class="profile-timeline-user" >
	                                  <img src="'.URL::to('/').'/admin/images/avatar.jpg" alt="" class="img-rounded">
	                                </a>
	                                <div class="profile-timeline-user-details">
	                                  <a href="#" class="bold">'.$chat->user->name.'</a>
	                                </div>
	                              </div>
	                              <div class="profile-timeline-content">
	                                <p style="margin-bottom:0px;">'.Carbon::createFromTimeStamp(strtotime($chat->created_at))->diffForHumans().'</p>
	                                <p class="other-text-box" ><a href="'.URL::to('/').'/uploads/'.$media->value.'">'.$file_type.'</a></p>
	                              </div>
	                            </div>';

    				}elseif($chat->type == 'img'){

    					$imgs = '';
			           	foreach ($chat->imgs as $key => $value) {
			           		$imgs .= '<a href="'.URL::to('/').'/uploads/'.$value->value.'"><img src="'.URL::to('/').'/uploads/'.$value->value.'" style="height: 80px;width: 80px;margin: 0px 5px;"></a>';
			           	}

			           	$html = '<div class="card-block">
		                              <div class="profile-timeline-header">
		                                <a href="#" class="profile-timeline-user">
		                                  <img src="'.URL::to('/').'/admin/images/avatar.jpg" alt="" class="img-rounded">
		                                </a>
		                                <div class="profile-timeline-user-details">
		                                  <a href="#" class="bold">'.$chat->user->name.'</a>
		                                </div>
		                              </div>
		                              <div class="profile-timeline-content">
		                                <p style="margin-bottom:0px;">'.Carbon::createFromTimeStamp(strtotime($chat->created_at))->diffForHumans().'</p>
		                                <p class="other-text-box" >'.$imgs.'</p>
		                              </div>
		                            </div>';

    				}



    			} //user check


    		} //endforeach

    	}


    	if($html != ""){
    		$data['html'] = $html;
    		$data['last'] = $last;
    		return $data;
    	}

    }


}
