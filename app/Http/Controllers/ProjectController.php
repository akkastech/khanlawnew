<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Projects;
use App\ProjectStatus;
use App\ProjectUsers;
use App\Chat;
use Auth;
use App\User;
use DateTime;
use App\Mailers\Mailer;

class ProjectController extends Controller
{
    protected $mailer;
    function __construct(Mailer $mailer){

        $this->mailer = $mailer;

    }

    private function send_mail($email, $message)
    {
        // email
        $view = 'admin.mail';
        $subject = 'Welcome';
        $data['noti'] = $message;

        $this->mailer->sendTo($email, $subject, $view, $data);
    }

    private function get_users($project_id, $type, $message)
    {
        $users = ProjectUsers::where('project_idFk', $project_id)->get();

        foreach ($users as $key => $user) {

            if($user->type == $type){
                $this->send_mail(User::findOrFail($user->user_idFk)->email, $message);
            }

        }

    }

    public function project_list()
    {
        $user = Projects::where('notification', '=', '0')->get();
        // dd(count($user));
        if (count($user) > 0) {
            foreach ($user as $key => $value) {

                $new = Projects::findOrFail($value->project_id);
                // dd($new);
                $new->notification = '1';
                $new->save();

            }
        }

        if(Auth::User()->user_role_idFk == 1|| Auth::User()->user_role_idFk == 6|| Auth::User()->user_role_idFk == 7){

            $projects = Projects::where('project_status', '1')->where('project_going_status', 'Not-completed')->get();

        }elseif(Auth::User()->user_role_idFk == 3 || Auth::User()->user_role_idFk == 4 || Auth::User()->user_role_idFk == 2 || Auth::User()->user_role_idFk == 6 || Auth::User()->user_role_idFk == 7){
            $project_id_array = [];
            if(count(Auth::User()->projects) > 0){
                foreach (Auth::User()->projects as $key => $value) {
                    $project_id_array[$key] = $value->project_idFk;
                }
            }

            $projects = Projects::whereIn('project_id', $project_id_array)->where('project_going_status', 'Not-completed')->get();

        }

        return view('admin.projects.list_projects', compact('projects'));
    }

    public function project_closed_list()
    {
        if(Auth::User()->user_role_idFk == 1|| Auth::User()->user_role_idFk == 6|| Auth::User()->user_role_idFk == 7 ){

            $projects = Projects::where('project_status', '1')->where('project_going_status', 'Completed')->get();

        }elseif(Auth::User()->user_role_idFk == 3 || Auth::User()->user_role_idFk == 4 || Auth::User()->user_role_idFk == 2 || Auth::User()->user_role_idFk == 6 || Auth::User()->user_role_idFk == 7){
            $project_id_array = [];
            if(count(Auth::User()->projects) > 0){
                foreach (Auth::User()->projects as $key => $value) {
                    $project_id_array[$key] = $value->project_idFk;
                }
            }

            $projects = Projects::whereIn('project_id', $project_id_array)->where('project_going_status', 'Completed')->get();

        }

        return view('admin.projects.closed_list', compact('projects'));
    }

    public function project_add_page()
    {
        $users = User::where('user_id', '!=', '1')->where('user_status', '1')->get();

        return view('admin.projects.add_project', compact('users'));
    }

    public function project_submit(Request $request)
    {
        // dd(count($request['laywer_idFk']));
        if (!Projects::where('title', $request->title)->exists()) {
            $pro = new Projects();

            $pro->title        = $request->title;
            $pro->project_type = $request->type;
            $pro->description  = $request->desc;
            $pro->opening_date = $request->opening_date;
            $pro->closing_date = $request->closing_date;
            $pro->created_by   = Auth::User()->user_id;

            $pro->save();

            $date = new DateTime();

            $pro = Projects::findOrFail($pro->project_id);

            $pro->file_no = $request->file_no;

            $pro->save();

            $chat = new Chat();

            $chat->project_idFk = $pro->project_id;
            $chat->type = 'null';

            $chat->save();

            if(count($request->client_idFk) > 0 && $request->client_idFk[0] != null){

                foreach ($request->client_idFk as $key => $value) {

                    $users = new ProjectUsers();

                    $users->name = User::findOrFail($value)->name;
                    $users->user_idFk = $value;
                    $users->project_idFk = $pro->project_id;
                    $users->type = 'Client';

                    $users->save();

                    $this->send_mail(User::findOrFail($value)->email, 'creat project');
                }


            }


            // dd(User::findOrFail(1)->user_id, $pro->project_id );
            $user = new ProjectUsers();
            $user->name = User::findOrFail(1)->name;
            $user->user_idFk = User::findOrFail(1)->user_id;
            $user->project_idFk = $pro->project_id;
            $user->type = 'Admin';

            $user->save();
            // dd(User::findOrFail(1)->email);
            if($pro->created_by != 1){
              $this->send_mail(User::findOrFail(1)->email, 'New Project Created By'. Auth::User()->name);
            }

            if(count($request->agent_idFk) > 0 && $request->agent_idFk[0] != null){

                foreach ($request->agent_idFk as $key => $value) {

                    $users = new ProjectUsers();

                    $users->name = User::findOrFail($value)->name;
                    $users->user_idFk = $value;
                    $users->project_idFk = $pro->project_id;
                    $users->type = 'Agent';

                    $users->save();

                    $this->send_mail(User::findOrFail($value)->email, 'creat project');
                }


            }

            if(count($request->mortgage_idFk) > 0 && $request->mortgage_idFk[0] != null){

                foreach ($request->mortgage_idFk as $key => $value) {

                    $users = new ProjectUsers();

                    $users->name = User::findOrFail($value)->name;
                    $users->user_idFk = $value;
                    $users->project_idFk = $pro->project_id;
                    $users->type = 'Mortgage';

                    $users->save();

                    $this->send_mail(User::findOrFail($value)->email, 'creat project');
                }


            }

            if(count($request->laywer_idFk) > 0 && $request->lawyer[0] != null){

                foreach ($request->laywer_idFk as $key => $value) {

                    $users = new ProjectUsers();

                    $users->name = User::findOrFail($value)->name;
                    $users->user_idFk = $value;
                    $users->project_idFk = $pro->project_id;
                    $users->type = 'Laywer';

                    $users->save();

                    $this->send_mail(User::findOrFail($value)->email, 'creat project');
                }


            }

            if($request->opposision_idFk[0] != null && count($request->opposision_idFk) > 0){

                foreach ($request->opposision_idFk as $key => $value) {

                    $users = new ProjectUsers();

                    $users->name = User::findOrFail($value)->name;
                    $users->user_idFk = $value;
                    $users->project_idFk = $pro->project_id;
                    $users->type = 'Opposition';

                    $users->save();
                }


            }


            for ($i=1; $i <= 12; $i++) {

                $status = new ProjectStatus();

                $status->number = "".$i."";
                $status->project_idFk = $pro->project_id;

                $status->save();

            }




            return redirect('projects/list')->with('success', 'Project Submited Successfully');
        }else{
            return redirect('projects/list')->with('success', 'Project Alreay Exists');
        }

    }

    public function project_edit_page($project_id)
    {
        $project = Projects::findOrFail($project_id);
        $users = User::where('user_id', '!=', '1')->where('user_status', '1')->get();
        // dd($users, $project);
        return view('admin.projects.edit_project', compact('project', 'users'));
    }

    public function project_update(Request $request)
    {
        $date = new DateTime();
        $pro = Projects::findOrFail($request->project_id);

        $pro->title = $request->title;
        $pro->project_type = $request->type;
        $pro->description = $request->desc;
        $pro->file_no = $request->project_id.$date->format('m').$date->format('y').'RE';
        $pro->project_status = '1';
        $pro->created_by   = Auth::User()->user_id;
        $pro->opening_date = $request->opening_date;
        $pro->closing_date = $request->closing_date;

        $pro->save();
        if(!$pro->chat){
            $chat = new Chat();

            $chat->project_idFk = $pro->project_id;
            $chat->type = 'null';

            $chat->save();
          }

          if(count($request->client_idFk) > 0 && $request->client_idFk[0] != null){

              foreach ($request->client_idFk as $key => $value) {
                  $this->send_mail(User::findOrFail($value)->email, 'create project');
              }


          }


          if($request->agent_idFk[0] != null && count($request->agent_idFk) > 0){

              foreach ($request->agent_idFk as $key => $value) {
                  $this->send_mail(User::findOrFail($value)->email, 'Accepted Project');
              }


          }

          if($request->mortgage_idFk[0] != null && count($request->mortgage_idFk) > 0){

              foreach ($request->mortgage_idFk as $key => $value) {
                  $this->send_mail(User::findOrFail($value)->email, 'create project');
              }


          }

          if($request->laywer_idFk[0] != null && count($request->laywer_idFk) > 0){

              foreach ($request->laywer_idFk as $key => $value) {
                  $this->send_mail(User::findOrFail($value)->email, 'creat project');
              }
          }

        ProjectUsers::where('project_idFk', $request->project_id)->delete();

        if(count($request->client_idFk) > 0 && $request->client_idFk[0] != null){

            foreach ($request->client_idFk as $key => $value) {

                $users = new ProjectUsers();

                $users->name = User::findOrFail($value)->name;
                $users->user_idFk = $value;
                $users->project_idFk = $pro->project_id;
                $users->type = 'Client';

                $users->save();
            }


        }

        if($request->laywer_idFk[0] != null && count($request->laywer_idFk) > 0){

            foreach ($request->laywer_idFk as $key => $value) {

                $users = new ProjectUsers();

                $users->name = User::findOrFail($value)->name;
                $users->user_idFk = $value;
                $users->project_idFk = $pro->project_id;
                $users->type = 'Lawyer';

                $users->save();

                $this->send_mail(User::findOrFail($value)->email, 'creat project');
            }


        }

        if($request->agent_idFk[0] != null && count($request->agent_idFk) > 0){

            foreach ($request->agent_idFk as $key => $value) {

                $users = new ProjectUsers();

                $users->name = User::findOrFail($value)->name;
                $users->user_idFk = $value;
                $users->project_idFk = $pro->project_id;
                $users->type = 'Agent';

                $users->save();
            }


        }

        if($request->mortgage_idFk[0] != null && count($request->mortgage_idFk) > 0){
            foreach ($request->mortgage_idFk as $key => $value) {

                $users = new ProjectUsers();

                $users->name = User::findOrFail($value)->name;
                $users->user_idFk = $value;
                $users->project_idFk = $pro->project_id;
                $users->type = 'Mortgage';

                $users->save();
            }


        }

        if(count($request->opposision_idFk) > 0 && $request->opposision_idFk[0] != null){

            foreach ($request->opposision_idFk as $key => $value) {
                $users = new ProjectUsers();

                $users->name = User::findOrFail($value)->name;
                $users->user_idFk = $value;
                $users->project_idFk = $pro->project_id;
                $users->type = 'Opposition';

                $users->save();
            }


        }
        // dd('dumped');

        return redirect('projects/list')->with('success', 'Project Updated Successfully');
    }

    public function project_complete($project_id)
    {
        Projects::where('project_id', $project_id)->update(['project_going_status' => 'Completed']);

        return back()->with('success', 'Project Completed Successfully');
    }

    // public function project_deactive($project_id)
    // {
    //     Projects::where('project_id', $project_id)->update(['project_status' => '0']);

    //     return back()->with('success', 'Project Deactivated Successfully');
    // }

    public function project_delete($project_id)
    {

        ProjectUsers::where('project_idFk', $project_id)->delete();
        ProjectStatus::where('project_idFk', $project_id)->delete();

        Projects::where('project_id', $project_id)->delete();

        return back()->with('success', 'Project Deleted Successfully');
    }


    // project status
    public function project_status_page($project_id)
    {
        $status = ProjectStatus::where('project_idFk', $project_id)->get();
        // $status = ProjectStatus::all();
        // dd($status);
        if(count($status) > 0){
            $project = Projects::findOrFail($project_id);

            return view('admin.projects.status', compact('status', 'project'));
        }

    }

    public function project_status_active($status_id)
    {
        ProjectStatus::where('status_id', $status_id)->update(['status' => '1']);
        $project = Projects::findOrFail(ProjectStatus::findOrFail($status_id)->project_idFk);
        $statuses = ProjectStatus::where('project_idFk', ProjectStatus::findOrFail($status_id)->project_idFk)->get();

        $status_array = [];
        $mail_array = [];
        $id_array = [];
        foreach ($statuses as $key => $status) {

            $status_array[$key] = $status->status;
            $mail_array[$key]   = $status->mail;
            $id_array[$key] = $status->status_id;

        }

        if($project->project_type == 'Purchase'){

            // if($status_array[0] == '1' && $mail_array[0] == '0'){
            //     $this->get_users($project->project_id, 'Agent', ' Your deal for "property add" has been recevied.');
            //     ProjectStatus::where('status_id', $id_array[0])->update(['mail' => '1']);
            // }

            if($status_array[1] == '1' && $mail_array[1] == '0'){
                ProjectStatus::where('status_id', $id_array[1])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Client', 'welcome to your client care portal for your closing of "property address"');
                $this->get_users($project->project_id, 'Mortgage', 'You have been connect with "client name" for your deal "property address" on khan law client care portal');
                $this->get_users($project->project_id, 'Agent', 'You have been connect with "client name" for your deal "property address" on khan law client care portal ');
            }

            if($status_array[2] == '1' && $mail_array[2] == '0'){
                ProjectStatus::where('status_id', $id_array[2])->update(['mail' => '1']);

            }

            if($status_array[3] == '1' && $mail_array[3] == '0'){
                ProjectStatus::where('status_id', $id_array[3])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Client', ' Title search has been performed and requistion have been sent for "property address" closing on "closing date"');

            }

            if($status_array[4] == '1' && $mail_array[4] == '0'){
                ProjectStatus::where('status_id', $id_array[4])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Client', 'We have received your mortgage instrucations');
                $this->get_users($project->project_id, 'Mortgage', 'We have received mortage instructions for "property address" closing on "closing date"');
                $this->get_users($project->project_id, 'Agent', ' Mortgage instructions for " property" have been recevied by our office. ');

            }

            if($status_array[5] == '1' && $mail_array[5] == '0'){
                ProjectStatus::where('status_id', $id_array[5])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Client', 'Legal doucments for your purchase are ready. Please call our office to schedule an appointment. ');

            }

            if($status_array[6] == '1' && $mail_array[6] == '0'){
                ProjectStatus::where('status_id', $id_array[6])->update(['mail' => '1']);

            }

            if($status_array[7] == '1' && $mail_array[7] == '0'){
                ProjectStatus::where('status_id', $id_array[7])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Mortgage', ' We have met the client and requistion for funds has been submited.');

            }

            if($status_array[8] == '1' && $mail_array[8] == '0'){
                ProjectStatus::where('status_id', $id_array[8])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Mortgage', 'Confirmation for funding has been recevied, we are ready to close.');
                $this->get_users($project->project_id, 'Agent', 'Confirmation for funding has been recevied we are ready to close. ');

            }

            if($status_array[9] == '1' && $mail_array[9] == '0'){
                ProjectStatus::where('status_id', $id_array[9])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Mortgage', ' Congradulations the deal has been closed succesfully');
                $this->get_users($project->project_id, 'Agent', ' Congradulations the deal has been closed succesfully.');

            }

            if($status_array[10] == '1' && $mail_array[10] == '0'){
                ProjectStatus::where('status_id', $id_array[10])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Client', ' Congradulations!!!Your keys are ready for pick up ');

            }

            if($status_array[11] == '1' && $mail_array[11] == '0'){
                ProjectStatus::where('status_id', $id_array[11])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Client', '  Your final reporting is ready for pick up');

            }


        }

        // sale
        if($project->project_type == 'Sale'){

            // if($status_array[0] == '1' && $mail_array[0] == '0'){
            //     ProjectStatus::where('status_id', $id_array[0])->update(['mail' => '1']);
            //     $this->get_users($project->project_id, 'Agent', 'Your deal for "property add" has been recevied.');
            //
            // }

            if($status_array[1] == '1' && $mail_array[1] == '0'){
                ProjectStatus::where('status_id', $id_array[1])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Client', 'welcome to your client care portal for your closing of "property address"');
                $this->get_users($project->project_id, 'Agent', 'You have been connect with "client name" for your deal "property address" on khan law client care portal ');
            }

            if($status_array[2] == '1' && $mail_array[2] == '0'){
                ProjectStatus::where('status_id', $id_array[2])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Client', 'please provide our office with your mortage statment, any LOC registerd on the proerty and property tax statment with proof of payment');

            }

            if($status_array[3] == '1' && $mail_array[3] == '0'){
                ProjectStatus::where('status_id', $id_array[3])->update(['mail' => '1']);

            }

            if($status_array[4] == '1' && $mail_array[4] == '0'){
                ProjectStatus::where('status_id', $id_array[4])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Client', ' Legal doucments for your sale are ready. Please call our office to schedule an appointment. Please bing copy of the key and 2 peices of id.');

            }

            if($status_array[5] == '1' && $mail_array[5] == '0'){
                ProjectStatus::where('status_id', $id_array[5])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Agent', 'Client signatues are completed');

            }

            if($status_array[6] == '1' && $mail_array[6] == '0'){
                ProjectStatus::where('status_id', $id_array[6])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Client', 'Closing docments have been sent to purchaser\'s lawyer. We are ready to close');
                $this->get_users($project->project_id, 'Agent', ' Closing docments have been sent to purchaser\'s lawyer. We are ready to close');

            }

            if($status_array[7] == '1' && $mail_array[7] == '0'){
                ProjectStatus::where('status_id', $id_array[7])->update(['mail' => '1']);

            }

            if($status_array[8] == '1' && $mail_array[8] == '0'){
                ProjectStatus::where('status_id', $id_array[8])->update(['mail' => '1']);

            }

            if($status_array[9] == '1' && $mail_array[9] == '0'){
                ProjectStatus::where('status_id', $id_array[9])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Client', 'Congradulations sale of "property" is complete. ');
                $this->get_users($project->project_id, 'Agent', ' Congradulations the deal has been closed succesfully.');

            }

            if($status_array[10] == '1' && $mail_array[10] == '0'){
                ProjectStatus::where('status_id', $id_array[10])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Client', ' Your final reporting is ready for pick up');

            }

            if($status_array[11] == '1' && $mail_array[11] == '0'){
                ProjectStatus::where('status_id', $id_array[11])->update(['mail' => '1']);

            }

        }

        // Refinance
        if($project->project_type == 'Refinance'){

            // if($status_array[0] == '1' && $mail_array[0] == '0'){
            //     ProjectStatus::where('status_id', $id_array[0])->update(['mail' => '1']);
            //
            // }

            if($status_array[1] == '1' && $mail_array[1] == '0'){
                ProjectStatus::where('status_id', $id_array[1])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Client', ' welcome to your client care portal for your closing of "property address"');
                $this->get_users($project->project_id, 'Mortgage', 'You have been connect with "client name" for your deal "property address" on khan law client care portal ');
            }

            if($status_array[2] == '1' && $mail_array[2] == '0'){
                ProjectStatus::where('status_id', $id_array[2])->update(['mail' => '1']);

            }

            if($status_array[3] == '1' && $mail_array[3] == '0'){
                ProjectStatus::where('status_id', $id_array[3])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Client', 'please provide our office with your mortage statment, any LOC registerd on the proerty and property tax statment with proof of payment');

            }

            if($status_array[4] == '1' && $mail_array[4] == '0'){
                ProjectStatus::where('status_id', $id_array[4])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Client', 'We have received your mortgage instrucations');
                $this->get_users($project->project_id, 'Mortgage', 'Mortgage instructions for " property" have been recevied by our office. ');

            }

            if($status_array[5] == '1' && $mail_array[5] == '0'){
                ProjectStatus::where('status_id', $id_array[5])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Client', ' Legal doucments for your refinace are ready. Please call our office to schedule an appointment');

            }

            if($status_array[6] == '1' && $mail_array[6] == '0'){
                ProjectStatus::where('status_id', $id_array[6])->update(['mail' => '1']);

            }

            if($status_array[7] == '1' && $mail_array[7] == '0'){
                ProjectStatus::where('status_id', $id_array[7])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Mortgage', ' We have met the client and requistion for funds has been submited. ');

            }

            if($status_array[8] == '1' && $mail_array[8] == '0'){
                ProjectStatus::where('status_id', $id_array[8])->update(['mail' => '1']);

            }

            if($status_array[9] == '1' && $mail_array[9] == '0'){
                ProjectStatus::where('status_id', $id_array[9])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Client', 'Congradulations the deal has been closed succesfully.');
                $this->get_users($project->project_id, 'Mortgage', ' Congradulations the deal has been closed succesfully.');

            }

            if($status_array[10] == '1' && $mail_array[10] == '0'){
                ProjectStatus::where('status_id', $id_array[10])->update(['mail' => '1']);
                $this->get_users($project->project_id, 'Client', 'Your final reporting is ready for pick up');

            }

            if($status_array[11] == '1' && $mail_array[11] == '0'){
                ProjectStatus::where('status_id', $id_array[11])->update(['mail' => '1']);

            }

        }





        $project->save();
        return back()->with('success', 'Status Completed Successfully');
    }

    public function project_status_deactive($status_id)
    {
        ProjectStatus::where('status_id', $status_id)->update(['status' => '0']);

        return back()->with('success', 'Status Cancel Successfully');
    }


    // requested projects start here
    public function request_project_list()
    {

        if(Auth::User()->user_role_idFk == 1){
            $projects = Projects::where('project_status', '0')->get();
        }else{
            $projects = Projects::where('requested_by', Auth::User()->user_id)->get();
        }

        return view('admin.projects.requested_list_projects', compact('projects'));

    }

    public function request_project_add()
    {
        $users = User::where('created_by', Auth::User()->user_id)->where('user_status', '1')->get();

        return view('admin.projects.requested_add_project', compact('users'));
    }

    public function request_project_submit(Request $request)
    {

        $pro = new Projects();

        $pro->title        = $request->title;
        $pro->project_type = $request->type;
        $pro->description  = $request->desc;
        $pro->closing_date = $request->closing_date;
        $pro->opening_date = $request->opening_date;
        $pro->project_status = '0';
        $pro->created_by   = Auth::User()->user_id;
        $pro->requested_by  = Auth::User()->user_id;

        if($request->hasFile('agrement')){


            $filename = 'agrement_2016'.str_random(40).'.'.$request->file('agrement')->getClientOriginalExtension();

            $request->file('agrement')->move('uploads/', $filename);
            $pro->agrement = $filename;
        }

        $pro->save();


        if($request->client_idFk){

            foreach ($request->client_idFk as $key => $value) {

                $users = new ProjectUsers();

                $users->name = User::findOrFail($value)->name;
                $users->user_idFk = $value;
                $users->project_idFk = $pro->project_id;
                $users->type = 'Client';

                $users->save();
            }

            $users = new ProjectUsers();

            $users->name = Auth::User()->name;
            $users->user_idFk = Auth::User()->user_id;
            $users->project_idFk = $pro->project_id;
            $users->type = 'Agent';

            $users->save();


        }

        for ($i=1; $i <= 12; $i++) {

            $status = new ProjectStatus();

            $status->number = "".$i."";
            $status->project_idFk = $pro->project_id;

            $status->save();

        }

        return redirect('requested/projects/list')->with('success', 'Project Request Send Successfully');
    }

    public function request_project_edit($id)
    {
        $project = Projects::findOrFail($id);
        $users = User::where('created_by', Auth::User()->user_id)->where('user_status', '1')->get();

        return view('admin.projects.requested_edit_project', compact('project', 'users'));
    }

    public function request_project_update(Request $request)
    {
        $pro = Projects::findOrFail($request->project_id);

        $pro->title = $request->title;
        $pro->project_type = $request->type;
        $pro->description = $request->desc;
        $pro->opening_date = $request->opening_date;

        if($request->hasFile('agrement')){


            $filename = 'agrement_2016'.str_random(40).'.'.$request->file('agrement')->getClientOriginalExtension();

            $request->file('agrement')->move('uploads/', $filename);
            $pro->agrement = $filename;
        }

        $pro->save();

        ProjectUsers::where('project_idFk', $request->project_id)->delete();

        if($request->client_idFk){

            foreach ($request->client_idFk as $key => $value) {

                $users = new ProjectUsers();

                $users->name = User::findOrFail($value)->name;
                $users->user_idFk = $value;
                $users->project_idFk = $pro->project_id;
                $users->type = 'Client';

                $users->save();
            }

            $users = new ProjectUsers();

            $users->name = Auth::User()->name;
            $users->user_idFk = Auth::User()->user_id;
            $users->project_idFk = $pro->project_id;
            $users->type = 'Agent';

            $users->save();

        }

        return redirect('requested/projects/list')->with('success', 'Project Updated Successfully');
    }

    public function request_project_reject($id)
    {
        $project = Projects::findOrFail($id);
        $project->project_status = '2';
        $project->save();

        $this->send_mail($project->user->email, 'Rejected project');

        return redirect()->back()->with('success', 'Project Rejected Successfully');
    }

    public function request_project_accept($id)
    {
        $project = Projects::findOrFail($id);
        $users = User::where('user_id', '!=', '1')->where('user_status', '1')->get();

        return view('admin.projects.edit_project', compact('project', 'users'));
    }



}
