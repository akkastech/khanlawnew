<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Auth;
use Hash;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function registration(Request $request)
    {
        $check = User::where('email', $request->email)->exists();

        if($check == true)
            return back()->with('error', 'Email Already Register..')->withInput();
        
        // dd($request);
        if($request->type == '3' || $request->type == '4'){

            $user = new User();

            $user->name = $request->name;
            $user->email = $request->email;
            
            $user->country = $request->country;
            $user->province = $request->province;
            $user->city = $request->city;
            if ($request->number) {
                $user->number = $request->number;
            }
            if ($request->password) {
                $user->password = Hash::make($request->password);
            }
            if ($request->img) {
                $user->img = $request->img;  
            }

            $user->user_role_idFk = $request->type;

            $user->save();
            Auth::login($user);

            return redirect('dashboard')->with('success', 'Registration Successfully Completed.');

        }else{

            return back()->with('error', 'Cannot proccess try again later')->withInput();

        }

        
    }
}
