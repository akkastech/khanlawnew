<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Hash;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function login_page()
    {
        return view('auth.login');
    }

    public function signup_page()
    {
        return view('auth.signup');
    }

    public function authenticateUser(Request $request)
    {

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->remember)){
            if(Auth::User()->user_status == 1){
              return redirect('dashboard');
            }else{
              return redirect()->back()->with('error', 'Account disabled')->withInput();
            }


        }else{

            return redirect()->back()->with('error', 'Invalid credentials please verify before login')->withInput();
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect('login');
    }

    public function facebook_login_redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function facebook_login_callback()
    {
        $user = Socialite::driver('facebook')->user();
        // dd($user);
        $check = User::where('email', $user->email)->first();
        if (count($check) > 0) {
            Auth::Login($check);
            return redirect('dashboard');
        }else{
            return view('auth.login_social', compact('user'));
        }
    }

    public function google_login_redirect()
    {
        return Socialite::driver('google')->redirect();
    }

    public function google_login_callback()
    {
        $user = Socialite::driver('google')->stateless()->user();
        // dd($user);
        $check = User::where('email', $user->email)->first();
        if (count($check) > 0) {
            Auth::Login($check);
            return redirect('dashboard');
        }else{

            return view('auth.login_social', compact('user'));
        }
    }

}
