<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Projects;
use Auth;
use App\ProjectUsers;

class PortalController extends Controller
{
    public function portals_list()
    {
        $user = Projects::where('notification', '=', '0')->get();
        // dd($user);
        if (count($user) > 0) {
            foreach ($user as $key => $value) {

                $new = Projects::findOrFail($value->project_id);
                $new->notification = 1;

                $new->save();

            }
        }
        if(Auth::User()->user_role_idFk == 1 || Auth::User()->user_role_idFk == 6 || Auth::User()->user_role_idFk == 7){
            $projects = Projects::where('project_status', '1')->get();
        }else{

            $projects = Auth::User()->projects;
            $project_id_array = [];
            if(count($projects) > 0){

                foreach($projects as $key => $pro){
                    $project_id_array[$key] = $pro->project_idFk;
                }

            }

            $projects = Projects::where('project_status', '1')->whereIn('project_id', $project_id_array)->get();
        }

    	return view('admin.portals.list_portals', compact('projects'));
    }

    public function tracker_list_page()
    {
        if(Auth::User()->user_role_idFk == 1 || Auth::User()->user_role_idFk == 6 || Auth::User()->user_role_idFk == 7 || Auth::User()->user_role_idFk == 5){
          $projects = Projects::where('project_status', '1')->get();
           
        }else{
          $projects = [];
          $users = ProjectUsers::where('user_idFk', Auth::User()->user_id)->get();
          foreach ($users as $key => $value) {

              $projects[$key] = Projects::where('project_id', $value->project_idFk)->where('project_status', 1)->get();
          }
        }
        
        if (count($projects) == 1) {
            $pro = Projects::findOrFail($projects[0]->project_id);
            return view('admin.tracker.single_list_tracker_new', compact('projects','pro'));
            
        }else{
            $projects = [];
            $users = ProjectUsers::where('user_idFk', Auth::User()->user_id)->get();
            foreach ($users as $key => $value) {
              $projects[$key] = Projects::where('project_id', $value->project_idFk)->where('project_status', 1)->get();
            }
            return view('admin.tracker.list_tracker', compact('projects'));   
        }
    }

    public function tracker_list($project_id)
    {
    	$pro = Projects::findOrFail($project_id);

        if(Auth::User()->user_role_idFk == 1 || Auth::User()->user_role_idFk == 6 || Auth::User()->user_role_idFk == 7 || Auth::User()->user_role_idFk == 5){
            $projects = Projects::where('project_status', '1')->get();
            // dd($projects);
        }else{

            $projects = Auth::User()->projects;
            $project_id_array = [];
            if(count($projects) > 0){

                foreach($projects as $key => $value){
                    $project_id_array[$key] = $value->project_idFk;
                }

            }

            $projects = Projects::where('project_status', '1')->whereIn('project_id', $project_id_array)->get();
        }

    	return view('admin.tracker.single_list_tracker_new', compact('pro', 'projects'));
    }

}
