<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectUsers extends Model
{
    protected $table = 'project_users';
    protected $primaryKey = 'p_user_id';

    public function project(){
        return $this->belongsTo('App\Projects', 'project_idFk', 'project_id');
    }
}
