<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Realtors extends Model
{
    protected $primaryKey = 'realtor_id';

    public function user()
    {
      return $this->belongsTo('App\User', 'realtor_idFk', 'user_id');
    }

    public function realtors($id)
    {
      return Realtors::where('broker_idFk', $id)->count();
      
    }
}
