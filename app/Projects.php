<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProjectStatus;
use Carbon\Carbon;
use Auth;

class Projects extends Model
{
    protected $table = 'projects';
    protected $primaryKey = 'project_id';

    public function user(){
        return $this->belongsTo('App\User', 'created_by', 'user_id');
    }
    public function client(){
        return $this->hasMany('App\ProjectUsers', 'project_idFk', 'project_id')->where('type', 'Client');
    }
    public function agent(){
        return $this->hasMany('App\ProjectUsers', 'project_idFk', 'project_id')->where('type', 'Agent');
    }
    public function mortgage(){
        return $this->hasMany('App\ProjectUsers', 'project_idFk', 'project_id')->where('type', 'Mortgage');
    }
    public function opposition(){
        return $this->hasMany('App\ProjectUsers', 'project_idFk', 'project_id')->where('type', 'Opposition');
    }
    public function lawyer(){
        return $this->hasMany('App\ProjectUsers', 'project_idFk', 'project_id')->where('type', 'Lawyer');
    }
    // public function tracker(){
    //     return $this->hasMany('App\ProjectStatus', 'project_idFk', 'project_id');
    // }
    public function status($number){
        $status = $this->hasMany('App\ProjectStatus', 'project_idFk', 'project_id')->get();

        if(count($status) > 0){

            return $status[($number-1)]->status;

        }

        
    }

    public function un_read(){
        return $this->hasOne('App\UnReadChat', 'project_idFk', 'project_id')->where('status', '0')->where('user_idFk', Auth::User()->user_id);
    }

    // check date or states are completed or not
    public function check_complete($id)
    {
        $status_count = ProjectStatus::where('project_idFk', $id)->where('status', '1')->count();
        
        if($status_count == 12){

            $project = Projects::findOrFail($id);
            if(Carbon::createFromTimeStamp(strtotime($project->closing_date))->isPast() == true){
                return 'true';
            }else{
                return 'false';
            }

        }

        return 'false';
    }

    public function chat(){
        return $this->hasOne('App\Chat', 'project_idFk', 'project_id');
    }
    
}
