<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnReadChat extends Model
{
    protected $table = 'un_read_chats';
    protected $primaryKey = 'ur_chat_id';
}
