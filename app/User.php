<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Projects;

class User extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role(){
        return $this->belongsTo('App\UserRoles', 'user_role_idFk', 'role_id');
    }
    public function admin_pojects(){
        return $this->hasMany('App\Projects', 'created_by', 'user_id');
    }
    public function projects(){
        return $this->hasMany('App\ProjectUsers', 'user_idFk', 'user_id');
    }
    public function users(){
        return $this->hasMany('App\User', 'created_by', 'user_id');
    }
    // public function project_for_tracker(){
    //     return $this->hasMany('App\Projects', 'created_by', 'user_id');
    // }
    public function project_for_tracker()
    {
      $userIDS = [];

      $users = User::whereIn('user_role_idFk', ['1', '7', '6'])->get();
      if(count($users) > 0){
        foreach ($users as $key => $value) {
            $userIDS[$key] = $value->user_id;
        }
      }

      return Projects::whereIn('created_by', $userIDS)->get();
    }
    public function un_read(){
        return $this->hasMany('App\UnReadChat', 'user_idFk', 'user_id')->where('status', '0');
    }
    public function client_project(){
        return $this->hasOne('App\ProjectUsers', 'user_idFk', 'user_id');

    }
    public function realtors(){
        return $this->hasMany('App\Realtors', 'broker_idFk', 'user_id');
    }

    public function new_users()
    {
        return User::where('notification', '0')->count();
    }
    public function new_projects()
    {
        return Projects::where('notification', '0')->count();
    }

    public function closed_notify()
    {
        return Projects::where('project_going_status', 'Completed')->count();
    }

    public function check_realtor($id, $realtor_id)
    {
      return Realtors::where('broker_idFk', $id)->where('realtor_idFk', $realtor_id)->exists();
    }

}
