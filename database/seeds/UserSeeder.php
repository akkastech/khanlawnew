<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Khan Law LLP',
                'email'     => 'khanlawportal@gmail.com',
                'password'  => bcrypt('khanlawportal@123'),
                'img'       => 'user.jpg',
                'number'  => '03001234567',
                'country'  => 'Canada',
                'province'  => 'Ontario',
                'city'  => 'Milton',
                'user_role_idFk' => 1
            ]
                ,[
                'name' => 'client 1',
                'email'     => 'client1@getnada.com',
                'password'  => bcrypt('123'),
                'img'       => 'user.jpg',
                'number'  => '03001234567',
                'country'  => 'Pakistan',
                'province'  => 'Punjab',
                'city'  => 'Lahore',
                'user_role_idFk' => 2
            ],[
                'name' => 'client 2',
                'email'     => 'client2@getnada.com',
                'password'  => bcrypt('123'),
                'img'       => 'user.jpg',
                'number'  => '03002234567',
                'country'  => 'Pakistan',
                'province'  => 'Punjab',
                'city'  => 'Lahore',
                'user_role_idFk' => 2
            ],[
                'name' => 'client 3',
                'email'     => 'client3@getnada.com',
                'password'  => bcrypt('123'),
                'img'       => 'user.jpg',
                'number'  => '03003234567',
                'country'  => 'Pakistan',
                'province'  => 'Punjab',
                'city'  => 'Lahore',
                'user_role_idFk' => 2
            ],[
                'name' => 'realtor 1',
                'email'     => 'realtor1@getnada.com',
                'password'  => bcrypt('123'),
                'img'       => 'user.jpg',
                'number'  => '03001234567',
                'country'  => 'Pakistan',
                'province'  => 'Punjab',
                'city'  => 'Lahore',
                'user_role_idFk' => 3
            ],[
                'name' => 'realtor 2',
                'email'     => 'realtor2@getnada.com',
                'password'  => bcrypt('123'),
                'img'       => 'user.jpg',
                'number'  => '03002234567',
                'country'  => 'Pakistan',
                'province'  => 'Punjab',
                'city'  => 'Lahore',
                'user_role_idFk' => 3
            ],[
                'name' => 'realtor 3',
                'email'     => 'realtor3@getnada.com',
                'password'  => bcrypt('123'),
                'img'       => 'user.jpg',
                'number'  => '03003234567',
                'country'  => 'Pakistan',
                'province'  => 'Punjab',
                'city'  => 'Lahore',
                'user_role_idFk' => 3
            ],[
                'name' => 'lender 1',
                'email'     => 'lender1@getnada.com',
                'password'  => bcrypt('123'),
                'img'       => 'user.jpg',
                'number'  => '03001234567',
                'country'  => 'Pakistan',
                'province'  => 'Punjab',
                'city'  => 'Lahore',
                'user_role_idFk' => 4
            ],[
                'name' => 'lender 2',
                'email'     => 'lender2@getnada.com',
                'password'  => bcrypt('123'),
                'img'       => 'user.jpg',
                'number'  => '03002234567',
                'country'  => 'Pakistan',
                'province'  => 'Punjab',
                'city'  => 'Lahore',
                'user_role_idFk' => 4
            ],[
                'name' => 'lender 3',
                'email'     => 'lender3@getnada.com',
                'password'  => bcrypt('123'),
                'img'       => 'user.jpg',
                'number'  => '03003234567',
                'country'  => 'Pakistan',
                'province'  => 'Punjab',
                'city'  => 'Lahore',
                'user_role_idFk' => 4
            ],[
                'name' => 'lawyer 1',
                'email'     => 'lawyer1@getnada.com',
                'password'  => bcrypt('123'),
                'img'       => 'user.jpg',
                'number'  => '03001234567',
                'country'  => 'Pakistan',
                'province'  => 'Punjab',
                'city'  => 'Lahore',
                'user_role_idFk' => 6
            ],[
                'name' => 'lawyer 2',
                'email'     => 'lawyer2@getnada.com',
                'password'  => bcrypt('123'),
                'img'       => 'user.jpg',
                'number'  => '03002234567',
                'country'  => 'Pakistan',
                'province'  => 'Punjab',
                'city'  => 'Lahore',
                'user_role_idFk' => 6
            ],[
                'name' => 'lawyer 3',
                'email'     => 'lawyer3@getnada.com',
                'password'  => bcrypt('123'),
                'img'       => 'user.jpg',
                'number'  => '03003234567',
                'country'  => 'Pakistan',
                'province'  => 'Punjab',
                'city'  => 'Lahore',
                'user_role_idFk' => 6
            ],[
                'name' => 'lawclerk 1',
                'email'     => 'lawclerk1@getnada.com',
                'password'  => bcrypt('123'),
                'img'       => 'user.jpg',
                'number'  => '03001234567',
                'country'  => 'Pakistan',
                'province'  => 'Punjab',
                'city'  => 'Lahore',
                'user_role_idFk' => 7
            ],[
                'name' => 'lawclerk 2',
                'email'     => 'lawclerk2@getnada.com',
                'password'  => bcrypt('123'),
                'img'       => 'user.jpg',
                'number'  => '03002234567',
                'country'  => 'Pakistan',
                'province'  => 'Punjab',
                'city'  => 'Lahore',
                'user_role_idFk' => 7
            ],[
                'name' => 'lawclerk 3',
                'email'     => 'lawclerk3@getnada.com',
                'password'  => bcrypt('123'),
                'img'       => 'user.jpg',
                'number'  => '03003234567',
                'country'  => 'Pakistan',
                'province'  => 'Punjab',
                'city'  => 'Lahore',
                'user_role_idFk' => 7
            ],[
                'name' => 'broker 1',
                'email'     => 'broker1@getnada.com',
                'password'  => bcrypt('123'),
                'img'       => 'user.jpg',
                'number'  => '03001234567',
                'country'  => 'Pakistan',
                'province'  => 'Punjab',
                'city'  => 'Lahore',
                'user_role_idFk' => 5
            ],[
                'name' => 'broker 2',
                'email'     => 'broker2@getnada.com',
                'password'  => bcrypt('123'),
                'img'       => 'user.jpg',
                'number'  => '03002234567',
                'country'  => 'Pakistan',
                'province'  => 'Punjab',
                'city'  => 'Lahore',
                'user_role_idFk' => 5
            ],[
                'name' => 'broker 3',
                'email'     => 'broker3@getnada.com',
                'password'  => bcrypt('123'),
                'img'       => 'user.jpg',
                'number'  => '03003234567',
                'country'  => 'Pakistan',
                'province'  => 'Punjab',
                'city'  => 'Lahore',
                'user_role_idFk' => 5
            ],
        ]);
    }
}
