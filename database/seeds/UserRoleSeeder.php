<?php

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert([
            [
                'role_title' => 'Super Admin'
            ],[
                'role_title' => 'Client'
            ],[
                'role_title' => 'Realtor'
            ],[
                'role_title' => 'Lender'
            ],[
                'role_title' => 'Broker Of Record'
            ],[
                'role_title' => 'Lawyer'
            ],[
                'role_title' => 'Law Clerk'
            ]
        ]);
    }
}
