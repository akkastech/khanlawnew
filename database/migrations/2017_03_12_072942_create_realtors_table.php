<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealtorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('realtors', function (Blueprint $table) {
            $table->increments('realtor_id');
            $table->integer('broker_idFk')->unsigned();
            $table->integer('realtor_idFk')->unsigned();
            $table->timestamps();
        });

        Schema::table('realtors', function($table) {
            $table->foreign('broker_idFk')->references('user_id')->on('users')->onDelete('restrict');
            $table->foreign('realtor_idFk')->references('user_id')->on('users')->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('realtors');
    }
}
