<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function(Blueprint $table)
        {
            $table->increments('user_id');
            $table->string('name');
            $table->string('email');
            $table->string('password',60)->nullable();
            $table->string('number')->nullable();
            $table->string('img')->default('user.jpg');
            $table->string('country')->nullable();
            $table->string('province')->nullable();
            $table->string('city')->nullable();
            $table->integer('country_idFk')->nullable();
            $table->integer('province_idFk')->nullable();
            $table->integer('city_idFk')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('user_role_idFk')->unsigned();
            $table->enum('notification', array(1,0))->default(0);
            $table->enum('user_status',array(1,0))->default(1);
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::table('users', function($table) {
            $table->foreign('created_by')->references('user_id')->on('users')->onDelete('restrict');
            $table->foreign('user_role_idFk')->references('role_id')->on('user_roles')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
