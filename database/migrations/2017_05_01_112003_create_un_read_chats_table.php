<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnReadChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('un_read_chats', function (Blueprint $table) {
            $table->increments('ur_chat_id');
            $table->integer('project_idFk')->unsigned();
            $table->integer('user_idFk')->unsigned();
            $table->enum('status',array(1,0))->default(0);
            $table->timestamps();
        });

        Schema::table('un_read_chats', function($table) {
            $table->foreign('project_idFk')->references('project_id')->on('projects')->onDelete('restrict');
            $table->foreign('user_idFk')->references('user_id')->on('users')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('un_read_chats');
    }
}
