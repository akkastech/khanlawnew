<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('project_id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->date('opening_date');
            $table->date('closing_date');
            $table->string('agrement')->nullable();
            $table->string('file_no')->nullable();
            // $table->string('sale_1')->nullable();
            // $table->string('sale_2')->nullable();
            // $table->string('sale_3')->nullable();
            // $table->string('purchase_1')->nullable();
            // $table->string('purchase_2')->nullable();
            // $table->string('purchase_3')->nullable();
            // $table->string('purchase_4')->nullable();
            $table->integer('created_by')->unsigned();
            $table->integer('requested_by')->unsigned()->nullable();
            $table->enum('notification', array(0,1))->default(0);
            $table->enum('project_type',array("Sale","Purchase","Refinance"));
            $table->string('project_going_status')->default('Not-completed');
            $table->enum('project_status',array(2,1,0))->default(1);
            $table->timestamps();
        });

        Schema::table('projects', function($table) {
            $table->foreign('created_by')->references('user_id')->on('users')->onDelete('restrict');
            $table->foreign('requested_by')->references('user_id')->on('users')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
