<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_status', function (Blueprint $table) {
            $table->increments('status_id');
            $table->string('number');
            $table->integer('project_idFk')->unsigned();
            $table->enum('status',array(1,0))->default(0);
            $table->enum('mail',array(1,0))->default(0);
            $table->timestamps();
        });

        Schema::table('project_status', function($table) {
            $table->foreign('project_idFk')->references('project_id')->on('projects')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_status');
    }
}
