<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_media', function (Blueprint $table) {
            $table->increments('media_id');
            $table->string('value');
            $table->integer('chat_idFk')->unsigned();
            $table->enum('media_status',array(1,0))->default(1);
            $table->string('media_type')->nullable();
            $table->timestamps();
        });

        Schema::table('chat_media', function($table) {
            $table->foreign('chat_idFk')->references('chat_id')->on('chat')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_media');
    }
}
