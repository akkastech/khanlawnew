<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat', function (Blueprint $table) {
            $table->increments('chat_id');
            $table->string('message')->nullable();
            $table->string('type');
            $table->integer('project_idFk')->unsigned();
            $table->integer('user_idFk')->unsigned()->nullable();
            $table->enum('chat_status',array(1,0))->default(0);
            $table->timestamps();
        });

        Schema::table('chat', function($table) {
            $table->foreign('project_idFk')->references('project_id')->on('projects')->onDelete('restrict');
            $table->foreign('user_idFk')->references('user_id')->on('users')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat');
    }
}
